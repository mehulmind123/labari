//
//  APIRequest.m
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import "APIRequest.h"
#import "MIAFNetworking.h"

static APIRequest *request = nil;

@implementation APIRequest

+ (id)request
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        request = [[APIRequest alloc] init];
        [[MIAFNetworking sharedInstance] setBaseURL:BASEURL];
        
        NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:[[[[MIAFNetworking sharedInstance] sessionManager] responseSerializer] acceptableStatusCodes]];
        
        [indexSet addIndex:400];
        
        [[[MIAFNetworking sharedInstance] sessionManager] responseSerializer].acceptableStatusCodes = indexSet;
    });
    
    if ([CUserDefaults valueForKey:UserDefaultLoginToken])
        [[[MIAFNetworking sharedInstance] sessionManager].requestSerializer setValue:[CUserDefaults valueForKey:UserDefaultLoginToken] forHTTPHeaderField:@"token"];
    else
        [[[MIAFNetworking sharedInstance] sessionManager].requestSerializer setValue:@"" forHTTPHeaderField:@"token"];
    
    return request;
}

- (void)checkLoginUserStatus:(NSString *)message
{
    if ([UIApplication userId])
    {
        [[[UIAlertView alloc] initWithTitle:@"Inactive User" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error
{
    NSLog(@"API Error(%@) == %@", api, error);
    
    if (error.code != -999 && ![error.localizedDescription isEqualToString:@"cancelled"])
        [CustomAlertView iOSAlert:@"" withMessage:error.localizedDescription withDelegate:nil];
}

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task
{
    return ((NSHTTPURLResponse *)task.response).statusCode == 200 ? nil : [NSError errorWithDomain:@"" code:((NSHTTPURLResponse *)task.response).statusCode userInfo:nil];
}










#pragma mark
#pragma mark - LRF



@end
