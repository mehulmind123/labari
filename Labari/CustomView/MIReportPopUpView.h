//
//  MIReportPopUpView.h
//  Labari
//
//  Created by mac-0005 on 3/22/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum : NSUInteger
{
    PublicBox,
    InterestBox
    
}reportType;


@interface MIReportPopUpView : UIView
{
    IBOutlet UIView *viewControl;
}

@property (strong, nonatomic) NSMutableArray *arrData;
@property (strong, nonatomic) NSMutableArray *arrSelected;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cntblVReportHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnViewControlReportHeight;
@property (strong, nonatomic) IBOutlet UITableView *tblVReport;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnColse;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

@property (nonatomic , assign) reportType reportType;

+ (id)customReportView:(reportType)type interest:(NSMutableArray *)arr;

@end
