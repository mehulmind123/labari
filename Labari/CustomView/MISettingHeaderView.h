//
//  MISettingHeaderView.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 21/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISettingHeaderView : UITableViewHeaderFooterView

@property (strong, nonatomic) IBOutlet UIView *objTopSeperator;
@property (strong, nonatomic) IBOutlet UILabel *lblHeaderTitle;
@property (strong, nonatomic) IBOutlet UIView *objBottomSeperator;

@end
