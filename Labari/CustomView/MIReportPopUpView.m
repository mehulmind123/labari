//
//  MIReportPopUpView.m
//  Labari
//
//  Created by mac-0005 on 3/22/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIReportPopUpView.h"

#import "MIReportTableViewCell.h"

@implementation MIReportPopUpView

- (void)awakeFromNib
{
    [super awakeFromNib];
    viewControl.layer.cornerRadius = 5;
    
    
    [_btnCancel setTitle:CLocalize(@"Cancel").uppercaseString forState:UIControlStateNormal];
    [_btnSubmit setTitle:CLocalize(@"Submit").uppercaseString forState:UIControlStateNormal];
    
    
    _arrSelected = [[NSMutableArray alloc] init];
}


+ (id)customReportView:(reportType)type interest:(NSMutableArray *)arr
{
    MIReportPopUpView *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIReportPopUpView" owner:nil options:nil] lastObject];
    
    CViewSetWidth(customView, CScreenWidth);
    CViewSetHeight(customView, CScreenHeight);
    
    customView.reportType = type;
    
    if (customView.reportType == PublicBox)
    {
        customView.lblTitle.text = CLocalize(@"Report");
        customView.arrData = [[NSMutableArray alloc] initWithObjects:
                              @{@"title":@"It's spam", @"selected":@0},
                              @{@"title":@"It's inappropriate", @"selected":@0}, nil];
    }
    else
    {
        customView.lblTitle.text = CLocalize(@"Select interest");
        customView.arrData = [[NSMutableArray alloc] initWithObjects:
                              @{@"title":@"Arts & Entertainment", @"selected":@0},
                              @{@"title":@"Autos & Vehicles", @"selected":@0},
                              @{@"title":@"Beauty & Fitness", @"selected":@0},
                              @{@"title":@"Books & Literature", @"selected":@0},
                              @{@"title":@"Business & industrial", @"selected":@0},
                              @{@"title":@"Computers & Electronics", @"selected":@0}, nil];
        
        customView.arrSelected = arr;
    }
    
    customView.cntblVReportHeight.constant = customView.arrData.count * 37;
    customView.cnViewControlReportHeight.constant = customView.cntblVReportHeight.constant + 124;
    
    [customView.tblVReport registerNib:[UINib nibWithNibName:@"MIReportTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIReportTableViewCell"];
    
    
    if ([customView isKindOfClass:[MIReportPopUpView class]])
        return customView;
    else
        return nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.btnCancel layoutIfNeeded];
    MIGradientLayer *gradientAddFrd = [MIGradientLayer gradientLayerWithFrameForBorder:_btnCancel.layer.bounds];
    [_btnCancel.layer insertSublayer:gradientAddFrd atIndex:0];
    
}

- (IBAction)btnCloseClicked:(id)sender
{
    [self removeFromSuperview];
}

# pragma mark
# pragma mark - UITableview Datasource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MIReportTableViewCell";
    MIReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    NSDictionary *dictData = [_arrData objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = [dictData stringValueForJSON:@"title"];
    
    
    if([_arrSelected containsObject:[dictData valueForKey:@"title"]])
        cell.imgVCheck.image = [UIImage imageNamed:@"check"];
    else
        cell.imgVCheck.image = [UIImage imageNamed:@"unselect_report"];
    
    
    //    if ([[dictData numberForJson:@"selected"] isEqual:@1])
    //    {
    //        if()
    //    }
    //        cell.imgVCheck.image = [UIImage imageNamed:@"check"];
    //    else
    //    {
    //
    //    }
    //        cell.imgVCheck.image = [UIImage imageNamed:@"unselect_report"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:[_arrData objectAtIndex:indexPath.row]];
    
    
    if([_arrSelected containsObject:[dicData valueForKey:@"title"]])
        [_arrSelected removeObject:[dicData valueForKey:@"title"]];
    
    else //(![_arrSelected containsObject:[dicData valueForKey:@"title"]])
        [_arrSelected addObject:[dicData valueForKey:@"title"]];
    
    
    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

@end
