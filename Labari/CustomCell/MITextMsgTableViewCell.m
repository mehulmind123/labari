//
//  MITextMsgTableViewCell.m
//  Labari
//
//  Created by mac-0006 on 27/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MITextMsgTableViewCell.h"

@implementation MITextMsgTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    [_vContent layoutIfNeeded];
    
    _vContent.layer.cornerRadius = 10;
    _vContent.layer.masksToBounds = YES;
}


@end
