//
//  MISettingSearchTableViewCell.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 23/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISettingSearchTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *objSearchVisibility;
@property (strong, nonatomic) IBOutlet UIButton *btnSearchFriend;
@property (strong, nonatomic) IBOutlet UIButton *btnSearchPublic;
@property (strong, nonatomic) IBOutlet UIButton *btnSearchOnlyMe;

@end
