//
//  MIRequestTableViewCell.m
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIRequestTableViewCell.h"

@implementation MIRequestTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.contentView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.contentView.layer.shadowOffset = CGSizeZero;
    self.contentView.layer.shadowOpacity = 0.2f;
    self.contentView.layer.shadowRadius = 2.0f;
    self.contentView.layer.masksToBounds = NO;
}

@end
