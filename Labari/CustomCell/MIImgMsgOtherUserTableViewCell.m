//
//  MIImgMsgOtherUserTableViewCell.m
//  Labari
//
//  Created by mac-0006 on 28/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIImgMsgOtherUserTableViewCell.h"

@implementation MIImgMsgOtherUserTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_imgVMsg layoutIfNeeded];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_imgVMsg.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path  = maskPath.CGPath;
    _imgVMsg.layer.mask = maskLayer;
}

@end
