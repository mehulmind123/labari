//
//  MIInterestCollectionViewCell.m
//  Labari
//
//  Created by mac-0006 on 27/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIInterestCollectionViewCell.h"

@implementation MIInterestCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.cornerRadius = 12;
    self.layer.masksToBounds = YES;

}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setNeedsUpdateConstraints];
    [self layoutIfNeeded];
    
    MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrame:self.layer.bounds];
    [self.layer insertSublayer:gradient atIndex:0];
}
@end
