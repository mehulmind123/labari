//
//  MIAlbumTableViewCell.m
//  Labari
//
//  Created by mac-0006 on 29/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIAlbumTableViewCell.h"
#import "MIAlbumCollectionViewCell.h"

@implementation MIAlbumTableViewCell
{
    NSArray *arrImg;
}
@synthesize collAlbum;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [collAlbum registerNib:[UINib nibWithNibName:@"MIAlbumCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIAlbumCollectionViewCell"];
    [self setCollectionLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setCollectionLayout];
}

- (void)setCollectionLayout
{
    [collAlbum layoutIfNeeded];
    self.cnCollHeight.constant = collAlbum.collectionViewLayout.collectionViewContentSize.height;
}


#pragma mark - UICollectionView Delegate and Datasource
#pragma mark -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"MIAlbumCollectionViewCell";
    MIAlbumCollectionViewCell * cell = [collAlbum dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    cell.imgVAlbum.hidden = YES;
    cell.imgVAddAlbum.hidden = YES;
    
    if([self.strType isEqualToString:@"MyProfile"])
    {
        if(indexPath.row == 0)
            cell.imgVAddAlbum.hidden = NO;
        else
            cell.imgVAlbum.hidden = NO;
    }
    else
        cell.imgVAlbum.hidden = NO;
    
    
    cell.imgVAlbum.image = [UIImage imageNamed:@"1.jpeg"];
    [cell.imgVAlbum enableFullScreenImage];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.item == 0)
    {
        
    }
    else
    {
        
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat size = (CScreenWidth - 10)/3;
    return CGSizeMake(size, size);
}

@end
