//
//  MIMessageTableViewCell.m
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import "MIMessageTableViewCell.h"

@implementation MIMessageTableViewCell
@synthesize imgUserImage,lblUserName,lblMessageText,lblTime;

- (void)awakeFromNib
{
    [super awakeFromNib];
    [imgUserImage.layer setCornerRadius:(imgUserImage.frame.size.height/2)];
    [imgUserImage.layer setMasksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
