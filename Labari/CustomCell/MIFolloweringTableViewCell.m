//
//  MIFolloweringTableViewCell.m
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import "MIFolloweringTableViewCell.h"

@implementation MIFolloweringTableViewCell
@synthesize imgUser,lblUserName,btnUnfollow;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [imgUser.layer setCornerRadius:(imgUser.frame.size.height/2)];
    [imgUser.layer setMasksToBounds:YES];
    
    [btnUnfollow setTitle:CLocalize(@"Unfollow").uppercaseString forState:UIControlStateNormal];
    
    MIGradientLayer *gradientAddFrd = [MIGradientLayer gradientLayerWithFrameForBorder:btnUnfollow.layer.bounds];
    [btnUnfollow.layer insertSublayer:gradientAddFrd atIndex:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
