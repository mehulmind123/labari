//
//  MISidePanelTableViewCell.m
//  Labari
//
//  Created by mac-0005 on 3/21/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MISidePanelTableViewCell.h"

@implementation MISidePanelTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _lblRequestCount.layer.cornerRadius = CViewWidth(_lblRequestCount) / 2;
    _lblRequestCount.layer.masksToBounds = YES;
}

@end
