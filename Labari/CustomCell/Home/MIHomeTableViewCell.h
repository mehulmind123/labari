//
//  MIHomeTableViewCell.h
//  Labari
//
//  Created by mac-0005 on 3/20/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIHomeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewPost;
@property (weak, nonatomic) IBOutlet UIImageView *imgVUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTime;
@property (weak, nonatomic) IBOutlet UIButton *btnUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgVPost;
@property (weak, nonatomic) IBOutlet UILabel *lblPostDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnReport;
@property (weak, nonatomic) IBOutlet UIView *viewCount;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayPause;

@end
