//
//  MIHomeTableViewCell.m
//  Labari
//
//  Created by mac-0005 on 3/20/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIHomeTableViewCell.h"

@implementation MIHomeTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _viewPost.layer.cornerRadius = 7;
    _viewPost.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewPost.layer.shadowOffset = CGSizeZero;
    _viewPost.layer.shadowOpacity = 0.09f;
    _viewPost.layer.shadowRadius = 8.0f;
    
    _imgVPost.layer.shadowColor = CRGBA(0, 0, 0, 0.5).CGColor;
    _imgVPost.layer.shadowOffset = CGSizeZero;
    _imgVPost.layer.shadowOpacity = 0.4;
    _imgVPost.layer.shadowRadius = 7.0;
    
    _imgVUserProfile.layer.cornerRadius = CViewWidth(_imgVUserProfile) / 2;
}

@end
