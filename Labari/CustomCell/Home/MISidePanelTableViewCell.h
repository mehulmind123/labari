//
//  MISidePanelTableViewCell.h
//  Labari
//
//  Created by mac-0005 on 3/21/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISidePanelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestCount;

@end
