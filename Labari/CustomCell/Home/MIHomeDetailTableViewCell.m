//
//  MIHomeDetailTableViewCell.m
//  Labari
//
//  Created by mac-0006 on 29/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIHomeDetailTableViewCell.h"
#import "MIHomeImgCollectionViewCell.h"

@implementation MIHomeDetailTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.collImg registerNib:[UINib nibWithNibName:@"MIHomeImgCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIHomeImgCollectionViewCell"];
    
}



#pragma mark - UICollectionView Delegate and Datasource
#pragma mark -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _arrImg.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"MIHomeImgCollectionViewCell";
    MIHomeImgCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.imgVPost.image = [UIImage imageNamed:[_arrImg objectAtIndex:indexPath.row]];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collImg.frame.size.width,self.collImg.frame.size.height);
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if ([scrollView isEqual:self.collImg])
    {
        int page = round(scrollView.contentOffset.x/scrollView.bounds.size.width);
        
        [self.pageV setCurrentPage:page];
    }
}

@end
