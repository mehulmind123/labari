//
//  MIHomeImgCollectionViewCell.h
//  Labari
//
//  Created by mac-0006 on 29/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIHomeImgCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVPost;

@end
