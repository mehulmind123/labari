//
//  MIHomeImageTableViewCell.m
//  Labari
//
//  Created by mac-0006 on 29/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIHomeImageTableViewCell.h"
#import "MIHomeImgCollectionViewCell.h"

@implementation MIHomeImageTableViewCell
@synthesize arrImg;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    
    _viewPost.layer.cornerRadius = 7;
    _viewPost.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewPost.layer.shadowOffset = CGSizeZero;
    _viewPost.layer.shadowOpacity = 0.09f;
    _viewPost.layer.shadowRadius = 8.0f;
    
    _collImg.layer.shadowColor = CRGBA(0, 0, 0, 0.5).CGColor;
    _collImg.layer.shadowOffset = CGSizeZero;
    _collImg.layer.shadowOpacity = 0.4;
    _collImg.layer.shadowRadius = 7.0;
    
    _imgVUserProfile.layer.cornerRadius = CViewWidth(_imgVUserProfile) / 2;
    
    
    [self.collImg registerNib:[UINib nibWithNibName:@"MIHomeImgCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIHomeImgCollectionViewCell"];
}


#pragma mark - UICollectionView Delegate and Datasource
#pragma mark -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrImg.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"MIHomeImgCollectionViewCell";
    MIHomeImgCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.imgVPost.image = [UIImage imageNamed:[arrImg objectAtIndex:indexPath.row]];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collImg.frame.size.width,self.collImg.frame.size.height);
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if ([scrollView isEqual:self.collImg])
    {
        int page = round(scrollView.contentOffset.x/scrollView.bounds.size.width);
        
        [self.pageV setCurrentPage:page];
    }
}

@end
