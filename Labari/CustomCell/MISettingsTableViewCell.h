//
//  MISettingsTableViewCell.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 21/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISettingsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *objProfileView;
@property (strong, nonatomic) IBOutlet UIView *objSepraterView;
@property (strong, nonatomic) IBOutlet UIImageView *imgIcon;
@property (strong, nonatomic) IBOutlet UIImageView *imgLangSelect;
@property (strong, nonatomic) IBOutlet UIImageView *imgVLang;
@property (strong, nonatomic) IBOutlet UIButton *btnLangSelect;
@property (strong, nonatomic) IBOutlet UISwitch *objNotificationSwitch;
@property (strong, nonatomic) IBOutlet UIImageView *imgArrow;



@end
