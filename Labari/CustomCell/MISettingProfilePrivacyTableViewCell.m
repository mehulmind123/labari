//
//  MISettingProfilePrivacyTableViewCell.m
//  DemoStreamingURL
//
//  Created by mac-0003 on 23/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import "MISettingProfilePrivacyTableViewCell.h"

@implementation MISettingProfilePrivacyTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if(Is_iPhone_6 || Is_iPhone_6_PLUS)
    {
         [_btnFriend setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
         [_btnPublic setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
         [_btnOnlyMe setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    }
   
}



@end
