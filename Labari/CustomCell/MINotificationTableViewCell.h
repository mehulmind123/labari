//
//  MINotificationTableViewCell.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 21/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MINotificationTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *objView;
@property (strong, nonatomic) IBOutlet UILabel *lblNotificationMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIView *objSeperator;

@end
