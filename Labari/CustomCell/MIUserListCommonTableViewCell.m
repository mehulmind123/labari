//
//  MIUserListCommonTableViewCell.m
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIUserListCommonTableViewCell.h"

@implementation MIUserListCommonTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _imgVUser.layer.cornerRadius = CViewWidth(_imgVUser)/2;
    _imgVUser.layer.masksToBounds = YES;
    
    [_btnAccept setTitle:CLocalize(@"Accept") forState:UIControlStateNormal];
    [_btnReject setTitle:CLocalize(@"Reject") forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    MIGradientLayer *gradientReject= [MIGradientLayer gradientLayerWithFrameForBorder:_btnReject.layer.bounds];
    [_btnReject.layer insertSublayer:gradientReject atIndex:0];
    
    
    MIGradientLayer *gradientAccept = [MIGradientLayer gradientLayerWithFrameForBorder:_btnAccept.layer.bounds];
    [_btnAccept.layer insertSublayer:gradientAccept atIndex:0];
    
}

@end
