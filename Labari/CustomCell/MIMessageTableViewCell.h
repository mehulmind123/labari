//
//  MIMessageTableViewCell.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIMessageTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgUserImage;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblMessageText;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;

@end
