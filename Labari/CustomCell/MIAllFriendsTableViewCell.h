//
//  MIAllFriendsTableViewCell.h
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAllFriendsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgVUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UIButton *btnUnFriend;

@end
