//
//  MISettingProfilePrivacyTableViewCell.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 23/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISettingProfilePrivacyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *objViewProfilePrivacy;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacyTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnFriend;
@property (weak, nonatomic) IBOutlet UIButton *btnPublic;
@property (weak, nonatomic) IBOutlet UIButton *btnOnlyMe;


@end
