//
//  MIFolloweringTableViewCell.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIGradientLayer.h"
#import "Constants.h"

@interface MIFolloweringTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UIButton *btnUnfollow;
@property (strong, nonatomic) IBOutlet UIButton *btnProfile;

@end
