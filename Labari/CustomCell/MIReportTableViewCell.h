//
//  MIReportTableViewCell.h
//  Labari
//
//  Created by mac-0005 on 3/22/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIReportTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgVCheck;

@end
