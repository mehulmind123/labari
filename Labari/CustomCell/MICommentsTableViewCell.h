//
//  MICommentsTableViewCell.h
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICommentsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgVUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentText;
@property (strong, nonatomic) IBOutlet UIButton *btnReply;
@property (strong, nonatomic) IBOutlet UIView *viewSeprater;

@end
