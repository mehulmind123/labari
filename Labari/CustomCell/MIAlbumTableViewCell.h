//
//  MIAlbumTableViewCell.h
//  Labari
//
//  Created by mac-0006 on 29/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAlbumTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UICollectionView *collAlbum;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnCollHeight;
@property (strong, nonatomic) NSString *strType;
@end
