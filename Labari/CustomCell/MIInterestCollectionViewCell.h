//
//  MIInterestCollectionViewCell.h
//  Labari
//
//  Created by mac-0006 on 27/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIInterestCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end
