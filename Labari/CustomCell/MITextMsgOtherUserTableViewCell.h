//
//  MITextMsgOtherUserTableViewCell.h
//  Labari
//
//  Created by mac-0006 on 28/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITextMsgOtherUserTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *vContent;
@property (strong, nonatomic) IBOutlet UILabel *lblMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;

@end
