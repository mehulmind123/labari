//
//  AppDelegate.h
//  Labari
//
//  Created by mac-0005 on 3/20/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <MediaPlayer/MediaPlayer.h>
#import "TWTSideMenuViewController.h"
#import "MISidePanelViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, TWTSideMenuViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (nonatomic, strong) TWTSideMenuViewController *sideMenuViewController;

@property (nonatomic, strong) MPMoviePlayerController *videoPlayer;


- (void)saveContext;



-(void)openMenuViewcontroller:(UIViewController *)viewController animated:(BOOL)animated;
- (void)resignKeyboard;


- (void)setWindowRootViewController:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed;

- (NSAttributedString *)setAttributedString:(NSString *)string andAlign:(NSTextAlignment)alignment;

- (MPMoviePlayerController *)playVideoWithResource:(NSString *)resource andType:(NSString *)type;

@end

