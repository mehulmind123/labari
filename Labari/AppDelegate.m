//
//  AppDelegate.m
//  Labari
//
//  Created by mac-0005 on 3/20/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "AppDelegate.h"

#import "MISelectLanguageViewController.h"
#import "MILoginViewController.h"
#import "MIHomeViewController.h"


@interface AppDelegate ()
{
    MISidePanelViewController *sidePanelVC;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    
    self.videoPlayer = [[MPMoviePlayerController alloc] init];
    
    [[UINavigationBar appearance] setBackgroundImage: [UIImage new]
                                       forBarMetrics: UIBarMetricsDefault];
    
    [UINavigationBar appearance].shadowImage = [UIImage new];
    
    
    MISelectLanguageViewController *languageVC = [[MISelectLanguageViewController alloc] initWithNibName:@"MISelectLanguageViewController" bundle:nil];
//    [self openMenuViewcontroller:languageVC animated:NO];
    [self setWindowRootViewController:[[UINavigationController alloc] initWithRootViewController:languageVC] animated:NO completion:nil];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Labari"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}




# pragma mark
# pragma mark - General Method

- (void)setWindowRootViewController:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed
{
    [UIView transitionWithView:self.window
                      duration:animated?0.5:0.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         self.window.rootViewController = vc;
         [UIView setAnimationsEnabled:oldState];
         
         
     } completion:^(BOOL finished)
     {
         if (completed)
             completed(finished);
     }];
}


#pragma mark - Helper Method
#pragma mark -

- (NSAttributedString *)setAttributedString:(NSString *)string andAlign:(NSTextAlignment)alignment
{
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:4];
    style.alignment = alignment;
    [attrString addAttribute:NSParagraphStyleAttributeName
                       value:style
                       range:NSMakeRange(0, attrString.length)];
    return attrString;
}


- (MPMoviePlayerController *)playVideoWithResource:(NSString *)resource andType:(NSString *)type
{
    NSString *videoFileName = [[NSBundle mainBundle] pathForResource:resource ofType:type inDirectory:nil];

  //  self.videoPlayer.contentURL =  [NSURL fileURLWithPath:videoFileName];
    MPMoviePlayerController *moviePlayer =  [[MPMoviePlayerController alloc] initWithContentURL: [NSURL fileURLWithPath:videoFileName]];

    [moviePlayer setShouldAutoplay:YES];
    [moviePlayer prepareToPlay];
    [moviePlayer setFullscreen:NO];
    [moviePlayer setRepeatMode:YES];
    [moviePlayer setControlStyle: MPMovieControlStyleNone];
    [moviePlayer setScalingMode: MPMovieScalingModeAspectFill];
    
    return moviePlayer;
}


#pragma mark
#pragma mark - SidePanelViewController

-(void)openMenuViewcontroller:(UIViewController *)viewController animated:(BOOL)animated
{
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];

    
    if (!_sideMenuViewController)
    {
        if (!sidePanelVC)
            sidePanelVC = [[MISidePanelViewController alloc] initWithNibName:@"MISidePanelViewController" bundle:nil];
        
        _sideMenuViewController = [[TWTSideMenuViewController alloc] initWithMenuViewController:sidePanelVC mainViewController:navigationController];
        
        _sideMenuViewController.shadowColor = [UIColor blackColor];
        _sideMenuViewController.edgeOffset = (UIOffset) { .horizontal = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 24.0f : 0.0f };
        _sideMenuViewController.zoomScale = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 0.7050f : 0.85f;
        _sideMenuViewController.delegate = self;
        
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarMenuClicked:)];
        
        [self setWindowRootViewController:_sideMenuViewController animated:animated completion:nil];

    }
    else
    {
        [_sideMenuViewController setMainViewController:navigationController animated:animated closeMenu:YES];
    }
}

-(void)leftBarMenuClicked:(UIBarButtonItem *)sender
{
    [self resignKeyboard];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (void)resignKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}


@end
