//
//  MIAddNewRequestViewController.h
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAddNewRequestViewController : UIViewController
{
    IBOutlet UIScrollView *scrollvRequest;
    IBOutlet UIView *viewMain;
    
    IBOutlet MIGenericTextField *txtWantToDo;
    IBOutlet MIGenericTextField *txtSelectCity;
    IBOutlet MIGenericTextField *txtWantToBuy;
    IBOutlet MIGenericTextField *txtSelectCountry;
    IBOutlet MIGenericTextField *txtPrice;
    
    IBOutlet UITextView *txtVDescription;
    IBOutlet UIButton *btnUploadImage;
    IBOutlet UIButton *btnSubmit;
    
    IBOutlet UILabel *lblPrice;
    IBOutlet UILabel *lblFullDesc;
    IBOutlet UILabel *lblUploadImg;
}


- (IBAction)btnUploadImageClicked:(id)sender;
- (IBAction)btnSubmitClicked:(id)sender;

@end
