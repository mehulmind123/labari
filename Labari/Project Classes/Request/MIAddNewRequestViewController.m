//
//  MIAddNewRequestViewController.m
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIAddNewRequestViewController.h"

@interface MIAddNewRequestViewController ()

@end

@implementation MIAddNewRequestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Add New Requests");
    
    
    [txtWantToDo setPlaceholder:CLocalize(@"What do you want to do?")];
    [txtWantToBuy setPlaceholder:CLocalize(@"What do you want to__?")];
    [txtSelectCountry setPlaceholder:CLocalize(@"Select Country")];
    [txtSelectCity setPlaceholder:CLocalize(@"Select City")];
    [txtWantToBuy setPlaceHolderColor:ColorGray_C6DAEC];
    
    
    [lblPrice setText:CLocalize(@"Price")];
    [lblFullDesc setText:CLocalize(@"Enter Full Description")];
    [lblUploadImg setText:CLocalize(@"Upload Image")];
    [btnSubmit setTitle:CLocalize(@"Submit") forState:UIControlStateNormal];
    
    [txtWantToDo setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(30, 15)];
    [txtSelectCountry setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(30, 15)];
    [txtSelectCity setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(30, 15)];
    
    txtVDescription.layer.cornerRadius = 5;
    btnUploadImage.layer.cornerRadius = 5;
    
    
    
    
    [txtWantToDo setPickerData:@[@"Test1",@"Test2",@"Test3"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    [txtWantToBuy setPickerData:@[@"Test1",@"Test2",@"Test3"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    [txtSelectCountry setPickerData:@[@"India",@"USA",@"Japan"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    [txtSelectCity setPickerData:@[@"Ahmedabad",@"Vadodara",@"Surat"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnUploadImageClicked:(id)sender
{
    [self selectImageWithEditing:YES animation:YES completion:^(UIImage *image)
     {
         if (image)
         {
             [btnUploadImage setImage:image forState:UIControlStateSelected];
         }
     }];
}

- (IBAction)btnSubmitClicked:(id)sender {
}
@end
