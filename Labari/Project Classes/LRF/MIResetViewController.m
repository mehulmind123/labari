//
//  MIResetViewController.m
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIResetViewController.h"
#import "MILoginViewController.h"

@interface MIResetViewController ()

@end

@implementation MIResetViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initailize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
}




#pragma mark - General Method
#pragma mark -

- (void)initailize
{
    self.title = CLocalize(@"Reset Password");
    
    [lblResetPW setText:CLocalize(@"Reset Your Password")];
    [txtVerifyCode setPlaceholder:CLocalize(@"Verification Code")];
    [txtNewPW setPlaceholder:CLocalize(@"New Password")];
    [txtConfirmPW setPlaceholder:CLocalize(@"Confirm Password")];
    [btnSubmit setTitle:CLocalize(@"Submit") forState:UIControlStateNormal];
    [txtVerifyCode setPlaceHolderColor:ColorGray_C6DAEC];
    
    lblText.attributedText = [appDelegate setAttributedString:[NSString stringWithFormat:@"%@\n%@",CLocalize(@"Enter the verification code you received"),CLocalize(@"and create new password.")] andAlign:NSTextAlignmentCenter];
    
    // lblText.attributedText = [appDelegate setAttributedString:@"Enter the verification code you received\nand create new password." andAlign:NSTextAlignmentCenter];
}



#pragma mark - Button Action
#pragma mark -

- (IBAction)btnSubmitClicked:(id)sender
{
    if(![txtVerifyCode.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Verification code can not be blank.") withTitle:@""];
    }
    else if (![txtNewPW.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password field can not be blank.") withTitle:@""];
    }
    else if ([txtNewPW.text length] < 6)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password must be minimum 6 characters alphanumeric.") withTitle:@""];
    }
    else if (![txtConfirmPW.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Confirm password field can not be blank.") withTitle:@""];
    }
    else if (![txtConfirmPW.text isEqualToString:txtNewPW.text])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password and Confirm password combination does not match.") withTitle:@""];
    }
    else
    {
        MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
    
}
@end
