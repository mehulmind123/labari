//
//  MILoginViewController.h
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MILoginViewController : ParentViewController
{
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    
    IBOutlet UIButton *btnForgotPW;
    IBOutlet UIButton *btnConfirm;
    
    IBOutlet UILabel *lblSignup;
}
@end
