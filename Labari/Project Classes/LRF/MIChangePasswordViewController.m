//
//  MIChangePasswordViewController.m
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIChangePasswordViewController.h"
#import "MILoginViewController.h"

@interface MIChangePasswordViewController ()

@end

@implementation MIChangePasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initailize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - General Method
#pragma mark -

- (void)initailize
{
    
    self.title = CLocalize(@"Change Password");
    
    [lblChangePW setText:CLocalize(@"Change Your Password?")];
    [txtOldPW setPlaceholder:CLocalize(@"Old password")];
    [txtNewPW setPlaceholder:CLocalize(@"New Password")];
    [txtConfirmPW setPlaceholder:CLocalize(@"Confirm Password")];
    [btnSubmit setTitle:CLocalize(@"Submit") forState:UIControlStateNormal];
    [txtOldPW setPlaceHolderColor:ColorGray_C6DAEC];
    
    lblText.attributedText = [appDelegate setAttributedString:[NSString stringWithFormat:@"%@\n%@",CLocalize(@"Enter the old password and create"),CLocalize(@"new password & re-confirm the password.")] andAlign:NSTextAlignmentCenter];
    
}



#pragma mark - Button Action
#pragma mark -

- (IBAction)btnSubmitClicked:(id)sender
{
    if (![txtOldPW.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Old password field can not be blank.") withTitle:@""];
    }
    else if ([txtOldPW.text length] < CPasswordLength)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password must be minimum 6 characters alphanumeric.") withTitle:@""];
    }
    else if (![txtNewPW.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"New password field can not be blank.") withTitle:@""];
    }
    else if ([txtNewPW.text length] < CPasswordLength)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password must be minimum 6 characters alphanumeric.") withTitle:@""];
    }
    else if (![txtConfirmPW.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Confirm password field can not be blank.") withTitle:@""];
    }
    else if (![txtConfirmPW.text isEqualToString:txtNewPW.text])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password and Confirm password combination does not match.") withTitle:@""];
    }
    else
    {
        MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
}

@end
