//
//  MILoginViewController.m
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MILoginViewController.h"
#import "MIForgotPWViewController.h"
#import "MIRegisterViewController.h"
#import "MIHomeViewController.h"
#import "MISettingsViewController.h"

@interface MILoginViewController ()

@end

@implementation MILoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initailize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}


#pragma mark - General Method
#pragma mark -

- (void)initailize
{
    [txtEmail setPlaceholder:CLocalize(@"Email or Mobile")];
    [txtPassword setPlaceholder:CLocalize(@"Password")];
    [btnForgotPW setTitle:CLocalize(@"Forgot password") forState:UIControlStateNormal];
    [btnConfirm setTitle:CLocalize(@"Confirm") forState:UIControlStateNormal];
    
    [txtEmail setPlaceHolderColor:ColorGray_C6DAEC];
    [self setAttributedUserText];
    
    if (IS_IPHONE_SIMULATOR)
    {
        txtEmail.text = @"ruturaj.mindinventory@gmail.com";
        txtPassword.text = @"123456";
    }
}



#pragma mark - Attributed Method
#pragma mark -

- (void)setAttributedUserText
{
    NSString *string = CLocalize(@"Don't have an account? Sign up");
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedText addAttributes: @{NSFontAttributeName: CFontAvenirLTStd55Roman(13), NSForegroundColorAttributeName:ColorTurquoise_19bbd5} range: [string rangeOfString:@"Sign up"]];
    
    lblSignup.attributedText = attributedText;
}



#pragma mark - Button Action
#pragma mark -


- (IBAction)btnForgotPasswordClicked:(id)sender
{
    MIForgotPWViewController *forgotPwVC = [[MIForgotPWViewController alloc] initWithNibName:@"MIForgotPWViewController" bundle:nil];
    [self.navigationController pushViewController:forgotPwVC animated:YES];
}

- (IBAction)btnSignupClicked:(id)sender
{
    MIRegisterViewController *registerVC = [[MIRegisterViewController alloc] initWithNibName:@"MIRegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (IBAction)btnConfrimClicked:(id)sender
{
//    if(![txtEmail.text isBlankValidationPassed])
//    {
//        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Email or Mobile number field can not be blank.") withTitle:@""];
//    }
//    else if (![txtEmail.text isValidEmailAddress])
//    {
//        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Please enter valid email address.") withTitle:@""];
//    }
//    else if (![txtPassword.text isBlankValidationPassed])
//    {
//        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password field can not be blank.") withTitle:@""];
//    }
//    else
//    {
        MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
        [appDelegate openMenuViewcontroller:homeVC animated:YES];
//   }

}
@end
