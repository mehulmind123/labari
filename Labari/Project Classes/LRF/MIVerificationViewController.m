//
//  MIVerificationViewController.m
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIVerificationViewController.h"
#import "MILoginViewController.h"

@interface MIVerificationViewController ()

@end

@implementation MIVerificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initailize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
}




#pragma mark - General Method
#pragma mark -

- (void)initailize
{
    self.title = CLocalize(@"Verification");
    
    [txtVerifyCode setPlaceholder:CLocalize(@"Verification Code")];
    [txtVerifyCode setPlaceHolderColor:ColorGray_C6DAEC];
    [btnResendCode setTitle:CLocalize(@"Resend Code") forState:UIControlStateNormal];
    [btnSubmit setTitle:CLocalize(@"Submit") forState:UIControlStateNormal];
    
    
    lblText.attributedText = [appDelegate setAttributedString:[NSString stringWithFormat:@"%@\n%@",CLocalize(@"Enter verification code you received on"),CLocalize(@"your Mobile Number to activate your account.")] andAlign:NSTextAlignmentCenter];
}



#pragma mark - Button Action
#pragma mark -

- (IBAction)btnResendCodeClicked:(id)sender
{
    
}

- (IBAction)btnSubmitClicked:(id)sender
{
    if(![txtVerifyCode.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Verification code can not be blank.") withTitle:@""];
    }
    else
    {
         MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
         [self.navigationController pushViewController:loginVC animated:NO];
    }

}
@end
