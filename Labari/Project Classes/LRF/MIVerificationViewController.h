//
//  MIVerificationViewController.h
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIVerificationViewController : ParentViewController
{
    IBOutlet UITextField *txtVerifyCode;
    IBOutlet UILabel *lblText;
    IBOutlet UIButton *btnResendCode;
    IBOutlet UIButton *btnSubmit;
}

@end
