//
//  MIForgotPWViewController.h
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIForgotPWViewController : ParentViewController
{
    IBOutlet UITextField *txtMobile;
    IBOutlet UILabel *lblText;
    IBOutlet UILabel *lblForgotPWText;
    IBOutlet UIButton *btnSendOTP;
}
@end
