//
//  MIRegisterViewController.m
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIRegisterViewController.h"
#import "MIVerificationViewController.h"
#import "MIAboutUsViewController.h"
#import "MIReportPopUpView.h"

@interface MIRegisterViewController ()
{
    MIReportPopUpView *reportPopUp;
    NSMutableArray *arrInterest;
}
@end

@implementation MIRegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initailize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}



#pragma mark - General Method
#pragma mark -

- (void)initailize
{
    self.title = CLocalize(@"Register");
    
  
    [txtFirstName setPlaceholder:CLocalize(@"First name")];
    [txtLastName setPlaceholder:CLocalize(@"Last name")];
    [txtEmail setPlaceholder:CLocalize(@"Email")];
    [txtMobile setPlaceholder:CLocalize(@"Mobile number")];
    [txtConfirmPassword setPlaceholder:CLocalize(@"Confirm Password")];
    [txtPassword setPlaceholder:CLocalize(@"Password")];
    [txtDOB setPlaceholder:CLocalize(@"Date of Birth")];
    [txtCity setPlaceholder:CLocalize(@"City")];
    [txtCountry setPlaceholder:CLocalize(@"Country")];
    [txtInterest setPlaceholder:CLocalize(@"Select interest")];
    
    [btnMaleTitle setTitle:CLocalize(@"Male") forState:UIControlStateNormal];
    [btnFemaleTitle setTitle:CLocalize(@"Female") forState:UIControlStateNormal];
    [btnSubmit setTitle:CLocalize(@"Submit") forState:UIControlStateNormal];
    [self setAttributedUserText];
    
    [txtFirstName setPlaceHolderColor:ColorGray_C6DAEC];
    [txtLastName setPlaceHolderColor:ColorGray_C6DAEC];
    [txtEmail setPlaceHolderColor:ColorGray_C6DAEC];
    [txtMobile setPlaceHolderColor:ColorGray_C6DAEC];
    [txtInterest setPlaceHolderColor:ColorGray_C6DAEC];
    
    
    
    arrInterest = [[NSMutableArray alloc] init];
    
    [txtInterest setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(40, 15)];
    
    NSDate *date;
    [txtDOB setDatePickerWithDateFormat:@"dd-MM-yyy" defaultDate:date changed:^(NSDate *date) {}];
    [txtDOB setDatePickerMode:UIDatePickerModeDate];
    
   
    btnUpload.layer.cornerRadius = CViewWidth(btnUpload)/2;
    btnUpload.layer.masksToBounds = YES;
    
   
    btnMale.selected = YES;
    btnMaleTitle.selected = YES;
    
   
    [txtInterest setPickerData:@[@"Test1",@"Test2",@"Test3"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];

}



#pragma mark - Attributed Method
#pragma mark -

- (void)setAttributedUserText
{
    
    NSString *str1=  CLocalize(@"By creating an account, I accept the Terms of Use and Privacy Policy");
    NSDictionary *attributes = @{NSFontAttributeName: CFontAvenirLTStd85Heavy(13), NSForegroundColorAttributeName:ColorGray_C6DAEC}; 
    
    lblTermsCondition.attributedText = [[NSAttributedString alloc]initWithString:str1 attributes:attributes];
    
    [lblTermsCondition setLinkForSubstring:@"Terms of Use" withLinkHandler:^(FRHyperLabel *label, NSString *substring)
    {
        MIAboutUsViewController *aboutUsVC = [[MIAboutUsViewController alloc] initWithNibName:@"MIAboutUsViewController" bundle:nil];
        aboutUsVC.strTitle = @"Terms of Use";
        [self.navigationController pushViewController:aboutUsVC animated:YES];
    }];
    
    
    [lblTermsCondition setLinkForSubstring:@"Privacy Policy" withLinkHandler:^(FRHyperLabel *label, NSString *substring)
     {
         MIAboutUsViewController *aboutUsVC = [[MIAboutUsViewController alloc] initWithNibName:@"MIAboutUsViewController" bundle:nil];
         aboutUsVC.strTitle = @"Privacy Policy";
         [self.navigationController pushViewController:aboutUsVC animated:YES];
     }];
}




#pragma mark - Button Action
#pragma mark -

- (IBAction)btnSelectInterestClicked:(id)sender
{
    reportPopUp = [MIReportPopUpView customReportView:InterestBox interest:arrInterest];
    
    //.....Clicked events
    
    [reportPopUp.btnSubmit touchUpInsideClicked:^{
       
        if(reportPopUp.arrSelected.count < 1)
        {
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CSelectMinInterest withTitle:@""];
            return;
        }
        else if(reportPopUp.arrSelected.count > 3)
        {
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CSelectMaxInterest withTitle:@""];
            return;
        }
            
            
        arrInterest = reportPopUp.arrSelected;
        txtInterest.text = [arrInterest componentsJoinedByString:@","];
        [reportPopUp removeFromSuperview];
    }];
    
    [appDelegate.window addSubview:reportPopUp];
}

- (IBAction)btnUploadImage:(id)sender
{
    [self selectImageWithEditing:YES animation:YES completion:^(UIImage *image)
     {
         if (image)
         {
             [btnUpload setImage:image forState:UIControlStateNormal];
         }
     }];
}

- (IBAction)selectGender:(UIButton *)sender
{
    btnMale.selected =  btnFemale.selected = btnMaleTitle.selected = btnFemaleTitle.selected = NO;
    
    if(sender.tag == 0)
    {
        btnMale.selected = YES;
        btnMaleTitle.selected = YES;
    }
    else
    {
        btnFemale.selected = YES;
        btnFemaleTitle.selected = YES;
    }
}

- (IBAction)acceptTermsCondition:(UIButton *)sender
{
    if(sender.selected)
        sender.selected = NO;
    else
        sender.selected = YES;
}

- (IBAction)btnTermsConditionClicked:(id)sender
{
//    MIAboutUsViewController *aboutUsVC = [[MIAboutUsViewController alloc] initWithNibName:@"MIAboutUsViewController" bundle:nil];
//    aboutUsVC.strTitle = @"Terms and Conditions";
//    [self.navigationController pushViewController:aboutUsVC animated:YES];
}

- (IBAction)btnSubmitClicked:(id)sender
{
    
    if(![txtFirstName.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"First name can not be blank.") withTitle:@""];
    }
    else if (![txtLastName.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Last name can not be blank.") withTitle:@""];
    }
    else if (![txtEmail.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Email can not be blank.") withTitle:@""];
    }
    else if(![txtEmail.text isValidEmailAddress])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Please enter valid email address.") withTitle:@""];
    }
    else if (![txtMobile.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Mobile number field can not be blank.") withTitle:@""];
    }
    else if([txtMobile.text length] < 10 || [txtMobile.text length] > 10)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Please enter valid mobile number") withTitle:@""];
    }
    else if (![txtPassword.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password field can not be blank.") withTitle:@""];
    }
    else if([txtPassword.text length] < CPasswordLength)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password must be minimum 6 characters alphanumeric.") withTitle:@""];
    }
    else if (![txtConfirmPassword.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Confirm password field can not be blank.") withTitle:@""];
    }
    else if (![txtConfirmPassword.text isEqualToString:txtPassword.text])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Password and Confirm password combination does not match.") withTitle:@""];
    }
    else if (![txtInterest.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Please select atleast one Interest.") withTitle:@""];
    }
    else if (!btnAccept.selected)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Please accept terms and conditions.") withTitle:@""];
    }
    else
    {
        MIVerificationViewController *verifyVC = [[MIVerificationViewController alloc] initWithNibName:@"MIVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:verifyVC animated:YES];
    }
}


@end
