//
//  MIResetViewController.h
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIResetViewController : ParentViewController
{
    
    IBOutlet UITextField *txtVerifyCode;
    IBOutlet UITextField *txtNewPW;
    IBOutlet UITextField *txtConfirmPW;
    IBOutlet UILabel *lblText;
    IBOutlet UILabel *lblResetPW;
    IBOutlet UIButton *btnSubmit;
    
}
@end
