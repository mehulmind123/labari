//
//  MIForgotPWViewController.m
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIForgotPWViewController.h"
#import "MIResetViewController.h"

@interface MIForgotPWViewController ()

@end

@implementation MIForgotPWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initailize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - General Method
#pragma mark -

- (void)initailize
{
    self.title = CLocalize(@"Forgot password");
    
    [txtMobile setPlaceholder:CLocalize(@"Mobile number")];
    [lblForgotPWText setText:CLocalize(@"Forgot your password")];
    [btnSendOTP setTitle:CLocalize(@"Send OTP") forState:UIControlStateNormal];
    

    
    NSString *str;
    if(Is_iPhone_5 || IS_IPOD)
        str = [NSString stringWithFormat:@"%@\n%@\n%@",CLocalize(@"Enter your Mobile Number and we will"),CLocalize(@"send you OTP Code to reset"),CLocalize(@"your password.")];
    else
        str = [NSString stringWithFormat:@"%@\n%@",CLocalize(@"Enter your Mobile Number and we will send"),CLocalize(@"you OTP Code to reset your password.")];
    
    lblText.attributedText = [appDelegate setAttributedString:str andAlign:NSTextAlignmentCenter];
}



#pragma mark - Button Action
#pragma mark -

- (IBAction)btnSendOTPClicked:(id)sender
{
    if(![txtMobile.text isBlankValidationPassed])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Mobile number field can not be blank.") withTitle:@""];
    }
    else if([txtMobile.text length] < 10 ||  [txtMobile.text length] > 10)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CLocalize(@"Please enter valid mobile number") withTitle:@""];
    }
    else
    {
        MIResetViewController *resetVC = [[MIResetViewController alloc] initWithNibName:@"MIResetViewController" bundle:nil];
        [self.navigationController pushViewController:resetVC animated:YES];
    }
}

@end
