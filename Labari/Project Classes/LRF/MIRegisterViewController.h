//
//  MIRegisterViewController.h
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRHyperLabel.h"

@interface MIRegisterViewController : ParentViewController
{
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtMobile;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtConfirmPassword;
    IBOutlet UITextField *txtDOB;
    IBOutlet UITextField *txtCountry;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtInterest;
    
    IBOutlet UIButton *btnUpload;
    IBOutlet UIButton *btnMale;
    IBOutlet UIButton *btnFemale;
    IBOutlet UIButton *btnMaleTitle;
    IBOutlet UIButton *btnFemaleTitle;
    IBOutlet UIButton *btnAccept;
    IBOutlet UIButton *btnSubmit;
    
    IBOutlet FRHyperLabel *lblTermsCondition;
}

@end
