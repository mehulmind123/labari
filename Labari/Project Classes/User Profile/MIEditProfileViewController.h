//
//  MIEditProfileViewController.h
//  Labari
//
//  Created by mac-0006 on 24/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    FromsideMenu,
    OtherScreen
}EditProfile;

@interface MIEditProfileViewController : ParentViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UIView *vProfile;
    IBOutlet UIView *vGeneralInfo;
    IBOutlet UIView *vPrivateInfo;
    IBOutlet UIView *vOtherInfo;
    
    IBOutlet UILabel *lblProfilePicture;
    IBOutlet UILabel *lblGeneralInfo;
    IBOutlet UILabel *lblPrivateInfo;
    IBOutlet UILabel *lblOtherInfo;
    IBOutlet UILabel *lblMain;
    IBOutlet UILabel *lblCover;
    IBOutlet UILabel *lblWallPaper;
    IBOutlet UILabel *lblFirstName;
    IBOutlet UILabel *lblLastName;
    IBOutlet UILabel *lblEmail;
    IBOutlet UILabel *lblBirthDate;
    IBOutlet UILabel *lblGender;
    IBOutlet UILabel *lblCountry;
    IBOutlet UILabel *lblCity;
    IBOutlet UILabel *lblMobile;
    IBOutlet UILabel *lblInterest;
    IBOutlet UILabel *lblWebsite;
    IBOutlet UILabel *lblAboutMe;
    
    
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtMobile;
    IBOutlet UITextField *txtBirthdate;
    IBOutlet UITextField *txtGender;
    IBOutlet UITextField *txtCountry;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtWebsite;
    IBOutlet UITextView *txtVAboutMe;
    
    IBOutlet UIImageView *imgVMain;
    IBOutlet UIImageView *imgVCover;
    IBOutlet UIImageView *imgVWallpaper;
    
    IBOutlet UIButton *btnSave;
    
    IBOutlet UICollectionView *collVInterest;
}

@property (nonatomic , assign) EditProfile editProfileType;

@end
