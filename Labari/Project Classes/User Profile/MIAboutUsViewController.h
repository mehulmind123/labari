//
//  MIAboutUsViewController.h
//  Labari
//
//  Created by mac-0006 on 01/04/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAboutUsViewController : ParentViewController
{
    IBOutlet UIWebView *webView;
}

@property(strong, nonatomic)NSString *strTitle;

@end
