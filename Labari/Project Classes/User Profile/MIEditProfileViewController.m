//
//  MIEditProfileViewController.m
//  Labari
//
//  Created by mac-0006 on 24/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIEditProfileViewController.h"
#import "MIInterestCollectionViewCell.h"
#import "MIReportPopUpView.h"

@interface MIEditProfileViewController ()
{
    NSMutableArray *arrInterest;
    MIReportPopUpView *reportPopUp;
}
@end

@implementation MIEditProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Edit Profile");
    
    
    [lblProfilePicture setText:CLocalize(@"Profile picture").uppercaseString];
    [lblPrivateInfo setText:CLocalize(@"Private Info").uppercaseString];
    [lblOtherInfo setText:CLocalize(@"Other Info").uppercaseString];
    [lblGeneralInfo setText:CLocalize(@"General Info").uppercaseString];
    
    [lblMain setText:CLocalize(@"Main")];
    [lblCover setText:CLocalize(@"Cover")];
    [lblWallPaper setText:CLocalize(@"Wallpaper")];
    
    [lblFirstName setText:CLocalize(@"First name")];
    [lblLastName setText:CLocalize(@"Last name")];
    [lblEmail setText:CLocalize(@"Email")];
    [lblMobile setText:CLocalize(@"Mobile number")];
    [lblGender setText:CLocalize(@"Gender")];
    [lblBirthDate setText:CLocalize(@"birthdate")];
    [lblCountry setText:CLocalize(@"Country")];
    [lblCity setText:CLocalize(@"City")];
    [lblInterest setText:CLocalize(@"Interest")];
    [lblWebsite setText:CLocalize(@"Website")];
    [lblAboutMe setText:CLocalize(@"About me")];

    [btnSave setTitle:CLocalize(@"Save") forState:UIControlStateNormal];
    
    if(_editProfileType == OtherScreen)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBackClicked)];
    }
    else
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBackClicked)];
    }
    
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"profile_change_password"] style:UIBarButtonItemStylePlain target:self action:@selector(btnEditProfileClicked)];
    
    
    [self setViewShadow:vProfile];
    [self setViewShadow:vPrivateInfo];
    [self setViewShadow:vGeneralInfo];
    [self setViewShadow:vOtherInfo];
    
    
    imgVMain.layer.cornerRadius = CViewWidth(imgVMain)/2;
    imgVWallpaper.layer.cornerRadius = CViewWidth(imgVWallpaper)/2;
    imgVCover.layer.cornerRadius = CViewWidth(imgVCover)/2;
    
    imgVCover.layer.masksToBounds = YES;
    imgVWallpaper.layer.masksToBounds = YES;
    imgVMain.layer.masksToBounds = YES;
    
    [txtBirthdate setDatePickerWithDateFormat:@"dd-MM-yyy" defaultDate:[NSDate date]];
    [txtBirthdate setDatePickerMode:UIDatePickerModeDate];
    
    [self setTextFieldEnable:NO];
    
    arrInterest = [[NSMutableArray alloc] initWithArray:@[@"Arts & Entertainment",
                                                          @"Books & Literature",
                                                          @"Computers & Electronics"]] ;
    
    [collVInterest registerNib:[UINib nibWithNibName:@"MIInterestCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIInterestCollectionViewCell"];
}


- (void)setViewShadow:(UIView *)view
{
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(3, 1);
    view.layer.shadowOpacity = 0.1f;
    view.layer.shadowRadius = 3.0f;
}


- (void)setTextFieldEnable:(BOOL)isEnbale
{
    txtCity.enabled = isEnbale;
    txtEmail.enabled = isEnbale;
    txtGender.enabled = isEnbale;
    txtMobile.enabled = isEnbale;
    txtCountry.enabled = isEnbale;
    txtWebsite.enabled = isEnbale;
    txtLastName.enabled = isEnbale;
    txtVAboutMe.editable = isEnbale;
    txtBirthdate.enabled = isEnbale;
    txtFirstName.enabled = isEnbale;
    btnSave.enabled = isEnbale;
}

#pragma mark - UICollectionView Delegate and Datasource
#pragma mark -


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrInterest.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"MIInterestCollectionViewCell";
    MIInterestCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblTitle.text = [arrInterest objectAtIndex:indexPath.row];
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize calCulateSizze =[(NSString*)[arrInterest objectAtIndex:indexPath.row] sizeWithAttributes:NULL];
    calCulateSizze.width = calCulateSizze.width+40;
    calCulateSizze.height = collVInterest.frame.size.height;
    return calCulateSizze;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    reportPopUp = [MIReportPopUpView customReportView:InterestBox interest:arrInterest];
    
    //.....Clicked events
    
    [reportPopUp.btnSubmit touchUpInsideClicked:^{
        
        if(reportPopUp.arrSelected.count < 1)
        {
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CSelectMinInterest withTitle:@""];
            return;
        }
        else if(reportPopUp.arrSelected.count > 3)
        {
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CSelectMaxInterest withTitle:@""];
            return;
        }
        
        
        arrInterest = reportPopUp.arrSelected;
        [collVInterest reloadData];
        [reportPopUp removeFromSuperview];
    }];
    
    [appDelegate.window addSubview:reportPopUp];
}


#pragma mark - Action Events
#pragma mark -

- (IBAction)btnEditProfilePic:(UIButton *)sender
{
    [self selectImageWithEditing:YES animation:YES completion:^(UIImage *image)
     {
         if (image)
         {
             switch (sender.tag) {
                 case 0:
                 {
                     imgVMain.image = image;
                     break;
                 }
                 case 1:
                 {
                     imgVCover.image = image;
                     break;
                 }
                 case 2:
                 {
                     imgVWallpaper.image = image;
                     break;
                 }
             }
         }
     }];
}

- (IBAction)btnSaveClicked:(id)sender
{
    [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:@"Profile edited successfully."];
}

- (void)btnEditProfileClicked
{
     [self setTextFieldEnable:YES];
}

- (void)btnBackClicked
{
    if(_editProfileType == OtherScreen)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}
@end
