//
//  MIProfileViewController.m
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIProfileViewController.h"
#import "MIAddNewPostViewController.h"
#import "MIMessageViewController.h"
#import "MIEditProfileViewController.h"
#import "MIFriendsViewController.h"
#import "MICommentsViewController.h"
#import "MIHomeDetailViewController.h"

#import "MIReportPopUpView.h"
#import "MIProfileHeaderView.h"

#import "MIHomeTableViewCell.h"
#import "MIHomeAudioTableViewCell.h"
#import "MIHomeImageTableViewCell.h"
#import "MIAllFriendsTableViewCell.h"
#import "MIAlbumTableViewCell.h"




@interface MIProfileViewController ()
{
    MIReportPopUpView *reportPopUp;
    MIProfileHeaderView *profileHeader;
    
    AVPlayer *audioPlayer;
    AVPlayerItem *currentItem;
    MPMoviePlayerController *videoPlayer;
    
    NSTimeInterval duration, currentTime;
    NSArray *arrList;
    
    BOOL isReload;
}
@end

@implementation MIProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}


# pragma mark
# pragma mark - General Method

- (void)initialize
{
    arrList = @[@{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"image",@"post_image":@[@"temp_post1",@"temp_post2",@"temp_post3",@"temp_post4",@"temp_post5"],@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"audio",@"audio_url":@"http://mobicreation.in/engage/eng-uploads/track/201703071152282885.mp3",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"video",@"video_url":@"shutterstock_v6228647",@"video_type":@"mov",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1",@"video_thumb":@"temp_banner"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"audio",@"audio_url":@"http://mobicreation.in/engage/eng-uploads/track/201703091232561071.mp3",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"image",@"post_image":@[@"temp_post1",@"temp_post2",@"temp_post3",@"temp_post4",@"temp_post5"],@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"video",@"video_url":@"Sample Videos",@"video_type":@"mp4",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1",@"video_thumb":@"temp_banner"}];
    
    
    //.....UITableView Header......
    
    CGFloat height;
    
    if(Is_iPhone_5)
        height = 495;
    else
        height = ((485 * CScreenWidth) / 375) + 10;
    
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, height)];
    profileHeader = [MIProfileHeaderView customProfileHeader];
    profileHeader.btnPost.selected = YES;
    [viewHeader addSubview:profileHeader];
    
    tblVProfile.tableHeaderView = viewHeader;
    tblVProfile.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [tblVProfile setContentInset:UIEdgeInsetsMake(0, 0, 75, 0)];
    tblVProfile.rowHeight = height; //(CScreenWidth * 485) / 375;
    
    
    [tblVProfile registerNib:[UINib nibWithNibName:@"MIHomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeTableViewCell"];
    [tblVProfile registerNib:[UINib nibWithNibName:@"MIHomeImageTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeImageTableViewCell"];
    [tblVProfile registerNib:[UINib nibWithNibName:@"MIHomeAudioTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeAudioTableViewCell"];
    [tblVProfile registerNib:[UINib nibWithNibName:@"MIAllFriendsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAllFriendsTableViewCell"];
    [tblVProfile registerNib:[UINib nibWithNibName:@"MIAlbumTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAlbumTableViewCell"];
    [tblVProfile setEstimatedRowHeight:UITableViewAutomaticDimension];
    
    
    [self setProfileHeaderData];
}

- (void)setProfileHeaderData
{
    profileHeader.btnFollow.selected = YES;
    
    if (_profileType == MyProfile)
    {
        [profileHeader.btnMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
        [profileHeader.btnChat setImage:[UIImage imageNamed:@"add_post_white"] forState:UIControlStateNormal];
        [profileHeader.btnCamera hideByWidth:NO];
        [profileHeader.btnEdit hideByHeight:NO];
        [profileHeader.btnFollow hideByWidth:YES];
        profileHeader.cnBtnAddFriendCenter.constant = 0;
        [profileHeader.btnAddFriend setTitle:CLocalize(@"Edit Profile").uppercaseString forState:UIControlStateNormal];
    }
    else
    {
        [profileHeader.btnMenu setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [profileHeader.btnChat setImage:[UIImage imageNamed:@"chat"] forState:UIControlStateNormal];
        // profileHeader.cnBtnAddFriendCenter.constant = (CScreenWidth/2)/4;
        [profileHeader.btnCamera hideByHeight:YES];
        [profileHeader.btnEdit hideByHeight:YES];
    }
    
    
    [profileHeader.btnMenu touchUpInsideClicked:^{
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    
    [profileHeader.btnChat touchUpInsideClicked:^{
        
        if (_profileType == MyProfile)
        {
            MIAddNewPostViewController *addPostVC = [[MIAddNewPostViewController alloc] initWithNibName:@"MIAddNewPostViewController" bundle:nil];
            [self.navigationController pushViewController:addPostVC animated:YES];
        }
        else
        {
            /*  if() //User not in Friend List
             {
             UIAlertView *alt = [[UIAlertView alloc] initWithTitle:@"" message:CAddFriendList delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
             
             [alt showAlerViewWithHandlerBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
             {
             if(buttonIndex == 0)
             {}
             }];
             }
             else
             { */
            MIMessageViewController *messageVC = [[MIMessageViewController alloc] initWithNibName:@"MIMessageViewController" bundle:nil];
            messageVC.messageType = Other;
            [self.navigationController pushViewController:messageVC animated:YES];
            //    }
            
        }
        
    }];
    
    
    [profileHeader.btnAddFriend touchUpInsideClicked:^{
        
        if (_profileType == MyProfile)
        {
            MIEditProfileViewController *editProfile = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
            editProfile.editProfileType = OtherScreen;
            [self.navigationController pushViewController:editProfile animated:YES];
        }
        else
        {
            MIFriendsViewController *frdVC = [[MIFriendsViewController alloc] initWithNibName:@"MIFriendsViewController" bundle:nil];
            frdVC.friendType = FromProfile;
            [self.navigationController pushViewController:frdVC animated:YES];
        }
        
    }];
    
    
    [profileHeader.btnFollow touchUpInsideClicked:^{
        
        if(profileHeader.btnFollow.selected)
        {
            [profileHeader.btnFollow setTitle:CLocalize(@"Follow").uppercaseString forState:UIControlStateSelected];
            profileHeader.btnFollow.selected = NO;
        }
        
        else
        {
             [profileHeader.btnFollow setTitle:CLocalize(@"Unfollow").uppercaseString forState:UIControlStateNormal];
             profileHeader.btnFollow.selected = YES;
        }
        
    }];
    
    
    [profileHeader.btnEdit touchUpInsideClicked:^{
        
        [self selectImageWithEditing:YES animation:YES completion:^(UIImage *image)
         {
             if (image)
                 profileHeader.imgVUser.image = image;
             
         }];
    }];
    
    
    int position = (CScreenWidth * 2) / 375;
    [profileHeader.btnPost touchUpInsideClicked:^{
        
        btnInterestBox.hidden = NO;
        profileHeader.btnPost.selected = YES;
        profileHeader.btnAlbums.selected = profileHeader.btnFriends.selected = profileHeader.btnFFPost.selected = NO;
        
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [profileHeader.viewSelectedTab setConstraintConstant:CViewX(profileHeader.btnPost) + position toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
            [profileHeader.viewSelectedTab.superview layoutIfNeeded];
            
        }];
        
        [tblVProfile reloadData];
        
    }];
    
    [profileHeader.btnAlbums touchUpInsideClicked:^{
        
        btnInterestBox.hidden = YES;
        profileHeader.btnAlbums.selected = YES;
        profileHeader.btnPost.selected = profileHeader.btnFriends.selected = profileHeader.btnFFPost.selected = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [profileHeader.viewSelectedTab setConstraintConstant:CViewX(profileHeader.btnAlbums) + position toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
            [profileHeader.viewSelectedTab.superview layoutIfNeeded];
            
        }];
        
        [tblVProfile reloadData];
        
    }];
    
    [profileHeader.btnFriends touchUpInsideClicked:^{
        
        btnInterestBox.hidden = YES;
        profileHeader.btnFriends.selected = YES;
        profileHeader.btnPost.selected = profileHeader.btnAlbums.selected = profileHeader.btnFFPost.selected = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [profileHeader.viewSelectedTab setConstraintConstant:CViewX(profileHeader.btnFriends) + position toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
            [profileHeader.viewSelectedTab.superview layoutIfNeeded];
            
        }];
        
        [tblVProfile reloadData];
        
    }];
    
    [profileHeader.btnFFPost touchUpInsideClicked:^{
        
        btnInterestBox.hidden = YES;
        profileHeader.btnFFPost.selected = YES;
        profileHeader.btnPost.selected = profileHeader.btnAlbums.selected = profileHeader.btnFriends.selected = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [profileHeader.viewSelectedTab setConstraintConstant:CViewX(profileHeader.btnFFPost) + position toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
            [profileHeader.viewSelectedTab.superview layoutIfNeeded];
            
        }];
        
        [tblVProfile reloadData];
        
    }];
}




# pragma mark
# pragma mark - Action Events

- (void)showReportPopup
{
    NSMutableArray *arrSelected = [[NSMutableArray alloc] init];
    reportPopUp = [MIReportPopUpView customReportView:PublicBox interest:arrSelected];
    
    //.....Clicked events
    
    [reportPopUp.btnCancel touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [reportPopUp.btnSubmit touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [appDelegate.window addSubview:reportPopUp];
}

- (void)btnAddPostClicked
{
    if (_profileType == MyProfile)
    {
        MIAddNewPostViewController *addPostVC = [[MIAddNewPostViewController alloc] initWithNibName:@"MIAddNewPostViewController" bundle:nil];
        [self.navigationController pushViewController:addPostVC animated:YES];
    }
    else
    {
        //        MIAddNewPostViewController *addPostVC = [[MIAddNewPostViewController alloc] initWithNibName:@"MIAddNewPostViewController" bundle:nil];
        //        [self.navigationController pushViewController:addPostVC animated:YES];
    }
}

- (IBAction)btnChangePostClicked:(id)sender
{
    NSMutableArray *arrSelected = [[NSMutableArray alloc] init];
    reportPopUp = [MIReportPopUpView customReportView:InterestBox interest:arrSelected];
    
    //.....Clicked events
    
    [reportPopUp.btnCancel touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [reportPopUp.btnSubmit touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [appDelegate.window addSubview:reportPopUp];
}


- (void)updateAudioProgress
{
}


# pragma mark
# pragma mark - UITableview Datasource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(profileHeader.btnAlbums.selected)
        return 1;
    
    return arrList.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (profileHeader.btnPost.selected || profileHeader.btnFFPost.selected)
    {
        if([[[arrList objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"audio"])
            return (CScreenWidth * 275) / 375;
        
        return (CScreenWidth * 445) / 375;
    }
    else if(profileHeader.btnAlbums.selected)
        return UITableViewAutomaticDimension;
    else
        return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (profileHeader.btnPost.selected || profileHeader.btnFFPost.selected)
    {
        
        //TODO: Image cell
        
        NSDictionary *dict = [arrList objectAtIndex:indexPath.row];
        
        if([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            static NSString *identifier = @"MIHomeImageTableViewCell";
            MIHomeImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
            cell.lblUserName.text = [dict valueForKey:@"user_name"];
            cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
            cell.lblPostDetail.text = [dict valueForKey:@"desc"];
            cell.lblPostTime.text = [dict valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
            
            cell.arrImg = [dict valueForKey:@"post_image"]; // Pass Here array of Detail
            [cell scrollViewDidScroll:cell.collImg];
            cell.pageV.currentPage = 0;
            [cell.pageV setNumberOfPages:cell.arrImg.count];
            
            
            
            //.....Cell button clicked Event
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
                
            }];
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
            }];
            
            
            return cell;
        }
        
        //TODO: Audio cell
        else if([[dict valueForKey:@"type"] isEqualToString:@"audio"])
        {
            static NSString *identifier = @"MIHomeAudioTableViewCell";
            MIHomeAudioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
            cell.lblUserName.text = [dict valueForKey:@"user_name"];
            cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
            cell.lblPostDetail.text = [dict valueForKey:@"desc"];
            cell.lblPostTime.text = [dict valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
          /*
            currentItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:[dict valueForKey:@"audio_url"]]];
            currentTime = CMTimeGetSeconds(currentItem.currentTime);
            duration = CMTimeGetSeconds(currentItem.duration);
           
            cell.lblStartTime.text = [NSString stringWithFormat:@"%.2f",currentTime];
            cell.lblEndTime.text = [NSString stringWithFormat:@"%f", CMTimeGetSeconds(range.start) + CMTimeGetSeconds(range.duration)]; */
            
            
            //.....Cell button clicked Event
            
            [cell.btnPlayPause touchUpInsideClicked:^{
                
                if(cell.btnPlayPause.selected)
                {
                    //Pause
                    cell.btnPlayPause.selected = NO;
                    [audioPlayer pause];
                }
                else
                {
                    //Play
                    cell.btnPlayPause.selected = YES;
                    audioPlayer = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithURL:[NSURL URLWithString:[dict valueForKey:@"audio_url"]]]];
                    [audioPlayer play];
                    
                    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateAudioProgress) userInfo:nil repeats:YES];
                }
            }];
            
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
            }];
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
            }];
            
            
            return cell;
            
        }
        //TODO: Video cell
        else
        {
            static NSString *identifier = @"MIHomeTableViewCell";
            MIHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.imgVPost.image =[UIImage imageNamed:[dict valueForKey:@"video_thumb"]];
            cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
            cell.lblUserName.text = [dict valueForKey:@"user_name"];
            cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
            cell.lblPostDetail.text = [dict valueForKey:@"desc"];
            cell.lblPostTime.text = [dict valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
            
            [cell.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
            [videoPlayer.view removeFromSuperview];
            cell.btnPlayPause.selected = NO;
            
            
            
            //.....Cell button clicked Event
            
            [cell.btnPlayPause touchUpInsideClicked:^{
                
                if(cell.btnPlayPause.selected)
                {
                    //Pause
                    cell.btnPlayPause.selected = NO;
                    [cell.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
                    [videoPlayer stop];
                }
                else
                {
                    //Play
                    cell.btnPlayPause.selected = YES;
                    [cell.btnPlayPause setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    videoPlayer = [appDelegate playVideoWithResource:[dict valueForKey:@"video_url"] andType:[dict valueForKey:@"video_type"]];
                    [videoPlayer.view setFrame:cell.imgVPost.bounds];
                    [cell.imgVPost addSubview:videoPlayer.view];
                    [videoPlayer play];
                }
            }];
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
                
            }];
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
            }];
            
            
            return cell;
        }
    }
    
    else if (profileHeader.btnAlbums.selected)
    {
        static NSString *cellIdetifier = @"MIAlbumTableViewCell";
        MIAlbumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (_profileType == MyProfile)
            cell.strType = @"MyProfile";
        
        return cell;
    }
    
    else
    {
        static NSString *cellIdetifier = @"MIAllFriendsTableViewCell";
        MIAllFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (_profileType == MyProfile)
            [cell.btnUnFriend hideByWidth:NO];
        else
            [cell.btnUnFriend hideByWidth:YES];
        
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (profileHeader.btnPost.selected || profileHeader.btnFFPost.selected)
    {
        MIHomeDetailViewController *detailVC = [[MIHomeDetailViewController alloc] initWithNibName:@"MIHomeDetailViewController" bundle:nil];
        detailVC.dictDetail = [arrList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:detailVC animated:YES];
    }
    else if (profileHeader.btnFriends.selected)
    {
        MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
        profileVC.profileType = OtherUserProfile;
        [self.navigationController pushViewController:profileVC animated:YES];
    }
    
}

@end
