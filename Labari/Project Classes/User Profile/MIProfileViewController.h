//
//  MIProfileViewController.h
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    MyProfile,
    OtherUserProfile
    
}Profile;


@interface MIProfileViewController : ParentViewController
{
    IBOutlet UITableView *tblVProfile;
    IBOutlet UIButton *btnInterestBox;
}



@property (nonatomic , assign) Profile profileType;


- (IBAction)btnChangePostClicked:(id)sender;

@end
