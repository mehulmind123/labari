//
//  MISelectLanguageViewController.m
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MISelectLanguageViewController.h"
#import "MILoginViewController.h"

@interface MISelectLanguageViewController ()

@end

@implementation MISelectLanguageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnEnglish.selected = YES;
    CSetUserLanguage(CLanguageEnglish);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}



# pragma mark
# pragma mark - Button Action

- (IBAction)selectLanguage:(UIButton *)sender
{
    btnEnglish.selected = btnFrancais.selected = NO;
    
    if(sender.tag == 0)
    {
        btnEnglish.selected = YES;
        CSetUserLanguage(CLanguageEnglish);
    }
    else
    {
         btnFrancais.selected = YES;
         CSetUserLanguage(CLanguageFrench);
    }
    
}

- (IBAction)btnConfrimClicked:(id)sender
{
    MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
    [self.navigationController pushViewController:loginVC animated:YES];
}

@end
