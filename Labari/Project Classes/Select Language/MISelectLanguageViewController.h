//
//  MISelectLanguageViewController.h
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISelectLanguageViewController : ParentViewController
{
    IBOutlet UIButton *btnEnglish;
    IBOutlet UIButton *btnFrancais;
}
@end
