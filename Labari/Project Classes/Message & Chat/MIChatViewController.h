//
//  MIChatViewController.h
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTSImageViewController.h"

@interface MIChatViewController : ParentViewController <UITableViewDelegate, UITableViewDataSource, HPGrowingTextViewDelegate>
{
    
    IBOutlet UIView *viewHeader;
    IBOutlet UIView *viewComment;
    IBOutlet UITableView *tblVChat;
    IBOutlet HPGrowingTextView *txtVMessage;
    IBOutlet UIButton *btnSend;
    IBOutlet UIButton *btnAddToFrdList;
    IBOutlet UILabel *lblText;
    IBOutlet NSLayoutConstraint *cnTextVHeight;
    
}

@property (nonatomic, strong)NSString *strUsername;

- (IBAction)btnAddToFriendListClicked:(id)sender;
- (IBAction)btnSendClicked:(id)sender;

@end
