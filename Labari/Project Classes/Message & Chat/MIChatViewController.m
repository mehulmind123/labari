//
//  MIChatViewController.m
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIChatViewController.h"
#import "MITextMsgTableViewCell.h"
#import "MITextMsgOtherUserTableViewCell.h"
#import "MIImgMsgTableViewCell.h"
#import "MIImgMsgOtherUserTableViewCell.h"
#import "JTSImageInfo.h"

@interface MIChatViewController ()
{
    NSArray *arrChatList;
    NSIndexPath *imgIndexpath;
}
@end

@implementation MIChatViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



# pragma mark
# pragma mark - General Method

-(void)initialize
{
    self.title = self.strUsername;
    arrChatList = @[@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",@"Lorem Ipsum is simply dummy text of the printing and typesetting industry.",@"1.jpeg",@"2.jpeg"];
    
    [lblText setText:CLocalize(@"This sender is not in your friend list")];
    [btnAddToFrdList setTitle:CLocalize(@"Add to Friend List").uppercaseString forState:UIControlStateNormal];
    [btnSend setTitle:CLocalize(@"Send") forState:UIControlStateNormal];
    
    [viewHeader hideByHeight:YES];
   
    txtVMessage.placeholder = CLocalize(@"Type Message");
    [txtVMessage setPlaceholderColor:CRGB(213, 213, 213)];
    txtVMessage.font = CFontAvenirLTStd55Roman(13);
    txtVMessage.textColor =  ColorDarkBule_0e293c;
    txtVMessage.animateHeightChange = YES;
    txtVMessage.delegate = self;
    [txtVMessage setContentInset:UIEdgeInsetsMake(0, 5, 0, 35)];
    txtVMessage.tintColor = ColorDarkBule_0e293c;
    txtVMessage.backgroundColor = CRGB(242, 242, 242);
    txtVMessage.layer.cornerRadius  = cnTextVHeight.constant/2;
    txtVMessage.layer.masksToBounds = YES;
    
    
//    txtVMessage.
//    
//    UIButton *btnAttachment = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnAttachment setFrame:CGRectMake(0, 0, 40, 30)];
//    [btnAttachment setImage:[UIImage imageNamed:@"chat_attachment"] forState:UIControlStateNormal];
//    [btnAttachment addTarget:self action:@selector(btnAttachmentClicked) forControlEvents:UIControlEventTouchUpInside];
//    txtMessage.rightViewMode = UITextFieldViewModeAlways;
//    txtMessage.rightView = btnAttachment;
    
    
    
    viewComment.layer.shadowColor = [UIColor blackColor].CGColor;
    viewComment.layer.shadowOffset = CGSizeMake(3, 1);
    viewComment.layer.shadowOpacity = 0.3f;
    viewComment.layer.shadowRadius = 3.0f;
    
    
    [tblVChat registerNib:[UINib nibWithNibName:@"MITextMsgOtherUserTableViewCell" bundle:nil] forCellReuseIdentifier:@"MITextMsgOtherUserTableViewCell"];
    [tblVChat registerNib:[UINib nibWithNibName:@"MITextMsgTableViewCell" bundle:nil] forCellReuseIdentifier:@"MITextMsgTableViewCell"];
    [tblVChat registerNib:[UINib nibWithNibName:@"MIImgMsgTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIImgMsgTableViewCell"];
    [tblVChat registerNib:[UINib nibWithNibName:@"MIImgMsgOtherUserTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIImgMsgOtherUserTableViewCell"];
    
    [tblVChat setRowHeight:58];
    [tblVChat setEstimatedRowHeight:UITableViewAutomaticDimension];
}



#pragma mark - Growing textview delegate methods
#pragma mark -

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    cnTextVHeight.constant = height > 30 ? height : 30;
}

/*
- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText = [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(finalText.length >= CCommentLength)
        return NO;
    
    return YES;
}
*/

#pragma mark - ScrollView Method
#pragma mark -

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [txtVMessage resignFirstResponder];
}




# pragma mark
# pragma mark - Action Events

- (IBAction)btnAddToFriendListClicked:(id)sender
{
}

- (IBAction)btnSendClicked:(id)sender
{
    [appDelegate resignKeyboard];
    
    if([txtVMessage.text length] == 0)
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CAddMessage withTitle:@""];
}

- (IBAction)btnAttachmentClicked:(id)sender
{
    [self selectImageWithEditing:YES animation:YES completion:^(UIImage *image)
     {
         if (image)
         {
         }
     }];
}



#pragma mark -
#pragma mark - UITableView Delegate And Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrChatList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            NSString *identifier = @"MITextMsgTableViewCell";
            MITextMsgTableViewCell *cell = [tblVChat dequeueReusableCellWithIdentifier:identifier];
            
            cell.lblMsg.attributedText = [appDelegate setAttributedString:[arrChatList objectAtIndex:indexPath.row] andAlign:NSTextAlignmentLeft];
            
            return cell;
            break;
        }
        case 1:
        {
            NSString *identifier = @"MITextMsgOtherUserTableViewCell";
            MITextMsgOtherUserTableViewCell *cell = [tblVChat dequeueReusableCellWithIdentifier:identifier];
            
            cell.lblMsg.attributedText = [appDelegate setAttributedString:[arrChatList objectAtIndex:indexPath.row] andAlign:NSTextAlignmentLeft];
            
            return cell;
            break;
        }
        case 2:
        {
            NSString *identifier = @"MIImgMsgTableViewCell";
            MIImgMsgTableViewCell *cell = [tblVChat dequeueReusableCellWithIdentifier:identifier];
            
            cell.imgVMsg.image = [UIImage imageNamed:[arrChatList objectAtIndex:indexPath.row]];
            imgIndexpath = indexPath;
            
            [cell.imgVMsg enableFullScreenImage];
            
            return cell;
            break;
        }
        case 3:
        {
            NSString *identifier = @"MIImgMsgOtherUserTableViewCell";
            MIImgMsgOtherUserTableViewCell *cell = [tblVChat dequeueReusableCellWithIdentifier:identifier];
            
            cell.imgVMsg.image = [UIImage imageNamed:[arrChatList objectAtIndex:indexPath.row]];
            
            imgIndexpath = indexPath;
            
            [cell.imgVMsg enableFullScreenImage];
            
            
            return cell;
            break;
        }
    }
    return nil;
}


@end
