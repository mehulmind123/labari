//
//  RecordAudioViewController.h
//  Labari
//
//  Created by mac-0005 on 3/22/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordAudioViewController : UIViewController<AVAudioPlayerDelegate, AVAudioRecorderDelegate, MPMediaPickerControllerDelegate>
{
    IBOutlet UILabel *lblAudioRecording;
    IBOutlet UILabel *lblAudioTime;
    IBOutlet UIButton *btnAudioStop;
    IBOutlet UIButton *btnAudioPause;
    IBOutlet UIButton *btnAudioGallery;
    IBOutlet UIButton *btnAudioDelete;
    
}

- (IBAction)btnAudioStopClicked:(id)sender;
- (IBAction)btnAudioGalleryClicked:(id)sender;
- (IBAction)btnAudioPauseClicked:(id)sender;
- (IBAction)btnAudioDeleteClicked:(id)sender;

@end
