//
//  RecordAudioViewController.m
//  Labari
//
//  Created by mac-0005 on 3/22/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "RecordAudioViewController.h"

@interface RecordAudioViewController ()
{
    AVAudioRecorder *recorder;
    CADisplayLink *displaylink;
    
    NSTimer *timer;
    int recordingTime;
    double timerElapsed;
    NSDate *timerStarted;
    double timerInterval;
}
@end

@implementation RecordAudioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Record Audio");
    
    lblAudioRecording.text = CLocalize(@"Audio Recording");
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"audio_save"] style:UIBarButtonItemStylePlain target:self action:@selector(btnSaveClick)];
    
    [btnAudioPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
}

- (void)audioSessionStart
{
    int currentTimestamp = [[NSDate date] timeIntervalSince1970];
    
    NSArray *pathComponents = [NSArray arrayWithObjects: [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject], [NSString stringWithFormat:@"Labari%d.m4a", currentTimestamp], nil];
    
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    
    // SETUP AUDIO SESSION
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];
    
    
    // DEFINE THE RECORDER SETTING
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    
    // INITIATE AND PREPARE THE RECORDER
    
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    
    [recorder prepareToRecord];
    [recorder setMeteringEnabled:YES];
    [recorder record];
    
    NSError *error;
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    
    
}


# pragma mark
# pragma mark - Action Events

- (void)btnSaveClick
{
    [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:@"Audio saved successfully." withTitle:@""];
    [btnAudioPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [self invalidTimer];
}

- (IBAction)btnAudioStopClicked:(id)sender
{
    [self invalidTimer];
}

- (IBAction)btnAudioPauseClicked:(UIButton *)sender
{
    //.....STOP THE AUDIO PLAYER BEFORE RECORDING
    
    if (!recorder.recording)
    {
        [btnAudioPause setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
        [self audioSessionStart];
        [self setTimer];
    }
    else
    {
        // Pause Recording
        [recorder pause];
        [btnAudioPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        [timer invalidate];
        timer = nil;
        [recorder stop];
    }
}

- (IBAction)btnAudioGalleryClicked:(id)sender
{
    [self pickAudioFiles];
}

-(void)pickAudioFiles
{
    MPMediaPickerController *soundPicker = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeAnyAudio];
    soundPicker.delegate = self;
    soundPicker.allowsPickingMultipleItems = NO;
    [self systemMusicWithHandler:^(BOOL permissionGranted, NSArray *genres)
    {
        
    }];
    [self.navigationController presentViewController:soundPicker animated:YES completion:nil];
}

-(void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
    MPMediaItem *item = [[mediaItemCollection items] firstObject];
    NSURL *url = [item valueForProperty:MPMediaItemPropertyAssetURL];
    [mediaPicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)systemMusicWithHandler:(void(^) (BOOL permissionGranted, NSArray *genres))handler
{
    if (IS_Ios9)
    {
        [MPMediaLibrary requestAuthorization:^(MPMediaLibraryAuthorizationStatus status)
         {
             switch (status)
             {
                 case MPMediaLibraryAuthorizationStatusDenied:
                 {
                     dispatch_async(GCDMainThread, ^{
                         
                         UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                         UIAlertAction *goToSetting = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction  * _Nonnull action)
                            {
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                            }];
                         UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
                         [alert addAction:goToSetting];
                         [alert addAction:cancelAction];
                         [appDelegate.window.rootViewController presentViewController:alert animated:true completion:nil];
                         
                     });
                     
                     break;
                 }
                 case MPMediaLibraryAuthorizationStatusAuthorized:
                 {
                     dispatch_async(GCDMainThread, ^{
                         if (handler) handler(YES, [self getSystemMusic]);
                     });
                     break;
                 }
                 default:
                     break;
             }
         }];
    }
    else
    {
        if (handler) handler(YES, [self getSystemMusic]);
    }
}

- (NSArray *)getSystemMusic
{
    NSMutableArray *arrSongs = [[NSMutableArray alloc] init];
    MPMediaQuery *albums = [MPMediaQuery playlistsQuery];
    NSArray *playlists = [albums collections];
    
    for (MPMediaPlaylist *playlist in playlists)
    {
        if ([[playlist valueForProperty:MPMediaPlaylistPropertyPlaylistAttributes] isEqual:@2])
            continue;
        
        [arrSongs addObjectsFromArray:[playlist items]];
    }
    
    return arrSongs;
}

- (IBAction)btnAudioDeleteClicked:(id)sender
{
    [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:@"Recorded audio deleted successfully." withTitle:@""];
    [btnAudioPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [self invalidTimer];
    [recorder stop];
    [recorder deleteRecording]; //.....AUDIO MUST BE STOP BEFORE DELETING
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (void)SaveAudioFileToLibrary
{
    NSLog(@"Recorder URL == %@", recorder.url);
    NSData *urlData = [NSData dataWithContentsOfURL:recorder.url];
    NSString *strPath = [NSString stringWithFormat:@"%@/%@", [self getSoundPath], @"labari.m4a"];
    
    NSError *error = nil;
    
    if([[NSFileManager defaultManager] fileExistsAtPath:strPath])
        [[NSFileManager defaultManager] removeItemAtPath:strPath error:&error]; //Delete old file
    
    if (urlData)
    {
        if ([urlData writeToFile:strPath atomically:YES])
        {
            // yeah - file written
            NSLog(@"YES");
        }
        else
        {
            // oops - file not written
            NSLog(@"NO");
        }
    }
    else
    {
        // oops - couldn't get data
        NSLog(@"NOT");
    }
}

- (NSString *)getSoundPath
{
    NSError *error;
    
    NSString *strSoundPath = [NSString stringWithFormat:@"%@", CDocumentsDirectory];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:strSoundPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:strSoundPath withIntermediateDirectories:NO attributes:nil error:&error]; //
    
    return strSoundPath;
}




# pragma mark
# pragma mark - Timer Fuctions

- (void)setTimer
{
    if (!timer)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(timeRecording:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
}

-(void)timeRecording:(NSTimer *)timer1
{
    recordingTime++;
    
    int hours = recordingTime / 3600;
    int minutes = recordingTime / 60 % 60;
    int seconds = recordingTime % 60;
    
    
    lblAudioTime.hidden = NO;
    if (recordingTime <= 40)
    {
//        NSLog(@"00:%02d",recordingTime);
//        
//        if (seconds >= 0 && minutes > 0 && hours > 0)
//        {
//            lblAudioTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
//        }
//        else if (seconds >= 0 && minutes > 0)
//        {
//            lblAudioTime.text = [NSString stringWithFormat:@"00:%02d:%02d", minutes, seconds];
//        }
//        else if (minutes >= 0 && hours > 0)
//        {
//            lblAudioTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
//        }
//        else
//        {
//            lblAudioTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",seconds, minutes, seconds];
//        }
        
        
        lblAudioTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
//        lblAudioTime.text = [NSString stringWithFormat:@"00:00:%02d",recordingTime];
    }
    else
    {
//        [self showSaveRecordingAlertView];
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:@"Audio saved successfully." withTitle:@""];
        [btnAudioPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        [self invalidTimer];
    }
}

- (void)invalidTimer
{
    [timer invalidate];
    timer = nil;
    recordingTime = 0;
    [lblAudioTime setText:@"00:00:00"];
    [recorder stop];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

@end
