//
//  TakeVideoViewController.m
//  Mospur
//
//  Created by mac-0009 on 16/03/17.
//  Copyright © 2017 mac-0009. All rights reserved.
//

#import "TakeVideoViewController.h"
#import "LLSimpleCamera.h"
//#import "VideoViewController.h"

@interface TakeVideoViewController ()
{
    NSTimer *timerVideo;
    LLSimpleCamera *camera;
    BOOL isAvailable;
}

@end

@implementation TakeVideoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [camera start];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = FALSE;
    [timerVideo invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Initialize

- (void)initialize
{
    if (_uploadType == imageUpload)
        btnCameraVideo.selected = YES;
    else
        btnCameraVideo.selected = NO;
    
    [self btnCameraVideoClicked:btnCameraVideo];
    
    
    
    
    btnStart.progress = 0;
//    isAvailable = btnCameraVideo.selected;
    
    
    // Camera with precise quality, position and video parameters.
    camera = [[LLSimpleCamera alloc] initWithQuality:AVCaptureSessionPresetHigh
                                                 position:LLCameraPositionRear
                                             videoEnabled:YES];
    
    // Attach to a view controller
    [camera attachToViewController:self withFrame:CGRectMake(0, 64, CScreenWidth, CScreenHeight - viewNavigation.bounds.size.height - viewControls.bounds.size.height)];
    camera.fixOrientationAfterCapture = NO;
    
    
    
    // Take the required actions on a device change
    [camera setOnDeviceChange:^(LLSimpleCamera *cameraLocal, AVCaptureDevice * device)
    {
        
        NSLog(@"Device changed.");
        
        // device changed, check if flash is available
        if([cameraLocal isFlashAvailable])
        {
            btnFlash.hidden = NO;
            
            if(camera.flash == LLCameraFlashOff)
            {
                btnFlash.selected = NO;
            }
            else
            {
                btnFlash.selected = YES;
            }
        }
        else
        {
            btnFlash.hidden = YES;
        }
    }];
    
    
    [camera setOnError:^(LLSimpleCamera *camera, NSError *error)
    {
        NSLog(@"Camera error: %@", error);
        
        isAvailable = FALSE;
        if([error.domain isEqualToString:LLSimpleCameraErrorDomain])
        {
            if(error.code == LLSimpleCameraErrorCodeCameraPermission ||
               error.code == LLSimpleCameraErrorCodeMicrophonePermission)
            {
                
//                @"We need permission for the camera.\nPlease go to your settings.";
            }
        }
    }];
    
    if(![LLSimpleCamera isFrontCameraAvailable]/* && [LLSimpleCamera isRearCameraAvailable]*/)
    {
//        btnCameraFrontRear.hidden = TRUE;
    }
    
//    [self.view addSubview:viewControls];
//    [self.view addSubview:viewNavigation];
}


# pragma mark
# pragma mark - Action Events

- (IBAction)btnCloseClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnForwardClicked:(id)sender
{
    if (isAvailable)
    {
        if(camera.didRecordCompletionBlock)
        {
            camera.didRecordCompletionBlock(camera, camera.outputURL, nil);
        }
    }
    else
    {
//        VideoViewController *videoVC = [VideoViewController initWithXib];
//        videoVC.dictVideo = nil;
//        [self.navigationController pushViewController:videoVC animated:YES];
    }
}

- (IBAction)btnStartClicked:(UIButton *)sender
{
    if (_uploadType == videoUpload)
    {
        btnPlayImage.selected = TRUE;
        if (!camera.isRecording)
        {
            timerVideo = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
            
            btnFlash.hidden = YES;
//            btnCameraFrontRear.hidden = YES;
            [camera start];
            
            // start recording
            NSURL *outputURL = [[[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"mospur1"] URLByAppendingPathExtension:@"mov"];
            
            [camera startRecordingWithOutputUrl:outputURL didRecord:^(LLSimpleCamera *camera, NSURL *outputFileUrl, NSError *error)
             {
//                 VideoViewController *videoVC = [VideoViewController initWithXib];
//                 UIImage *img = [self generateThumbImage:outputFileUrl];
//                 videoVC.dictVideo = @{@"image" : img, @"url" : outputFileUrl};
//                 [self.navigationController pushViewController:videoVC animated:YES];
             }];
        }
        else
        {
            
        }
    }
    else
    {
        btnPlayImage.selected = NO;
        btnStart.progress = 0;
        [timerVideo invalidate];
        [camera stopRecording];
        
        [camera capture:^(LLSimpleCamera *camera, UIImage *image, NSDictionary *metadata, NSError *error)
        {
            UIImage *img = image;
            NSLog(@"Captured");
            NSLog(@"image == %@", img);
        }];
    }
}

- (IBAction)btnCameraVideoClicked:(UIButton *)sender
{
    //.....
    if (sender.selected)
    {
        btnPlayImage.selected = NO;
        btnStart.progress = 0;
        [timerVideo invalidate];
        [btnCameraVideo setImage:[UIImage imageNamed:@"go_to_video"] forState:UIControlStateNormal];
        sender.selected = NO;
        _uploadType = imageUpload;
    }
    else
    {
        [btnCameraVideo setImage:[UIImage imageNamed:@"go_to_camera"] forState:UIControlStateNormal];
        sender.selected = YES;
        _uploadType = videoUpload;
    }
}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:filepath options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 0.0);
    NSError *error = nil;
    CMTime actualTime;
    
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    
    return thumb ? thumb : [UIImage imageNamed:@"Home_Temp1"];
}

- (IBAction)btnGalleryClicked:(id)sender
{
    [self presentPhotoLibrary:^(UIImage *image)
    {
        if (image)
        {
            
        }
    }];
}

//- (IBAction)btnStopClicked:(id)sender
//{
//    if (isAvailable)
//    {
//        btnPlayImage.selected = FALSE;
//        btnFlash.hidden = NO;
////        btnCameraFrontRear.hidden = NO;
//        
//        [camera stopRecording];
//    }
//}

- (IBAction)btnFlashClicked:(id)sender
{
    //....Flash OFF
    if(camera.flash == LLCameraFlashOff)
    {
        BOOL done = [camera updateFlashMode:LLCameraFlashOn];
        if(done)
        {
            btnFlash.selected = YES;
            btnFlash.tintColor = [UIColor yellowColor];
        }
    }
    //....Flash ON
    else
    {
        BOOL done = [camera updateFlashMode:LLCameraFlashOff];
        if(done)
        {
            btnFlash.selected = NO;
            btnFlash.tintColor = [UIColor whiteColor];
        }
    }
}

- (IBAction)btnFrontClicked:(id)sender
{
    [camera togglePosition];
}

#pragma mark -
#pragma mark - Helper Method

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)updateProgress
{
    btnStart.progress = btnStart.progress + 1;
}

@end
