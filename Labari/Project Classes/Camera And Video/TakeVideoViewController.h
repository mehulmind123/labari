//
//  TakeVideoViewController.h
//  Mospur
//
//  Created by mac-0009 on 16/03/17.
//  Copyright © 2017 mac-0009. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MBCircularProgressBarView.h"
#import "PlayerButton.h"

typedef enum : NSUInteger
{
    imageUpload,
    videoUpload
    
}UploadType;

@interface TakeVideoViewController : UIViewController
{
    IBOutlet UIView *viewControls;
    IBOutlet UIView *viewNavigation;
    
    IBOutlet UIButton *btnPlayImage;
    IBOutlet UIButton *btnClose;
    IBOutlet PlayerButton *btnStart;
//    IBOutlet UIButton *btnStop;
    IBOutlet UIButton *btnFlash;
    IBOutlet UIButton *btnGallery;
    IBOutlet UIButton *btnCameraFrontRear;
    IBOutlet UIButton *btnCameraVideo;
}

@property (nonatomic , assign) UploadType uploadType;

@end
