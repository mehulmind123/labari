//
//  MICommentsViewController.m
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MICommentsViewController.h"

#import "MICommentsTableViewCell.h"

@interface MICommentsViewController ()
{
    UILabel *lblPlaceHolder;
    NSArray *arrCommentList;
}
@end

@implementation MICommentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - Action Events

- (void)initialize
{
    self.title = CLocalize(@"Comments");
    
    [btnSend setTitle:CLocalize(@"Send") forState:UIControlStateNormal];
    
    txtVComment.placeholder = CLocalize(@"Add Comment");
    [txtVComment setPlaceholderColor:CRGB(213, 213, 213)];
    txtVComment.font = CFontAvenirLTStd55Roman(13);
    txtVComment.textColor =  ColorDarkBule_0e293c;
    txtVComment.animateHeightChange = YES;
    txtVComment.delegate = self;
    [txtVComment setContentInset:UIEdgeInsetsMake(0, 5, 0, 5)];
    txtVComment.tintColor = ColorDarkBule_0e293c;
    txtVComment.backgroundColor = CRGB(242, 242, 242);
    txtVComment.layer.cornerRadius  = cnTxtHeight.constant/2;
    txtVComment.layer.masksToBounds = YES;
    
    
    //.....Set button gradient
    viewBottom.layer.shadowColor = CRGBA(0, 0, 0, 0.10) .CGColor;
    viewBottom.layer.shadowOpacity = .7f;
    
    arrCommentList = @[@{@"user_image":@"temp_user",@"user_name":@"Alexa Garison",@"comment_time":@"10 min. ago",@"comment":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats."},
                       @{@"user_image":@"temp_user",@"user_name":@"Alexa Garison",@"comment_time":@"10 min. ago",@"comment":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats."},
                       @{@"user_image":@"temp_user",@"user_name":@"Alexa Garison",@"comment_time":@"10 min. ago",@"comment":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats."},
                       @{@"user_image":@"temp_user",@"user_name":@"Alexa Garison",@"comment_time":@"10 min. ago",@"comment":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats."},
                       @{@"user_image":@"temp_user",@"user_name":@"Alexa Garison",@"comment_time":@"10 min. ago",@"comment":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats."},
                       @{@"user_image":@"temp_user",@"user_name":@"Alexa Garison",@"comment_time":@"10 min. ago",@"comment":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats."}];
    
   
    [tblVComments registerNib:[UINib nibWithNibName:@"MICommentsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MICommentsTableViewCell"];
    [tblVComments setContentInset:UIEdgeInsetsMake(15, 0, 0, 0)];
    [tblVComments setEstimatedRowHeight:105];
    [tblVComments setRowHeight:UITableViewAutomaticDimension];
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnSendClicked:(id)sender
{
    if([txtVComment.text length] == 0)
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CAddComment withTitle:@""];
}


#pragma mark - Growing textview delegate methods
#pragma mark -

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    cnTxtHeight.constant = height > 30 ? height : 30;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText = [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(finalText.length >= CCommentLength)
        return NO;
    
    return true;
}


#pragma mark - ScrollView Method
#pragma mark -

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [txtVComment resignFirstResponder];
}



# pragma mark
# pragma mark - UITableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCommentList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdetifier = @"MICommentsTableViewCell";
    MICommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnReply.hidden = YES;
    
    
    NSDictionary *dict = [arrCommentList objectAtIndex:indexPath.row];
    
    cell.imgVUser.image = [UIImage imageNamed:[dict valueForKey:@"user_image"]];
    cell.lblUserName.text = [dict valueForKey:@"user_name"];
    cell.lblCommentText.attributedText = [appDelegate setAttributedString:[dict valueForKey:@"comment"] andAlign:NSTextAlignmentLeft];
    cell.lblTime.text = [dict valueForKey:@"comment_time"];
    
    
    return cell;
}

@end
