//
//  MIMessageViewController.m
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import "MIMessageViewController.h"
#import "MIChatViewController.h"
#import "MISearchViewController.h"

@interface MIMessageViewController ()
{
    NSMutableArray *arrMessage;
    UISearchBar *searchBar;
}
@end

@implementation MIMessageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}



#pragma mark
#pragma mark - Set Initialize method

-(void)initialize
{
    self.title =  CLocalize(@"Message");
    
    //.....SET NAVIGATION BAR BUTTON
    
    if(_messageType == Other)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    }
    else
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    }
    
    
    self.navigationItem.rightBarButtonItem.tag = 0;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStylePlain target:self action:@selector(btnSearchClicked:)];
    
    
    arrMessage = [[NSMutableArray alloc] initWithObjects:
                  @{@"image":@"temp_user", @"user_name": @"Alexa Garison", @"message": @"Train your self to let go of",@"time": @"8:25 pm"},
                  @{@"image":@"temp_user", @"user_name": @"Lily Rechards", @"message": @"You have your moments.",@"time": @"9:40 pm"},
                  @{@"image":@"temp_user", @"user_name": @"Danish Wagh", @"message": @"R2 says the chances of survival are 725",@"time": @"3 hrs ago"},
                  @{@"image":@"temp_user", @"user_name": @"Haily Peterson Wagh",@"message": @"Train your self to let go of",@"time": @"4 hrs ago"},
                  @{@"image":@"temp_user", @"user_name": @"Alexa Garison",@"message": @"You have your moments.",@"time": @"6 hrs ago"},
                  @{@"image":@"temp_user", @"user_name": @"Amelia Nelson",@"message": @"R2 says the chances of survival are 725",@"time": @"8 hrs ago"},
                  @{@"image":@"temp_user", @"user_name": @"Irea Fitzgearald", @"message": @"Train your self to let go of",@"time": @"10 hrs ago"},
                  @{@"image":@"temp_user", @"user_name": @"Mia Blare",@"message": @"You have your moments. Not many of the",@"time": @"11 hrs ago"},nil];
    
    
    [tblMessage registerNib:[UINib nibWithNibName:@"MIMessageTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIMessageTableViewCell"];
    
}

- (void)setSerachBarOnNaviagtionBar
{
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 5, CViewWidth(self.navigationItem.titleView), self.navigationController.navigationBar.bounds.size.height)];
    [searchBar setImage:[UIImage imageNamed:@"search"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    [searchField setPlaceHolderColor:[UIColor whiteColor]];
    searchField.textColor = [UIColor whiteColor];
    [searchField addTarget:self action:@selector(searchTextChanged:) forControlEvents:UIControlEventEditingChanged];
    searchBar.tintColor = [UIColor whiteColor];
    searchBar.layer.cornerRadius = 2;
    searchBar.delegate = self;
    searchBar.placeholder = CLocalize(@"Search");
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor whiteColor];
    searchField.backgroundColor = CRGBA(255, 255, 255, 0.2);
    searchBar.showsCancelButton = NO;
    
    UIImage *imgClear = [UIImage imageNamed:@"search_close"];
    [searchBar setImage:imgClear forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    
    self.navigationItem.titleView = searchBar;
    
    
}



# pragma mark
# pragma mark - BarButton Action Events

- (void)searchTextChanged:(UITextField *)textField
{
    
}

- (void)btnSearchClicked:(UIBarButtonItem *)sender
{
    if(sender.tag == 0)
    {
        self.navigationItem.rightBarButtonItem.tag = 1;
        [self setSerachBarOnNaviagtionBar];
        sender.image = [UIImage imageNamed:@"camera_close"];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.tag = 0;
        self.navigationItem.titleView = nil;
        sender.image = [UIImage imageNamed:@"search"];
    }
}


- (void)btnMenuClicked
{
    [appDelegate resignKeyboard];
    
    if(_messageType == Other)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}




# pragma mark
# pragma mark - Searchbar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchField
{
    [searchField resignFirstResponder];
}




# pragma mark
# pragma mark - UITableView DataSource and Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrMessage.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdetifier = @"MIMessageTableViewCell";
    MIMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    NSDictionary *dict          = [arrMessage objectAtIndex:indexPath.row];
    cell.imgUserImage.image     = [UIImage imageNamed:[dict valueForKey:@"image"]];
    cell.lblUserName.text       = [dict valueForKey:@"user_name"];
    cell.lblMessageText.text    = [dict valueForKey:@"message"];
    cell.lblTime.text           = [dict valueForKey:@"time"];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIChatViewController *chatVC = [[MIChatViewController alloc] initWithNibName:@"MIChatViewController" bundle:nil];
    chatVC.strUsername = [[arrMessage objectAtIndex:indexPath.row] valueForKey:@"user_name"];
    [self.navigationController pushViewController:chatVC animated:YES];
}



@end
