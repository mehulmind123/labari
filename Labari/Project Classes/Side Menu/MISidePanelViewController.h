//
//  MISidePanelViewController.h
//  Labari
//
//  Created by mac-0005 on 3/21/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISidePanelViewController : UIViewController
{
    IBOutlet UIImageView *imgVUser;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblUserEmail;
    
    IBOutlet UITableView *tblVSideMenu;
}
@end
