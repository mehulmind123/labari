//
//  MIUploadPopupView.h
//  Labari
//
//  Created by mac-0006 on 21/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIUploadPopupView : UIView

+ (id)uploadView;

@property (strong, nonatomic) IBOutlet UIButton *btnImages;
@property (strong, nonatomic) IBOutlet UIButton *btnVideo;
@property (strong, nonatomic) IBOutlet UIButton *btnAudio;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UILabel *lblImage;
@property (strong, nonatomic) IBOutlet UILabel *lblVideo;
@property (strong, nonatomic) IBOutlet UILabel *lblAudio;
@end
