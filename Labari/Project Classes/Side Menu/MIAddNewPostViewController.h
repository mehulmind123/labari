//
//  MIAddNewPostViewController.h
//  Labari
//
//  Created by mac-0006 on 21/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    PublicBoxPost,
    InterestBoxPost
}postType;



@interface MIAddNewPostViewController : ParentViewController
{
    IBOutlet UITextField *txtCategory;
    IBOutlet UITextField *txtDay;
    
    IBOutlet UITextView *txtVAddTextInt;
    IBOutlet UITextView *txtVAddTextPublic;
    
    IBOutlet UIView *vInterest;
    IBOutlet UIView *vPublic;
    
    IBOutlet UIButton *btnPublic;
    IBOutlet UIButton *btnInterest;
    IBOutlet UIButton *btnUploadInterest;
    IBOutlet UIButton *btnUploadPublic;
    
    IBOutlet UIButton *btnNationalTxt;
    IBOutlet UIButton *btnInterNationalTxt;
    IBOutlet UIButton *btnNational;
    IBOutlet UIButton *btnInterNational;
    
  
    IBOutlet UILabel *lblPayText;
    
    IBOutlet NSLayoutConstraint *cnBottomVInterest;
    IBOutlet NSLayoutConstraint *cnBottomVPublic;
}

@property(nonatomic)BOOL isPublic;
@property (nonatomic , assign) postType typeOfPost;

@end
