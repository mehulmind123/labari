//
//  MIPopOverlayView.m
//  Labari
//
//  Created by mac-0006 on 21/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIPopOverlayView.h"

@implementation MIPopOverlayView

+ (id)popUpOverlay
{
    MIPopOverlayView *popUp = [[[NSBundle mainBundle] loadNibNamed:@"MIPopOverlayView" owner:nil options:nil] lastObject];
    
    CGSize size = [UIScreen mainScreen].bounds.size;
    popUp.frame = CGRectMake(0, 0, size.width, size.height);
    
    return popUp;
}



#pragma mark -
#pragma mark - Action Events

- (IBAction)btnCloseClicked:(UIButton *)sender
{
//    if (_shouldCloseOnClickOutside)
//    {
        UIView *subView = [self.subviews lastObject];
        if (self.presentType == PresentTypeCenter)
        {
            [UIView animateWithDuration:0.1 animations:^{
                subView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            } completion:^(BOOL finished)
             {
                 [self removeFromSuperview];
             }];
        }
        else
        {
            [UIView animateWithDuration:0.1 animations:^{
                CViewSetY(subView, CScreenHeight);
            } completion:^(BOOL finished)
             {
                 [self removeFromSuperview];
             }];
        }
  //  }
}


@end
