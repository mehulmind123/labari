//
//  MIFollowingViewController.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIFolloweringTableViewCell.h"
#import "MIMessageViewController.h"


typedef enum : NSUInteger
{
    Followers,
    Followings
    
}FollowType;


@interface MIFollowingViewController : ParentViewController
{
    IBOutlet UITableView *tblFollowers;
}

@property (nonatomic , assign) FollowType followType;

@end
