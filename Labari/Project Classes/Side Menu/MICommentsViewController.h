//
//  MICommentsViewController.h
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface MICommentsViewController : ParentViewController <HPGrowingTextViewDelegate>
{
    
    IBOutlet UITableView *tblVComments;
    IBOutlet UIView *viewBottom;
    IBOutlet HPGrowingTextView *txtVComment;
    IBOutlet UIButton *btnSend;
    
    IBOutlet NSLayoutConstraint *cnTxtHeight;
}


- (IBAction)btnSendClicked:(id)sender;

@end
