//
//  MISettingsViewController.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 21/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MISettingsViewController : ParentViewController
{
    
    IBOutlet UITableView *tblSetings;
    IBOutlet UIButton *btnLogout;

}

- (IBAction)btnLogoutIsClicked:(id)sender;

@end
