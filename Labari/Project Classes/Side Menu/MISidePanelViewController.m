//
//  MISidePanelViewController.m
//  Labari
//
//  Created by mac-0005 on 3/21/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MISidePanelViewController.h"
#import "SuperNavigationController.h"
#import "MIHomeViewController.h"
#import "MISearchViewController.h"
#import "MIMessageViewController.h"
#import "MISettingsViewController.h"
#import "MIRequestsViewController.h"
#import "MIFriendsViewController.h"
#import "MIEditProfileViewController.h"

#import "MISidePanelTableViewCell.h"

@interface MISidePanelViewController ()
{
    NSArray *arrSideMenu;
    NSMutableArray *arrSelected;
    NSIndexPath *index;
}
@end

@implementation MISidePanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    
    if(Is_iPhone_5)
        lblUserName.font = CFontAvenirLTStd85Heavy(18);
    
    arrSelected = [[NSMutableArray alloc] init];
    
    imgVUser.layer.cornerRadius = CViewWidth(imgVUser)/2;
    imgVUser.layer.masksToBounds = YES;
    
    arrSideMenu = @[CLocalize(@"Home"),
                    CLocalize(@"Search"),
                    CLocalize(@"Friends"),
                    CLocalize(@"Messsage"),
                    CLocalize(@"Request"),
                    CLocalize(@"Settings")];
    
    [tblVSideMenu registerNib:[UINib nibWithNibName:@"MISidePanelTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISidePanelTableViewCell"];
}



# pragma mark
# pragma mark - Action Event

- (IBAction)btnEditProfile:(id)sender
{
    MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
    [appDelegate openMenuViewcontroller:editProfileVC animated:YES];
}

- (IBAction)btnCancelClicked:(id)sender
{
    switch (index.row) {
        case 0:
        {
            MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:homeVC animated:YES];
            
            break;
        }
        case 1:
        {
            MISearchViewController *searchVC = [[MISearchViewController alloc] initWithNibName:@"MISearchViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:searchVC animated:YES];
            break;
        }
        case 2:
        {
            MIFriendsViewController *friendVC = [[MIFriendsViewController alloc] initWithNibName:@"MIFriendsViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:friendVC animated:YES];
            break;
        }
        case 3:
        {
            MIMessageViewController *messageVC = [[MIMessageViewController alloc] initWithNibName:@"MIMessageViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:messageVC animated:YES];
            break;
        }
        case 4:
        {
            MIRequestsViewController *requestVC = [[MIRequestsViewController alloc] initWithNibName:@"MIRequestsViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:requestVC animated:YES];
            break;
        }
        case 5:
        {
            MISettingsViewController *settingVC = [[MISettingsViewController alloc] initWithNibName:@"MISettingsViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:settingVC animated:YES];
            break;
        }
    }
}

# pragma mark
# pragma mark - UITableview Datasource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSideMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (CScreenWidth * 45) / 375;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MISidePanelTableViewCell";
    MISidePanelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.lblTitle.text = [arrSideMenu objectAtIndex:indexPath.row];
    cell.lblTitle.textColor = CRGB(14, 41, 60);
    
    if(arrSelected.count > 0)
    {
        if([arrSelected containsObject:[arrSideMenu objectAtIndex:indexPath.row]])
            cell.lblTitle.textColor = ColorTurquoise_19bbd5;
        else
            cell.lblTitle.textColor = CRGB(14, 41, 60);
    }
    
    
    
    cell.lblRequestCount.hidden = indexPath.row != 4;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    index = indexPath;
    
    if(arrSelected.count >0)
        [arrSelected removeAllObjects];
    
    if(![arrSelected containsObject:[arrSideMenu objectAtIndex:indexPath.row]])
        [arrSelected addObject:[arrSideMenu objectAtIndex:indexPath.row]];
    [tblVSideMenu reloadData];
    
    
    switch (indexPath.row) {
        case 0:
        {
            MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:homeVC animated:YES];
            
            break;
        }
        case 1:
        {
            MISearchViewController *searchVC = [[MISearchViewController alloc] initWithNibName:@"MISearchViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:searchVC animated:YES];
            break;
        }
        case 2:
        {
            MIFriendsViewController *friendVC = [[MIFriendsViewController alloc] initWithNibName:@"MIFriendsViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:friendVC animated:YES];
            break;
        }
        case 3:
        {
            MIMessageViewController *messageVC = [[MIMessageViewController alloc] initWithNibName:@"MIMessageViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:messageVC animated:YES];
            break;
        }
        case 4:
        {
            MIRequestsViewController *requestVC = [[MIRequestsViewController alloc] initWithNibName:@"MIRequestsViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:requestVC animated:YES];
            break;
        }
        case 5:
        {
            MISettingsViewController *settingVC = [[MISettingsViewController alloc] initWithNibName:@"MISettingsViewController" bundle:nil];
            [appDelegate openMenuViewcontroller:settingVC animated:YES];
            break;
        }
    }
}

@end
