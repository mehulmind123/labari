//
//  MIRequestDetailViewController.h
//  Labari
//
//  Created by mac-0006 on 27/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIRequestDetailViewController : ParentViewController <UITableViewDelegate, UITableViewDataSource, HPGrowingTextViewDelegate>
{
    IBOutlet UIView *vCreatorInfo;
    IBOutlet UIView *vDetail;
    IBOutlet UIView *vAddress;
    IBOutlet UIView *vComment;
    IBOutlet UIView *vMainContent;
    IBOutlet UIView *vShadow;
    
    IBOutlet UIScrollView *scrollV;
    
    IBOutlet UIImageView *imgVBanner;
    IBOutlet UIImageView *imgVCreator;
    
    IBOutlet UILabel *lblCreatorName;
    IBOutlet UILabel *lblTime;
    IBOutlet UILabel *lblPrice;
    IBOutlet UILabel *lblDetail;
    IBOutlet UILabel *lblAddress;
    IBOutlet UILabel *lblAddressTitle;
    IBOutlet UILabel *lblComment;
    
    IBOutlet UIButton *btnViewAll;
    IBOutlet UIButton *btnSend;
    IBOutlet HPGrowingTextView *txtComment;
    IBOutlet UITableView *tblComment;
    
    IBOutlet NSLayoutConstraint *cnTblHeight;
    IBOutlet NSLayoutConstraint *cnTxtViewHeight;
}

@property (strong, nonatomic) NSDictionary *dictDetail;

@end
