//
//  MINotificationViewController.m
//  DemoStreamingURL
//
//  Created by mac-0003 on 21/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import "MINotificationViewController.h"

@interface MINotificationViewController ()
{
    NSMutableArray *arrNotificationData;
}
@end

@implementation MINotificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




#pragma mark
#pragma mark - General Method

-(void)initialize
{
    self.title = CLocalize(@"Notifications"); 
    
    arrNotificationData = [[NSMutableArray alloc] initWithObjects:
                           @{@"message": @"Oh no ! Your prediction of spain to beat Italy was incorrect!",@"time": @"8:25 pm"},
                           @{@"message": @"10 incorrect in a roq! Almost like winning but not #redscreen",@"time": @"8:25 pm"},
                           @{@"message": @"Oh no! Your prediction of Queen's Park to beat Albion Rovers was incorrect!",@"time": @"8:25 pm"},
                           @{@"message": @"Great Great! Your predication of Bury to beat Scunthorpe United was correct!",@"time": @"8:25 pm"},
                           @{@"message": @"Oh no ! Your prediction of spain to beat Italy was incorrect!",@"time": @"8:25 pm"},
                           @{@"message": @"10 incorrect in a roq! Almost like winning but not #redscreen",@"time": @"8:25 pm"},
                           @{@"message": @"Oh no! Your prediction of Queen's Park to beat Albion Rovers was incorrect!",@"time": @"8:25 pm"}
                           ,nil];
    
    
    [tblNotification registerNib:[UINib nibWithNibName:@"MINotificationTableViewCell" bundle:nil] forCellReuseIdentifier:@"MINotificationTableViewCell"];
    [tblNotification setEstimatedRowHeight:UITableViewAutomaticDimension];
}






# pragma mark
# pragma mark - UITableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNotificationData.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [arrNotificationData objectAtIndex:indexPath.row];
    
    static NSString *cellIdetifier = @"MINotificationTableViewCell";
    MINotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
   
    if (indexPath.row < 3)
    {
        cell.objView.backgroundColor = ColorSkyBule_2c9dde;
        cell.lblNotificationMessage.textColor = ColorDarkBule_0e293c;
    }
    else
    {
        cell.objView.backgroundColor = ColorGray_C6DAEC;
        cell.lblNotificationMessage.textColor = ColorGray_C6DAEC;
    }
    
    
    cell.lblNotificationMessage.text = [dict valueForKey:@"message"];
    cell.lblTime.text = [dict valueForKey:@"time"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
