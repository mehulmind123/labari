//
//  MIUploadPopupView.m
//  Labari
//
//  Created by mac-0006 on 21/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIUploadPopupView.h"

@implementation MIUploadPopupView


+ (id)uploadView
{
    MIUploadPopupView *uploadView = [[[NSBundle mainBundle] loadNibNamed:@"MIUploadPopupView" owner:nil options:nil] lastObject];
    
    CViewSetSize(uploadView, CScreenWidth, 165);
    
    [uploadView.lblAudio setText:CLocalize(@"Audio").uppercaseString];
    [uploadView.lblImage setText:CLocalize(@"Images").uppercaseString];
    [uploadView.lblVideo setText:CLocalize(@"Video").uppercaseString];
    
    return uploadView;
}


@end
