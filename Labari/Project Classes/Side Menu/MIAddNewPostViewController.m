//
//  MIAddNewPostViewController.m
//  Labari
//
//  Created by mac-0006 on 21/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIAddNewPostViewController.h"
#import "MIPopOverlayView.h"
#import "MIUploadPopupView.h"
#import "TakeVideoViewController.h"
#import "RecordAudioViewController.h"

@interface MIAddNewPostViewController ()

@end

@implementation MIAddNewPostViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}



#pragma mark - General Method
#pragma mark -

- (void)initialize
{
    self.title = @"Add New Post";
    
    if(_typeOfPost == PublicBoxPost)
        [self btnSelectType:btnPublic];
    else
        [self btnSelectType:btnInterest];
    
    [txtDay setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(40, 15)];
    [txtCategory setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(40, 15)];
    
    lblPayText.layer.cornerRadius = 35;
    lblPayText.layer.masksToBounds = YES;
    
    btnNationalTxt.selected = btnNational.selected = YES;
    [self setAttributedUserText:@"$3"];
    
   
    [txtDay setPickerData:@[@"Test1",@"Test2",@"Test3"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    [txtCategory setPickerData:@[@"Test1",@"Test2",@"Test3"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    
}



#pragma mark - Attributed Method
#pragma mark -

- (void)setAttributedUserText:(NSString *)price
{
    NSString *string = [NSString stringWithFormat:@"You can reach unlimited people with just %@ pay with paypal to reach out to millions.", price];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedText addAttributes: @{NSFontAttributeName: CFontAvenirLTStd65Meduim(15), NSForegroundColorAttributeName:ColorTurquoise_19bbd5} range: [string rangeOfString:price]];
    
    lblPayText.attributedText = attributedText;
}




#pragma mark - Button Action
#pragma mark -

- (IBAction)btnSubmitClicked:(UIButton *)sender
{
    if(btnInterest.selected)
      [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSelectPostType:(UIButton *)sender
{
    btnNational.selected = btnInterNational.selected = btnNationalTxt.selected = btnInterNationalTxt.selected = NO;
    
    if(sender.tag == 0)
    {
        btnNationalTxt.selected = btnNational.selected = YES;
        [self setAttributedUserText:@"$3"];
    }
    else
    {
        btnInterNational.selected = btnInterNationalTxt.selected = YES;
        [self setAttributedUserText:@"$10"];
    }
}

- (IBAction)btnSelectType:(UIButton *)sender
{
    btnInterest.selected = btnPublic.selected = NO;
    vPublic.hidden = vInterest.hidden = YES;
    
    if(sender.tag == 0)
    {
        btnPublic.selected = YES;
        vPublic.hidden = NO;
        
        cnBottomVPublic.priority = 999;
        cnBottomVInterest.priority = UILayoutPriorityDefaultHigh - 1;
    }
    else
    {
        btnInterest.selected = YES;
        vInterest.hidden = NO;
        
        cnBottomVInterest.priority = 999;
        cnBottomVPublic.priority = UILayoutPriorityDefaultHigh - 1 ;
    }
}

- (IBAction)btnUploadClicked:(UIButton *)sender
{
    MIUploadPopupView *uploadView = [MIUploadPopupView uploadView];
    [self presentPopUp:uploadView from:PresentTypeBottom];
    
    
    
    //.....click events
    
    [uploadView.btnImages touchUpInsideClicked:^{
        
        [self dismissPopUp:uploadView];
        
        TakeVideoViewController *takeVideoVC = [[TakeVideoViewController alloc] initWithNibName:@"TakeVideoViewController" bundle:nil];
        takeVideoVC.uploadType = imageUpload;
        [self.navigationController presentViewController:takeVideoVC animated:YES completion:nil];
        
    }];
    
    [uploadView.btnVideo touchUpInsideClicked:^{
        
        [self dismissPopUp:uploadView];
        
        TakeVideoViewController *takeVideoVC = [[TakeVideoViewController alloc] initWithNibName:@"TakeVideoViewController" bundle:nil];
        takeVideoVC.uploadType = videoUpload;
        [self.navigationController presentViewController:takeVideoVC animated:YES completion:nil];
        
    }];
    
    [uploadView.btnAudio touchUpInsideClicked:^{
        
        [self dismissPopUp:uploadView];
        
        RecordAudioViewController *recordAudioVC = [[RecordAudioViewController alloc] initWithNibName:@"RecordAudioViewController" bundle:nil];
        [self.navigationController pushViewController:recordAudioVC animated:YES];
        
    }];
    
    [uploadView.btnClose touchUpInsideClicked:^{
        [self dismissPopUp:uploadView];
    }];
}

@end
