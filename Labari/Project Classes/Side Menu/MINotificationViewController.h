//
//  MINotificationViewController.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 21/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MINotificationTableViewCell.h"
#import "Constants.h"

#define CFontAvenirLTRoman(fontSize) [UIFont fontWithName:@"avenir-lt-roman" size:fontSize]

@interface MINotificationViewController : ParentViewController
{
    IBOutlet UITableView *tblNotification;
}
@end
