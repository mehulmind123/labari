//
//  MIAddNewRequestViewController.m
//  Labari
//
//  Created by mac-0005 on 3/24/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIAddNewRequestViewController.h"

@interface MIAddNewRequestViewController ()

@end

@implementation MIAddNewRequestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Add New Request";
    
    [txtWantToDo setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(30, 15)];
    [txtSelectCountry setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(30, 15)];
    [txtSelectCity setRightImage:[UIImage imageNamed:@"arrow"] withSize:CGSizeMake(30, 15)];
    
    txtVDescription.layer.cornerRadius = 5;
    btnUploadImage.layer.cornerRadius = 5;
    
    
    
    
    [txtWantToDo setPickerData:@[@"Test1",@"Test2",@"Test3"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    [txtWantToBuy setPickerData:@[@"Test1",@"Test2",@"Test3"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    [txtSelectCountry setPickerData:@[@"India",@"USA",@"Japan"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
    
    [txtSelectCity setPickerData:@[@"Ahmedabad",@"Vadodara",@"Surat"] update:^(NSString *text, NSInteger row, NSInteger component) {
    }];
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnUploadImageClicked:(id)sender
{
    [self selectImageWithEditing:YES animation:YES completion:^(UIImage *image)
     {
         if (image)
         {
             [btnUploadImage setImage:image forState:UIControlStateNormal];
         }
     }];
}

- (IBAction)btnSubmitClicked:(id)sender {
}

@end
