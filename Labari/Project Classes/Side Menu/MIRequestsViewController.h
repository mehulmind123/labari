//
//  MIRequestsViewController.h
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIRequestsViewController : ParentViewController <UISearchBarDelegate>
{
    
    IBOutlet UIButton *btnMatchingRequest;
    IBOutlet UIButton *btnOtherRequest;
    IBOutlet UIButton *btnMyOwnRequest;
    
    IBOutlet UIView *viewTab;
    IBOutlet UIView *viewSelectedTab;
    
    IBOutlet UITableView *tblVRequest;
}


- (IBAction)btnTabClicked:(id)sender;
- (IBAction)btnAddNewRequestClicked:(id)sender;

@end
