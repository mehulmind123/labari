//
//  MIFriendsViewController.h
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    FromSideMenu,
    FromProfile
}Friend;


@interface MIFriendsViewController : ParentViewController <UISearchBarDelegate>
{
    IBOutlet UIView *viewTab;
    IBOutlet UIView *viewSelectedTab;
    
    IBOutlet UITableView *tblVFriends;
    
    IBOutlet UIButton *btnAllFriends;
    IBOutlet UIButton *btnPendingRequest;
}

@property (nonatomic , assign) Friend friendType;
- (IBAction)btnTabClicked:(id)sender;

@end
