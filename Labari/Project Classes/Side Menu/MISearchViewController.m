//
//  MISearchViewController.m
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MISearchViewController.h"
#import "MICommentsViewController.h"
#import "MIProfileViewController.h"
#import "MIHomeDetailViewController.h"
#import "MIReportPopUpView.h"

#import "MIAllFriendsTableViewCell.h"
#import "MIHomeTableViewCell.h"
#import "MIHomeAudioTableViewCell.h"
#import "MIHomeImageTableViewCell.h"

@interface MISearchViewController ()
{
    NSMutableArray *arrSearchData;
    MIReportPopUpView *reportPopUp;
    NSArray *arrList;
    UISearchBar *searchBar;
    
    AVPlayer *audioPlayer;
    MPMoviePlayerController *videoPlayer;
    NSTimeInterval duration, currentTime;
    AVPlayerItem *currentItem;
    NSIndexPath *index;
}
@end

@implementation MISearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [viewSelectedTab layoutIfNeeded];
    [viewTab layoutIfNeeded];
    
    // HEADER VIEW GRADIENT
    MIGradientLayer *viewHeaderGradient = [MIGradientLayer gradientLayerWithFrame:viewTab.bounds];
    [viewTab.layer insertSublayer:viewHeaderGradient atIndex:0];
    
   
    // SELECTED TAB GRADIENT
    MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrameSeperator:viewSelectedTab.bounds];
    [viewSelectedTab.layer insertSublayer:gradient atIndex:0];
    viewSelectedTab.layer.cornerRadius = 2;
    viewSelectedTab.layer.masksToBounds = YES;
}


# pragma mark
# pragma mark - General Method

- (void)initialize
{
    
    [btnPosts setTitle:CLocalize(@"Posts").uppercaseString forState:UIControlStateNormal];
    [btnUsers setTitle:CLocalize(@"Users").uppercaseString forState:UIControlStateNormal];
    
    
    //.....SET NAVIGATION BAR BUTTON
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 5, CViewWidth(self.navigationItem.titleView), self.navigationController.navigationBar.bounds.size.height)];
    [searchBar setImage:[UIImage imageNamed:@"search"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    [searchField setPlaceHolderColor:[UIColor whiteColor]];
    searchField.textColor = [UIColor whiteColor];
    [searchField addTarget:self action:@selector(searchTextChanged:) forControlEvents:UIControlEventEditingChanged];
    searchBar.tintColor = [UIColor whiteColor];
    searchBar.layer.cornerRadius = 2;
    searchBar.delegate = self;
    searchBar.placeholder = CLocalize(@"Search"); 
    searchField.backgroundColor = CRGBA(255, 255, 255, 0.2);
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor whiteColor];
    searchBar.showsCancelButton = NO;
    UIImage *imgClear = [UIImage imageNamed:@"search_close"];
    [searchBar setImage:imgClear forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    self.navigationItem.titleView = searchBar;
    
    
    arrSearchData = [[NSMutableArray alloc] initWithObjects:
                     @{@"image":@"temp_user", @"user_name": @"Alexa Garison"},
                     @{@"image":@"temp_user", @"user_name": @"Lily Rechards"},
                     @{@"image":@"temp_user", @"user_name": @"Danish Wagh"},
                     @{@"image":@"temp_user", @"user_name": @"Haily Peterson Wagh"},
                     @{@"image":@"temp_user", @"user_name": @"Alexa Garison"},
                     @{@"image":@"temp_user", @"user_name": @"Amelia Nelson"},
                     @{@"image":@"temp_user", @"user_name": @"Irea Fitzgearald"},
                     @{@"image":@"temp_user", @"user_name": @"Mia Blare"},nil];
    
    
    
    arrList = @[@{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"image",@"post_image":@[@"temp_post1",@"temp_post2",@"temp_post3",@"temp_post4",@"temp_post5"],@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"audio",@"audio_url":@"http://mobicreation.in/engage/eng-uploads/track/201703071152282885.mp3",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"video",@"video_url":@"shutterstock_v6228647",@"video_type":@"mov",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1",@"video_thumb":@"temp_banner"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"audio",@"audio_url":@"http://mobicreation.in/engage/eng-uploads/track/201703091232561071.mp3",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"image",@"post_image":@[@"temp_post1",@"temp_post2",@"temp_post3",@"temp_post4",@"temp_post5"],@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"video",@"video_url":@"Sample Videos",@"video_type":@"mp4",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1",@"video_thumb":@"temp_banner"}];
    
    
    
    [tblVSearchUser registerNib:[UINib nibWithNibName:@"MIAllFriendsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAllFriendsTableViewCell"];
    [tblVSearchUser registerNib:[UINib nibWithNibName:@"MIHomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeTableViewCell"];
    [tblVSearchUser registerNib:[UINib nibWithNibName:@"MIHomeAudioTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeAudioTableViewCell"];
    [tblVSearchUser registerNib:[UINib nibWithNibName:@"MIHomeImageTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeImageTableViewCell"];
    
    
    
    btnUsers.selected = YES;
}






# pragma mark
# pragma mark - Action Events

- (void)btnMenuClicked
{
    [appDelegate resignKeyboard];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (IBAction)btnTabClicked:(UIButton *)sender
{
    if (sender.selected)
        return;
    
    btnUsers.selected = btnPosts.selected = NO;
    sender.selected = YES;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        int position = (CScreenWidth * 12) / 375;
        
        [viewSelectedTab setConstraintConstant:CViewX(sender) + position toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
        [viewSelectedTab.superview layoutIfNeeded];
        
    }];
    
    switch (sender.tag)
    {
        case 101:
        {
            btnUsers.selected = YES;
        }
            break;
            
        case 102:
        {
            btnPosts.selected = YES;
        }
            break;
    }
    
    [tblVSearchUser reloadData];
}

- (void)searchTextChanged:(UITextField *)textField
{
    
}


- (void)showReportPopup
{
    NSMutableArray *arrSelected = [[NSMutableArray alloc] init];
    reportPopUp = [MIReportPopUpView customReportView:PublicBox interest:arrSelected];
    
    //.....Clicked events
    
    [reportPopUp.btnCancel touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [reportPopUp.btnSubmit touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [appDelegate.window addSubview:reportPopUp];
}


- (void)updateAudioProgress
{
    
}




# pragma mark
# pragma mark - Searchbar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchField
{
    [searchField resignFirstResponder];
}




# pragma mark
# pragma mark - UITableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(btnUsers.selected)
        return arrSearchData.count;
    
    return arrList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(btnUsers.selected)
        return 70;
    else
    {
        if([[[arrList objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"audio"])
            return (CScreenWidth * 275) / 375;
        
        return (CScreenWidth * 445) / 375;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnUsers.selected)
    {
        static NSString *cellIdetifier = @"MIAllFriendsTableViewCell";
        MIAllFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        [cell.btnUnFriend hideByWidth:YES];
        
        NSDictionary *dict = [arrSearchData objectAtIndex:indexPath.row];
        
        cell.imgVUser.image   = [UIImage imageNamed:[dict valueForKey:@"image"]];
        cell.lblUserName.text = [dict valueForKey:@"user_name"];
        
        
        //.....Click events
        [cell.btnUnFriend touchUpInsideClicked:^{
            
        }];
        
        return cell;
    }
    else
    {
        
        //TODO: Image cell
        
        NSDictionary *dict = [arrList objectAtIndex:indexPath.row];
        
        if([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            static NSString *identifier = @"MIHomeImageTableViewCell";
            MIHomeImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
            cell.lblUserName.text = [dict valueForKey:@"user_name"];
            cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
            cell.lblPostDetail.text = [dict valueForKey:@"desc"];
            cell.lblPostTime.text = [dict valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
            
            cell.arrImg = [dict valueForKey:@"post_image"]; // Pass Here array of Detail
            [cell scrollViewDidScroll:cell.collImg];
            cell.pageV.currentPage = 0;
            [cell.pageV setNumberOfPages:cell.arrImg.count];
            
            
            
            //.....Cell button clicked Event
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
                
            }];
            
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
                index = indexPath;
            }];
            
            return cell;
        }
        
        //TODO: Audio cell
        else if([[dict valueForKey:@"type"] isEqualToString:@"audio"])
        {
            static NSString *identifier = @"MIHomeAudioTableViewCell";
            MIHomeAudioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
            cell.lblUserName.text = [dict valueForKey:@"user_name"];
            cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
            cell.lblPostDetail.text = [dict valueForKey:@"desc"];
            cell.lblPostTime.text = [dict valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
         /*
            currentItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:[dict valueForKey:@"audio_url"]]];
            currentTime = CMTimeGetSeconds(currentItem.currentTime);
            duration = CMTimeGetSeconds(currentItem.duration);
            
            
            cell.lblStartTime.text = [NSString stringWithFormat:@"%.2f",currentTime];
            cell.lblEndTime.text = [NSString stringWithFormat:@"%f", CMTimeGetSeconds(range.start) + CMTimeGetSeconds(range.duration)];
            
          */
            //.....Cell button clicked Event
            
            [cell.btnPlayPause touchUpInsideClicked:^{
                
                if(cell.btnPlayPause.selected)
                {
                    //Pause
                    cell.btnPlayPause.selected = NO;
                    [audioPlayer pause];
                }
                else
                {
                    //Play
                    index = indexPath;
                    cell.btnPlayPause.selected = YES;
                    audioPlayer = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithURL:[NSURL URLWithString:[dict valueForKey:@"audio_url"]]]];
                    [audioPlayer play];
                    
                    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateAudioProgress) userInfo:nil repeats:YES];
                }
            }];
            
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
            }];
            
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
                index = indexPath;
            }];
            
            return cell;
            
        }
        //TODO: Video cell
        else
        {
            static NSString *identifier = @"MIHomeTableViewCell";
            MIHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
      
            cell.imgVPost.image         =[UIImage imageNamed:[dict valueForKey:@"video_thumb"]];
            cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
            cell.lblUserName.text = [dict valueForKey:@"user_name"];
            cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
            cell.lblPostDetail.text = [dict valueForKey:@"desc"];
            cell.lblPostTime.text = [dict valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
            
            [cell.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
            [videoPlayer.view removeFromSuperview];
            cell.btnPlayPause.selected = NO;
            
            //.....Cell button clicked Event
            
            [cell.btnPlayPause touchUpInsideClicked:^{
                
                if(cell.btnPlayPause.selected)
                {
                    //Pause
                    cell.btnPlayPause.selected = NO;
                    [cell.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
                    [videoPlayer stop];
                }
                else
                {
                    //Play
                    index = indexPath;
                    cell.btnPlayPause.selected = YES;
                    [cell.btnPlayPause setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    videoPlayer = [appDelegate playVideoWithResource:[dict valueForKey:@"video_url"] andType:[dict valueForKey:@"video_type"]];
                    [videoPlayer.view setFrame:cell.imgVPost.bounds];
                    [cell.imgVPost addSubview:videoPlayer.view];
                    [videoPlayer play];
                }
            }];
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
                
            }];
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
                index = indexPath;
            }];
            
            return cell;
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

@end
