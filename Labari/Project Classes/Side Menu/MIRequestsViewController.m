//
//  MIRequestsViewController.m
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIRequestsViewController.h"
#import "MIAddNewRequestViewController.h"
#import "MIRequestDetailViewController.h"
#import "MIRequestTableViewCell.h"

@interface MIRequestsViewController ()
{
    UISearchBar *searchBar;
    NSMutableArray *arrRequestList;
}
@end

@implementation MIRequestsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [viewSelectedTab layoutIfNeeded];
    [viewTab layoutIfNeeded];
    
    
    // HEADER VIEW GRADIENT
    MIGradientLayer *viewHeaderGradient = [MIGradientLayer gradientLayerWithFrame:viewTab.bounds];
    [viewTab.layer insertSublayer:viewHeaderGradient atIndex:0];
    
    
    // SELECTED TAB GRADIENT
    MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrameSeperator:viewSelectedTab.bounds];
    [viewSelectedTab.layer insertSublayer:gradient atIndex:0];
    viewSelectedTab.layer.cornerRadius = 2;
    viewSelectedTab.layer.masksToBounds = YES;
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Requests");
    
    
    [btnMatchingRequest setTitle:CLocalize(@"Matching").uppercaseString forState:UIControlStateNormal];
    [btnOtherRequest setTitle:CLocalize(@"Other").uppercaseString forState:UIControlStateNormal];
    [btnMyOwnRequest setTitle:CLocalize(@"My Own").uppercaseString forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    
    self.navigationItem.rightBarButtonItem.tag = 0;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStylePlain target:self action:@selector(btnSearchClicked:)];
    
    
    
    
    arrRequestList = [[NSMutableArray alloc] initWithArray:@[@{@"item_image":@"request_image",@"item_name":@"Toyoto Etios",@"price":@"$19,200",@"desc":@"Toyota Etios Company conditios car signal hens used car. Company conditions with taxi parcing and full insurance and tax paid",@"address":@"Mumbai, India",@"post_time":@"1 month ago"},
                                                             @{@"item_image":@"request_image",@"item_name":@"Toyoto Etios",@"price":@"$19,200",@"desc":@"Toyota Etios Company conditios car signal hens used car. Company conditions with taxi parcing and full insurance and tax paid",@"address":@"Mumbai, India",@"post_time":@"1 month ago"},
                                                             @{@"item_image":@"request_image",@"item_name":@"Toyoto Etios",@"price":@"$19,200",@"desc":@"Toyota Etios Company conditios car signal hens used car. Company conditions with taxi parcing and full insurance and tax paid",@"address":@"Mumbai, India",@"post_time":@"1 month ago"},
                                                             @{@"item_image":@"request_image",@"item_name":@"Toyoto Etios",@"price":@"$19,200",@"desc":@"Toyota Etios Company conditios car signal hens used car. Company conditions with taxi parcing and full insurance and tax paid",@"address":@"Mumbai, India",@"post_time":@"1 month ago"},
                                                             @{@"item_image":@"request_image",@"item_name":@"Toyoto Etios",@"price":@"$19,200",@"desc":@"Toyota Etios Company conditios car signal hens used car. Company conditions with taxi parcing and full insurance and tax paid",@"address":@"Mumbai, India",@"post_time":@"1 month ago"},
                                                             @{@"item_image":@"request_image",@"item_name":@"Toyoto Etios",@"price":@"$19,200",@"desc":@"Toyota Etios Company conditios car signal hens used car. Company conditions with taxi parcing and full insurance and tax paid",@"address":@"Mumbai, India",@"post_time":@"1 month ago"}]];
    
    
    [tblVRequest registerNib:[UINib nibWithNibName:@"MIRequestTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIRequestTableViewCell"];
    
    
    btnMatchingRequest.selected = YES;
}

- (void)setSerachBarOnNaviagtionBar
{
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 5, CViewWidth(self.navigationItem.titleView), self.navigationController.navigationBar.bounds.size.height)];
    [searchBar setImage:[UIImage imageNamed:@"search"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    [searchField setPlaceHolderColor:[UIColor whiteColor]];
    searchField.textColor = [UIColor whiteColor];
    [searchField addTarget:self action:@selector(searchTextChanged:) forControlEvents:UIControlEventEditingChanged];
    searchBar.tintColor = [UIColor whiteColor];
    searchBar.layer.cornerRadius = 2;
    searchBar.delegate = self;
    searchBar.placeholder = CLocalize(@"Search");
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor whiteColor];
    searchField.backgroundColor = CRGBA(255, 255, 255, 0.2);
    searchBar.showsCancelButton = NO;
    
    UIImage *imgClear = [UIImage imageNamed:@"search_close"];
    [searchBar setImage:imgClear forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    
    self.navigationItem.titleView = searchBar;
    
}




# pragma mark
# pragma mark - BarButton Action Events

- (void)searchTextChanged:(UITextField *)textField
{
    
}


- (void)btnSearchClicked:(UIBarButtonItem *)sender
{
    if(sender.tag == 0)
    {
        self.navigationItem.rightBarButtonItem.tag = 1;
        [self setSerachBarOnNaviagtionBar];
        sender.image = [UIImage imageNamed:@"camera_close"];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.tag = 0;
        self.navigationItem.titleView = nil;
        sender.image = [UIImage imageNamed:@"search"];
        
    }
}


- (void)btnMenuClicked
{
    [appDelegate resignKeyboard];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}




# pragma mark
# pragma mark - Searchbar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchField
{
    [searchField resignFirstResponder];
}




# pragma mark
# pragma mark - Action Events


- (IBAction)btnTabClicked:(UIButton *)sender
{
    if (sender.selected)
        return;
    
    [appDelegate resignKeyboard];
    
    btnMatchingRequest.selected = btnOtherRequest.selected = btnMyOwnRequest.selected = NO;
    sender.selected = YES;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [viewSelectedTab setConstraintConstant:CViewX(sender) + 3 toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
        [viewSelectedTab.superview layoutIfNeeded];
        
    }];
    
    switch (sender.tag)
    {
        case 101:
        {
            btnMatchingRequest.selected = YES;
        }
            break;
            
        case 102:
        {
            btnOtherRequest.selected = YES;
        }
            break;
            
        case 103:
        {
            btnMyOwnRequest.selected = YES;
        }
            break;
    }
    
    [tblVRequest reloadData];
}

- (IBAction)btnAddNewRequestClicked:(id)sender
{
    MIAddNewRequestViewController *objMessage = [[MIAddNewRequestViewController alloc]initWithNibName:@"MIAddNewRequestViewController" bundle:nil];
    [self.navigationController pushViewController:objMessage animated:YES];
}





# pragma mark
# pragma mark - UITableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrRequestList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdetifier = @"MIRequestTableViewCell";
    MIRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    NSDictionary *dict = [arrRequestList objectAtIndex:indexPath.row];
    
    cell.imgVItem.image     = [UIImage imageNamed:[dict valueForKey:@"item_image"]];
    cell.lblItemName.text   = [dict valueForKey:@"item_name"];
    cell.lblPrice.text      = [dict valueForKey:@"price"];
    cell.lblItemDesc.text   = [dict valueForKey:@"desc"];
    cell.lblAddress.text    = [dict valueForKey:@"address"];
    cell.lblTimeOfPost.text = [dict valueForKey:@"post_time"];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIRequestDetailViewController *requestDetailVC = [[MIRequestDetailViewController alloc] initWithNibName:@"MIRequestDetailViewController" bundle:nil];
    requestDetailVC.dictDetail = [arrRequestList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:requestDetailVC animated:YES];
}

@end
