//
//  MISearchViewController.h
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISearchViewController : ParentViewController<UISearchBarDelegate>
{
    IBOutlet UIView *viewTab;
    IBOutlet UIView *viewSelectedTab;
    
    IBOutlet UITableView *tblVSearchUser;
    
    IBOutlet UIButton *btnUsers;
    IBOutlet UIButton *btnPosts;
}


- (IBAction)btnTabClicked:(id)sender;

@end
