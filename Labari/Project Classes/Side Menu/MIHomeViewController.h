//
//  MIHomeViewController.h
//  Labari
//
//  Created by mac-0005 on 3/20/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIHomeViewController : ParentViewController <AVAudioPlayerDelegate>
{
    IBOutlet UITableView *tblVPost;

    IBOutlet UIButton *btnAddPost;
    IBOutlet UIButton *btnPublicBox;
    IBOutlet UIButton *btnWorldBox;
    IBOutlet UIButton *btnInterestBox;

    
    IBOutlet UIView *viewSelected;
    IBOutlet UIView *viewHeader;
}

- (IBAction)btnTabClicked:(id)sender;

- (IBAction)btnAddPostClicked:(id)sender;

@end
