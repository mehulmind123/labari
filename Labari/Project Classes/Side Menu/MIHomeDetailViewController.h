//
//  MIHomeDetailViewController.h
//  Labari
//
//  Created by mac-0006 on 29/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "ParentViewController.h"

@interface MIHomeDetailViewController : ParentViewController <UITableViewDelegate, UITableViewDataSource, AVAudioPlayerDelegate, HPGrowingTextViewDelegate>
{
    IBOutlet UITableView *tblDetail;
    IBOutlet UIView *viewComment;
    IBOutlet UIButton *btnSend;
    
    IBOutlet HPGrowingTextView *txtVComment;
    IBOutlet NSLayoutConstraint *cnTxtViewHeight;
}

@property(strong, nonatomic)NSDictionary *dictDetail;

@end
