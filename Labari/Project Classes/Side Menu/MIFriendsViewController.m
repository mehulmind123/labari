//
//  MIFriendsViewController.m
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIFriendsViewController.h"
#import "MISearchViewController.h"

#import "MIAllFriendsTableViewCell.h"
#import "MIUserListCommonTableViewCell.h"

@interface MIFriendsViewController ()
{
    NSMutableArray *arrFriendsData;
    UISearchBar *searchBar;
}
@end

@implementation MIFriendsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [viewSelectedTab layoutIfNeeded];
    [viewTab layoutIfNeeded];
    
    // HEADER VIEW GRADIENT
    MIGradientLayer *viewHeaderGradient = [MIGradientLayer gradientLayerWithFrame:viewTab.bounds];
    [viewTab.layer insertSublayer:viewHeaderGradient atIndex:0];
    
  
    // SELECTED TAB GRADIENT
    MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrameSeperator:viewSelectedTab.bounds];
    [viewSelectedTab.layer insertSublayer:gradient atIndex:0];
    viewSelectedTab.layer.cornerRadius = 2;
    viewSelectedTab.layer.masksToBounds = YES;
    
    
}

# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Friends");
    
    [btnAllFriends setTitle:CLocalize(@"All Friends").uppercaseString forState:UIControlStateNormal];
    [btnPendingRequest setTitle:CLocalize(@"Pending Request").uppercaseString forState:UIControlStateNormal];
    
    
    if(_friendType == FromProfile)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    }
    else
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    }
    
    self.navigationItem.rightBarButtonItem.tag = 0;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStylePlain target:self action:@selector(btnSearchClicked:)];
  
    
    arrFriendsData = [[NSMutableArray alloc] initWithObjects:
                      @{@"image":@"temp_user", @"user_name": @"Alexa Garison"},
                      @{@"image":@"temp_user", @"user_name": @"Lily Rechards"},
                      @{@"image":@"temp_user", @"user_name": @"Danish Wagh"},
                      @{@"image":@"temp_user", @"user_name": @"Haily Peterson Wagh"},
                      @{@"image":@"temp_user", @"user_name": @"Alexa Garison"},
                      @{@"image":@"temp_user", @"user_name": @"Amelia Nelson"},
                      @{@"image":@"temp_user", @"user_name": @"Irea Fitzgearald"},
                      @{@"image":@"temp_user", @"user_name": @"Mia Blare"},nil];
    
   
    [tblVFriends registerNib:[UINib nibWithNibName:@"MIUserListCommonTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIUserListCommonTableViewCell"];
    [tblVFriends registerNib:[UINib nibWithNibName:@"MIAllFriendsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAllFriendsTableViewCell"];
    
    btnAllFriends.selected = YES;
}


- (void)setSerachBarOnNaviagtionBar
{
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 5, CViewWidth(self.navigationItem.titleView), self.navigationController.navigationBar.bounds.size.height)];
    [searchBar setImage:[UIImage imageNamed:@"search"]
       forSearchBarIcon:UISearchBarIconSearch
                  state:UIControlStateNormal];
    
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    [searchField setPlaceHolderColor:[UIColor whiteColor]];
    searchField.textColor = [UIColor whiteColor];
    [searchField addTarget:self action:@selector(searchTextChanged:) forControlEvents:UIControlEventEditingChanged];
    searchBar.tintColor = [UIColor whiteColor];
    searchBar.layer.cornerRadius = 2;
    searchBar.delegate = self;
    searchBar.placeholder = CLocalize(@"Search");
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor whiteColor];
    searchField.backgroundColor = CRGBA(255, 255, 255, 0.2);
    searchBar.showsCancelButton = NO;
    
    UIImage *imgClear = [UIImage imageNamed:@"search_close"];
    [searchBar setImage:imgClear forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    
    self.navigationItem.titleView = searchBar;
    
}



# pragma mark
# pragma mark - BarButton Action Events


- (void)searchTextChanged:(UITextField *)textField
{
}

- (void)btnSearchClicked:(UIBarButtonItem *)sender
{
    if(sender.tag == 0)
    {
        self.navigationItem.rightBarButtonItem.tag = 1;
        [self setSerachBarOnNaviagtionBar];
        sender.image = [UIImage imageNamed:@"camera_close"];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.tag = 0;
        self.navigationItem.titleView = nil;
        sender.image = [UIImage imageNamed:@"search"];
    }
}





# pragma mark
# pragma mark - Searchbar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchField
{
    [searchField resignFirstResponder];
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnTabClicked:(UIButton *)sender
{
    if (sender.selected)
        return;
    
    btnAllFriends.selected = btnPendingRequest.selected = NO;
    sender.selected = YES;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        int position = (CScreenWidth * 12) / 375;
        
        [viewSelectedTab setConstraintConstant:CViewX(sender) + position toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
        [viewSelectedTab.superview layoutIfNeeded];
        
    }];
    
    switch (sender.tag)
    {
        case 101:
        {
            btnAllFriends.selected = YES;
        }
            break;
            
        case 102:
        {
            btnPendingRequest.selected = YES;
        }
            break;
    }
    
    [tblVFriends reloadData];
}


- (void)btnMenuClicked
{
    [appDelegate resignKeyboard];
    
    if(_friendType == FromProfile)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}



# pragma mark
# pragma mark - UITableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrFriendsData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnAllFriends.selected)
    {
        static NSString *cellIdetifier = @"MIAllFriendsTableViewCell";
        MIAllFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        NSDictionary *dict = [arrFriendsData objectAtIndex:indexPath.row];
        
        cell.imgVUser.image = [UIImage imageNamed:[dict valueForKey:@"image"]];
        cell.lblUserName.text = [dict valueForKey:@"user_name"];
        
      
        [cell.btnUnFriend touchUpInsideClicked:^{
            
            UIAlertView *alt = [[UIAlertView alloc] initWithTitle:@"" message:CLocalize(@"Are you sure you want to remove this friend?") delegate:self cancelButtonTitle:CLocalize(@"No") otherButtonTitles:CLocalize(@"Yes"), nil];
            
            [alt showAlerViewWithHandlerBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
             {
                 if(buttonIndex == 0)
                 {}
             }];
        }];
        return cell;
    }
    else
    {
        static NSString *cellIdetifier = @"MIUserListCommonTableViewCell";
        MIUserListCommonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        NSDictionary *dict = [arrFriendsData objectAtIndex:indexPath.row];
        
        cell.imgVUser.image = [UIImage imageNamed:[dict valueForKey:@"image"]];
        cell.lblUserName.text = [dict valueForKey:@"user_name"];
        
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

@end
