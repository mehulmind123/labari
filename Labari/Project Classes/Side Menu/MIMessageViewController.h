//
//  MIMessageViewController.h
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIMessageTableViewCell.h"
#import "MINotificationViewController.h"

typedef enum : NSUInteger
{
    sideMenu,
    Other
}Message;

@interface MIMessageViewController : ParentViewController <UISearchBarDelegate>
{
    IBOutlet UITableView *tblMessage;
}


@property (nonatomic , assign) Message messageType;
@end
