//
//  MIHomeViewController.m
//  Labari
//
//  Created by mac-0005 on 3/20/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIHomeViewController.h"
#import "MICommentsViewController.h"
#import "MIAddNewPostViewController.h"
#import "MIProfileViewController.h"
#import "MIFriendsViewController.h"
#import "MINotificationViewController.h"
#import "MIRequestsViewController.h"
#import "MIEditProfileViewController.h"
#import "MIMessageViewController.h"
#import "MIHomeDetailViewController.h"

#import "MIHomeAudioTableViewCell.h"
#import "MIHomeTableViewCell.h"
#import "MIHomeImageTableViewCell.h"

#import "MIReportPopUpView.h"

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

@interface MIHomeViewController ()
{
    MIReportPopUpView *reportPopUp;
    UIButton *btnNotification;
    AVPlayer *audioPlayer;
    MPMoviePlayerController *videoPlayer;
    
    NSTimeInterval duration, currentTime;
    NSArray *arrList;
    AVPlayerItem *currentItem;
    NSIndexPath *index;
    int progress;
}
@end

@implementation MIHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
    [self setNotificationAnimation:btnNotification];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [viewHeader layoutIfNeeded];
    [viewSelected layoutIfNeeded];
    
    
    // HEADER VIEW GRADIENT
    MIGradientLayer *viewHeaderGradient = [MIGradientLayer gradientLayerWithFrame:viewHeader.bounds];
    [viewHeader.layer insertSublayer:viewHeaderGradient atIndex:0];
    
    
    // SELECTED TAB GRADIENT
    MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrameSeperator:viewSelected.bounds];
    [viewSelected.layer insertSublayer:gradient atIndex:0];
    viewSelected.layer.cornerRadius = 2;
    viewSelected.layer.masksToBounds = YES;
}



# pragma mark
# pragma mark - General method

- (void)initialize
{
    [self.navigationController setNavigationBarHidden:NO];
    
    
    [btnPublicBox setTitle:CLocalize(@"Public Box").uppercaseString forState:UIControlStateNormal];
    [btnWorldBox setTitle:CLocalize(@"World Box").uppercaseString forState:UIControlStateNormal];
    [btnInterestBox setTitle:CLocalize(@"Interest Box").uppercaseString forState:UIControlStateNormal];

    
    
    //.....SET NAVIGATION BAR BUTTON
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height/2, CScreenWidth/2, 30)];
    imgView.image = [UIImage imageNamed:@"labari_white"];
    imgView.contentMode = UIViewContentModeCenter;
    self.navigationItem.titleView = imgView;
    
    
    
    arrList = @[@{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"image",@"post_image":@[@"temp_post1",@"temp_post2",@"temp_post3",@"temp_post4",@"temp_post5"],@"desc":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats. new audio. Listen to our newest playlist. Back to Fall Beats.Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats. new audio. Listen to our newest playlist. Back to Fall Beats.Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats. new audio. Listen to our newest playlist. Back to Fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"audio",@"audio_url":@"http://mobicreation.in/engage/eng-uploads/track/201703071152282885.mp3",@"desc":@"Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats. new audio. Listen to our newest playlist. Back to Fall Beats.Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats. new audio. Listen to our newest playlist. Back to Fall Beats.Tap into a world of fresh new audio. Listen to our newest playlist. Back to Fall Beats. new audio. Listen to our newest playlist. Back to Fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"video",@"video_url":@"shutterstock_v6228647",@"video_type":@"mov",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1",@"video_thumb":@"temp_banner"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"audio",@"audio_url":@"http://mobicreation.in/engage/eng-uploads/track/201703091232561071.mp3",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"image",@"post_image":@[@"temp_post1",@"temp_post2",@"temp_post3",@"temp_post4",@"temp_post5"],@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1"},
                @{@"user_profile":@"temp_user",@"user_name":@"Alexa Garison",@"time":@"10 min. ago",@"post_title":@"Morning jumping of joy at seaside",@"type":@"video",@"video_url":@"Sample Videos",@"video_type":@"mp4",@"desc":@"Tap into a world of fresh new audio.Listen to our newset playlist.Back to fall Beats.",@"like_count":@"11",@"comment_count":@"1",@"video_thumb":@"temp_banner"}];
    
    
    
    
    
    [tblVPost registerNib:[UINib nibWithNibName:@"MIHomeAudioTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeAudioTableViewCell"];
    [tblVPost registerNib:[UINib nibWithNibName:@"MIHomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeTableViewCell"];
    [tblVPost registerNib:[UINib nibWithNibName:@"MIHomeImageTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeImageTableViewCell"];
    
    [tblVPost setContentInset:UIEdgeInsetsMake(0, 0, 12, 0)];

    
    viewHeader.layer.shadowColor = [UIColor blackColor].CGColor;
    viewHeader.layer.shadowOffset = CGSizeMake(0, 2);
    viewHeader.layer.shadowOpacity = 0.2f;
    viewHeader.layer.shadowRadius = 2.0f;
    viewHeader.layer.masksToBounds = NO;
    
    
    btnPublicBox.selected = YES;
    [self setRightBarButton];
    progress = 0;
}

- (void)setRightBarButton
{
    btnNotification = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [btnNotification setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    UIBarButtonItem *notitication = [[UIBarButtonItem alloc] initWithCustomView:btnNotification];
    
    
    [btnNotification touchUpInsideClicked:^{
        
        MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
        [self.navigationController pushViewController:notificationVC animated:YES];
        
    }];
    
    
    if (btnPublicBox.selected || btnWorldBox.selected)
    {
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:notitication, nil]];
    }
    else if (btnInterestBox.selected)
    {
        UIBarButtonItem *btnInterest = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"star"] style:UIBarButtonItemStylePlain target:self action:@selector(btnInterestClicked)];
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:notitication, btnInterest, nil]];
    }
    
}


- (void)setNotificationAnimation:(UIButton *)button
{
    CGAffineTransform leftWobble = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(-15.0));
    CGAffineTransform rightWobble = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(0.0));
    
    button.transform = leftWobble;  // starting point
    
    [UIView beginAnimations:@"wobble" context:(__bridge void * _Nullable)(button)];
    [UIView setAnimationRepeatAutoreverses:YES];
    [UIView setAnimationRepeatCount:10]; // adjustable
    [UIView setAnimationDuration:0.150];
    [UIView setAnimationDelegate:self];
    button.transform = rightWobble; // end here & auto-reverse
    [UIView commitAnimations];
    
}



# pragma mark
# pragma mark - Action Events

- (void)btnMenuClicked
{
    [appDelegate resignKeyboard];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (void)updateAudioProgress
{
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index.row inSection:0];
//    MIHomeAudioTableViewCell * cell = [tblVPost cellForRowAtIndexPath:indexPath];
    
//    if(currentItem.status == AVPlayerItemStatusReadyToPlay)
//    {
//        progress =+0.5;
//        [cell.progressAudio setProgress:currentTime  animated:YES];
//        cell.lblStartTime.text = [NSString stringWithFormat:@"%.2f",currentTime];
   // }
    
  
//    currentTime = CMTimeGetSeconds(currentItem.currentTime);
//    duration = CMTimeGetSeconds(currentItem.duration);
    
   // [cell.progressAudio setProgress:currentTime/duration  animated:YES];
    
}


- (void)reportPopupWith:(reportType)reportType
{
    NSMutableArray *arrSelected = [[NSMutableArray alloc] init];
    reportPopUp = [MIReportPopUpView customReportView:reportType interest:arrSelected];
    
    //.....Clicked events
    
    
    [reportPopUp.btnSubmit touchUpInsideClicked:^{
        
        if(reportPopUp.reportType == InterestBox)
        {
            if(reportPopUp.arrSelected.count < 1)
            {
                [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CSelectMinInterest withTitle:@""];
                return;
            }
            else if(reportPopUp.arrSelected.count > 3)
            {
                [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CSelectMaxInterest withTitle:@""];
                return;
            }
        }

        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index.row inSection:0];
        
        
        if([[[arrList objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
        {
            MIHomeImageTableViewCell * cell = [tblVPost cellForRowAtIndexPath:indexPath];
            cell.btnReport.selected = YES;
        }
        else if ([[[arrList objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"audio"])
        {
            MIHomeAudioTableViewCell * cell = [tblVPost cellForRowAtIndexPath:indexPath];
            cell.btnReport.selected = YES;
        }
        else
        {
            MIHomeTableViewCell * cell = [tblVPost cellForRowAtIndexPath:indexPath];
            cell.btnReport.selected = YES;
        }
        
        [tblVPost reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [reportPopUp removeFromSuperview];
    }];
    
    [appDelegate.window addSubview:reportPopUp];
}

- (void)btnInterestClicked
{
    [self reportPopupWith:InterestBox];
}

- (void)btnNotificationClicked
{
    MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
    [self.navigationController pushViewController:notificationVC animated:YES];
}

- (IBAction)btnAddPostClicked:(id)sender
{
    MIAddNewPostViewController *addPostVC = [[MIAddNewPostViewController alloc] initWithNibName:@"MIAddNewPostViewController" bundle:nil];

    if(btnInterestBox.selected)
        addPostVC.typeOfPost = InterestBoxPost;
    else
       addPostVC.typeOfPost = PublicBoxPost;
   
    [self.navigationController pushViewController:addPostVC animated:YES];
}

- (IBAction)btnTabClicked:(UIButton *)sender
{
    if (sender.selected)
        return;
    
    [appDelegate resignKeyboard];
    
    btnPublicBox.selected = btnWorldBox.selected = btnInterestBox.selected = NO;
    sender.selected = YES;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [viewSelected setConstraintConstant:CViewX(sender) + 3 toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
        [viewSelected.superview layoutIfNeeded];
        
    }];
    
    switch (sender.tag)
    {
        case 101:
        {
            btnPublicBox.selected = YES;
            [self setRightBarButton];
        }
            break;
            
        case 102:
        {
            btnWorldBox.selected = YES;
            [self setRightBarButton];
        }
            break;
            
        case 103:
        {
            btnInterestBox.selected = YES;
            [self setRightBarButton];
        }
            break;
    }
}





# pragma mark
# pragma mark - UITableview Datasource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[arrList objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"audio"])
        return (CScreenWidth * 275) / 375;
    
    return (CScreenWidth * 445) / 375;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //TODO: Image cell
    
    NSDictionary *dict = [arrList objectAtIndex:indexPath.row];
    
    if([[dict valueForKey:@"type"] isEqualToString:@"image"])
    {
        static NSString *identifier = @"MIHomeImageTableViewCell";
        MIHomeImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
        cell.lblUserName.text = [dict valueForKey:@"user_name"];
        cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
        cell.lblPostDetail.text = [dict valueForKey:@"desc"];
        cell.lblPostTime.text = [dict valueForKey:@"time"];
        
       
        [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
        [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
        
        
        [cell.collImg reloadData];
        cell.arrImg = [dict valueForKey:@"post_image"]; // Pass here array of Detail
        cell.pageV.currentPage = 0;
        [cell.pageV setNumberOfPages:cell.arrImg.count];
        
        
        
        //.....Cell button clicked Event
        
        [cell.btnUserProfile touchUpInsideClicked:^{
            
            MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
            profileVC.profileType = OtherUserProfile;
            [self.navigationController pushViewController:profileVC animated:YES];
        }];
        
        [cell.btnLike touchUpInsideClicked:^{
            cell.btnLike.selected = !cell.btnLike.selected;
        }];
        
        
        [cell.btnComment touchUpInsideClicked:^{
            
            MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
            [self.navigationController pushViewController:commentVC animated:YES];
            
        }];
        
        [cell.btnShare touchUpInsideClicked:^{
            
            MIRequestsViewController *friendVC = [[MIRequestsViewController alloc] initWithNibName:@"MIRequestsViewController" bundle:nil];
            [self.navigationController pushViewController:friendVC animated:YES];
            
        }];
        
        [cell.btnReport touchUpInsideClicked:^{
            [self reportPopupWith:PublicBox];
            index = indexPath;
        }];
        
        return cell;
    }
    
    //TODO: Audio cell
    else if([[dict valueForKey:@"type"] isEqualToString:@"audio"])
    {
        static NSString *identifier = @"MIHomeAudioTableViewCell";
        MIHomeAudioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
        cell.lblUserName.text = [dict valueForKey:@"user_name"];
        cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
        cell.lblPostDetail.text = [dict valueForKey:@"desc"];
        cell.lblPostTime.text = [dict valueForKey:@"time"];
        
        
        
        [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
        [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
        
    /*
        currentItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:[dict valueForKey:@"audio_url"]]];
        currentTime = CMTimeGetSeconds(currentItem.currentTime);
        duration = CMTimeGetSeconds(currentItem.duration);
        
        
        cell.lblStartTime.text = [NSString stringWithFormat:@"%.2f",currentTime];
        cell.lblEndTime.text = [NSString stringWithFormat:@"%f", CMTimeGetSeconds(range.start) + CMTimeGetSeconds(range.duration)]; */
        
        
        //.....Cell button clicked Event
        
        [cell.btnPlayPause touchUpInsideClicked:^{
            
            if(cell.btnPlayPause.selected)
            {
                //Pause
                cell.btnPlayPause.selected = NO;
                [audioPlayer pause];
            }
            else
            {
                //Play
                index = indexPath;
                cell.btnPlayPause.selected = YES;
                audioPlayer = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithURL:[NSURL URLWithString:[dict valueForKey:@"audio_url"]]]];
                [audioPlayer play];
                
                [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateAudioProgress) userInfo:nil repeats:YES];
            }
        }];
        
        
        [cell.btnUserProfile touchUpInsideClicked:^{
            
            MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
            profileVC.profileType = OtherUserProfile;
            [self.navigationController pushViewController:profileVC animated:YES];
        }];
        
        
        [cell.btnLike touchUpInsideClicked:^{
            cell.btnLike.selected = !cell.btnLike.selected;
        }];
        
        
        [cell.btnComment touchUpInsideClicked:^{
            
            MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
            [self.navigationController pushViewController:commentVC animated:YES];
        }];
        
        
        [cell.btnShare touchUpInsideClicked:^{
            
            MIRequestsViewController *friendVC = [[MIRequestsViewController alloc] initWithNibName:@"MIRequestsViewController" bundle:nil];
            [self.navigationController pushViewController:friendVC animated:YES];
            
        }];
        
        [cell.btnReport touchUpInsideClicked:^{
            [self reportPopupWith:PublicBox];
            index = indexPath;
        }];
        
        return cell;
        
    }
    //TODO: Video cell
    else
    {
        static NSString *identifier = @"MIHomeTableViewCell";
        MIHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        
        
        cell.imgVUserProfile.image = [UIImage imageNamed:[dict valueForKey:@"user_profile"]] ;
        cell.imgVPost.image = [UIImage imageNamed:[dict valueForKey:@"video_thumb"]];
        cell.lblUserName.text = [dict valueForKey:@"user_name"];
        cell.lblPostTitle.text = [dict valueForKey:@"post_title"];
        cell.lblPostDetail.text = [dict valueForKey:@"desc"];
        cell.lblPostTime.text = [dict valueForKey:@"time"];
        
        
        [cell.btnLike setTitle:[dict valueForKey:@"like_count"] forState:UIControlStateNormal];
        [cell.btnComment setTitle:[dict valueForKey:@"comment_count"] forState:UIControlStateNormal];
        
        [videoPlayer.view removeFromSuperview];
        cell.btnPlayPause.selected = NO;
        
        //.....Cell button clicked Event
        
        [cell.btnPlayPause touchUpInsideClicked:^{
            
            if(cell.btnPlayPause.selected)
            {
                //Pause
                cell.btnPlayPause.selected = NO;
                [cell.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
                [videoPlayer stop];
            }
            else
            {
                //Play
                index = indexPath;
                cell.btnPlayPause.selected = YES;
                [cell.btnPlayPause setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
               
                videoPlayer = [appDelegate playVideoWithResource:[dict valueForKey:@"video_url"] andType:[dict valueForKey:@"video_type"]];
                [videoPlayer.view setFrame:cell.imgVPost.bounds];
                [cell.imgVPost addSubview:videoPlayer.view];
                [videoPlayer play];
            }
        }];
        
        [cell.btnUserProfile touchUpInsideClicked:^{
            
            MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
            profileVC.profileType = OtherUserProfile;
            [self.navigationController pushViewController:profileVC animated:YES];
        }];
        
        
        [cell.btnLike touchUpInsideClicked:^{
            cell.btnLike.selected = !cell.btnLike.selected;
        }];
        
        
        [cell.btnComment touchUpInsideClicked:^{
            
            MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
            [self.navigationController pushViewController:commentVC animated:YES];
            
        }];
        
        [cell.btnShare touchUpInsideClicked:^{
            
            MIRequestsViewController *friendVC = [[MIRequestsViewController alloc] initWithNibName:@"MIRequestsViewController" bundle:nil];
            [self.navigationController pushViewController:friendVC animated:YES];
            
        }];
        
        [cell.btnReport touchUpInsideClicked:^{
            [self reportPopupWith:PublicBox];
            index = indexPath;
        }];
        
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIHomeDetailViewController *detailVC = [[MIHomeDetailViewController alloc] initWithNibName:@"MIHomeDetailViewController" bundle:nil];
    detailVC.dictDetail = [arrList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
    
}

@end
