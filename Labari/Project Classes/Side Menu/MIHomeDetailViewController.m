//
//  MIHomeDetailViewController.m
//  Labari
//
//  Created by mac-0006 on 29/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIHomeDetailViewController.h"
#import "MICommentsViewController.h"
#import "MIProfileViewController.h"
#import "MIReportPopUpView.h"

#import "MIHomeAudioTableViewCell.h"
#import "MIHomeTableViewCell.h"
#import "MICommentsTableViewCell.h"
#import "MIHomeImageTableViewCell.h"
#import "MIHomeDetailTableViewCell.h"
#import "MIHomeAudioDetailTableViewCell.h"


@interface MIHomeDetailViewController ()
{
    MIReportPopUpView *reportPopUp;
    NSArray *arrImage;
    
    AVPlayer *audioPlayer;
    MPMoviePlayerController *videoPlayer;
    NSTimeInterval duration, currentTime;
    AVPlayerItem *currentItem;
}
@end

@implementation MIHomeDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




#pragma mark - General Method
#pragma mark -

- (void)initialize
{
    self.title = [_dictDetail stringValueForJSON:@"post_title"];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"detail_page_share"] style:UIBarButtonItemStylePlain target:self action:@selector(btnShareClicked)];
    
    
    [self setShadowView:viewComment];
    [self setShadowView:tblDetail];
    
    [btnSend setTitle:CLocalize(@"Send") forState:UIControlStateNormal];
    txtVComment.placeholder = CLocalize(@"Add Comment");
    [txtVComment setPlaceholderColor:CRGB(213, 213, 213)];
    txtVComment.font = CFontAvenirLTStd55Roman(13);
    txtVComment.textColor =  ColorDarkBule_0e293c;
    txtVComment.animateHeightChange = YES;
    txtVComment.delegate = self;
    [txtVComment setContentInset:UIEdgeInsetsMake(0, 5, 0, 5)];
    txtVComment.tintColor = ColorDarkBule_0e293c;
    txtVComment.backgroundColor = CRGB(242, 242, 242);
    txtVComment.layer.cornerRadius  = cnTxtViewHeight.constant/2;
    txtVComment.layer.masksToBounds = YES;
    
    
    [tblDetail registerNib:[UINib nibWithNibName:@"MIHomeDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeDetailTableViewCell"];
    [tblDetail registerNib:[UINib nibWithNibName:@"MIHomeAudioDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHomeAudioDetailTableViewCell"];
    [tblDetail registerNib:[UINib nibWithNibName:@"MICommentsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MICommentsTableViewCell"];
    
    
   // [tblDetail setEstimatedRowHeight:455];
    [tblDetail setRowHeight:UITableViewAutomaticDimension];
    
}



#pragma mark -
#pragma mark - Function

- (void)btnShareClicked
{
    NSString *textToShare = @"";
    UIImage *imgToShare = [UIImage imageNamed:@"logo"];
    NSArray *objectsToShare = @[textToShare, imgToShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)setShadowView:(UIView *)view
{
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeZero;
    view.layer.shadowOpacity = 0.2f;
    view.layer.shadowRadius = 2.0f;
    view.layer.masksToBounds = NO;
}

- (void)showReportPopup
{
    NSMutableArray *arrSelected = [[NSMutableArray alloc] init];
    reportPopUp = [MIReportPopUpView customReportView:PublicBox interest:arrSelected];
    
    //.....Clicked events
    
    [reportPopUp.btnCancel touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [reportPopUp.btnSubmit touchUpInsideClicked:^{
        [reportPopUp removeFromSuperview];
    }];
    
    [appDelegate.window addSubview:reportPopUp];
}



- (void)updateAudioProgress
{
    /*
     MIHomeAudioDetailTableViewCell * cell = [tblDetail cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
     
     [cell.progressAudio setProgress:audioPlayer.currentTime/audioPlayer.duration animated:YES];
     cell.lblStartTime.text = [NSString stringWithFormat:@"%.2f",audioPlayer.duration-audioPlayer.currentTime];
     */
}



#pragma mark - UIButton Action Event
#pragma mark -

- (IBAction)btnCommentSendClicked:(id)sender
{
    if([txtVComment.text length] == 0)
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CAddComment withTitle:@""];
}


#pragma mark - Growing textview delegate methods
#pragma mark -

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    cnTxtViewHeight.constant = height > 30 ? height : 30;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText = [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(finalText.length >= CCommentLength)
        return NO;
    
    return true;
}


#pragma mark - ScrollView Method
#pragma mark -

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [txtVComment resignFirstResponder];
}




# pragma mark
# pragma mark - UITableview Datasource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 1)
        return 2;
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 1)
        return 30;
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == 1)
        return 40;
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 1)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, 30)];
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(23, 10, CScreenWidth-23, 20)];
        
        view.backgroundColor = ColorWhite_FFFFFF;
        lblTitle.text = CLocalize(@"Comments");
        lblTitle.font = CFontAvenirLTStd65Meduim(12);
        lblTitle.textColor = ColorDarkBule_0e293c;
        
        [view addSubview:lblTitle];
        return view;
        
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, 40)];
        UIButton *btnViewAll = [[UIButton alloc] initWithFrame:CGRectMake(23, 0, CScreenWidth-23, 40)];
        
        view.backgroundColor = ColorWhite_FFFFFF;
        [btnViewAll setTitle:CLocalize(@"View All Comments") forState:UIControlStateNormal];
        btnViewAll.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;        [btnViewAll setTitleColor:ColorTurquoise_19bbd5 forState:UIControlStateNormal];
        [btnViewAll.titleLabel setFont:CFontAvenirLTStd65Meduim(12)];
        
        
        [view addSubview:btnViewAll];
        
        [btnViewAll touchUpInsideClicked:^{
            
            MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
            [self.navigationController pushViewController:commentVC animated:YES];
        }];
        
        return view;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        
        //TODO: Image cell
        
        
        if([[_dictDetail valueForKey:@"type"] isEqualToString:@"image"])
        {
            static NSString *identifier = @"MIHomeDetailTableViewCell";
            MIHomeDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.btnPostImg.hidden = YES;
            cell.pageV.hidden      = NO;
            
            
            cell.imgVUserProfile.image  = [UIImage imageNamed:[_dictDetail valueForKey:@"user_profile"]] ;
            cell.lblUserName.text       = [_dictDetail valueForKey:@"user_name"];
            cell.lblPostTitle.text      = [_dictDetail valueForKey:@"post_title"];
            cell.lblPostDetail.text     = [_dictDetail valueForKey:@"desc"];
            cell.lblPostTime.text       = [_dictDetail valueForKey:@"time"];
            
            [cell.btnLike setTitle:[_dictDetail valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[_dictDetail valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
            
            cell.arrImg = [_dictDetail valueForKey:@"post_image"]; // Pass Here array of Detail
            [cell scrollViewDidScroll:cell.collImg];
            cell.pageV.currentPage = 0;
            [cell.pageV setNumberOfPages:cell.arrImg.count];
            
            
            
            //.....Cell button clicked Event
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
                
            }];
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
                // index = indexPath;
            }];
            
            return cell;
        }
        
        //TODO: Audio cell
        else if([[_dictDetail valueForKey:@"type"] isEqualToString:@"audio"])
        {
            static NSString *identifier = @"MIHomeAudioDetailTableViewCell";
            MIHomeAudioDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.imgVUserProfile.image = [UIImage imageNamed:[_dictDetail valueForKey:@"user_profile"]] ;
            cell.lblUserName.text       = [_dictDetail valueForKey:@"user_name"];
            cell.lblPostTitle.text      = [_dictDetail valueForKey:@"post_title"];
            cell.lblPostDetail.text     = [_dictDetail valueForKey:@"desc"];
            cell.lblPostTime.text       = [_dictDetail valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[_dictDetail valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[_dictDetail valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
            /*
             currentItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:[_dictDetail valueForKey:@"audio_url"]]];
             currentTime = CMTimeGetSeconds(currentItem.currentTime);
             duration = CMTimeGetSeconds(currentItem.duration);
             
             
             cell.lblStartTime.text = [NSString stringWithFormat:@"%.2f",currentTime];
             cell.lblEndTime.text = [NSString stringWithFormat:@"%f", CMTimeGetSeconds(range.start) + CMTimeGetSeconds(range.duration)];
             */
            
            
            //.....Cell button clicked Event
            
            [cell.btnPlayPause touchUpInsideClicked:^{
                
                if(cell.btnPlayPause.selected)
                {
                    //Pause
                    cell.btnPlayPause.selected = NO;
                    [audioPlayer pause];
                }
                else
                {
                    //Play
                    //   index = indexPath;
                    cell.btnPlayPause.selected = YES;
                    audioPlayer = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithURL:[NSURL URLWithString:[_dictDetail valueForKey:@"audio_url"]]]];
                    [audioPlayer play];
                    
                    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateAudioProgress) userInfo:nil repeats:YES];
                }
            }];
            
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
            }];
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
                //index = indexPath;
            }];
            
            return cell;
            
        }
        //TODO: Video cell
        else
        {
            static NSString *identifier = @"MIHomeDetailTableViewCell";
            MIHomeDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.btnPostImg.hidden  = NO;
            cell.pageV.hidden       = YES;
            
            
            cell.arrImg = @[[_dictDetail valueForKey:@"video_thumb"]];
            
            cell.imgVUserProfile.image  = [UIImage imageNamed:[_dictDetail valueForKey:@"user_profile"]] ;
            cell.lblUserName.text       = [_dictDetail valueForKey:@"user_name"];
            cell.lblPostTitle.text      = [_dictDetail valueForKey:@"post_title"];
            cell.lblPostDetail.text     = [_dictDetail valueForKey:@"desc"];
            cell.lblPostTime.text       = [_dictDetail valueForKey:@"time"];
            
            
            [cell.btnLike setTitle:[_dictDetail valueForKey:@"like_count"] forState:UIControlStateNormal];
            [cell.btnComment setTitle:[_dictDetail valueForKey:@"comment_count"] forState:UIControlStateNormal];
            
            [videoPlayer.view removeFromSuperview];
            cell.btnPostImg.selected = NO;
            
            
            //.....Cell button clicked Event
            
            [cell.btnPostImg touchUpInsideClicked:^{
                
                if(cell.btnPostImg.selected)
                {
                    //Pause
                    cell.btnPostImg.selected = NO;
                    [cell.btnPostImg setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
                    [videoPlayer stop];
                }
                else
                {
                    //Play
                    
                    // index = indexPath;
                    cell.btnPostImg.selected = YES;
                    [cell.btnPostImg setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    videoPlayer = [appDelegate playVideoWithResource:[_dictDetail valueForKey:@"video_url"] andType:[_dictDetail valueForKey:@"video_type"]];
                    [videoPlayer.view setFrame:cell.collImg.bounds];
                    [cell.collImg addSubview:videoPlayer.view];
                    [videoPlayer play];
                }
            }];
            
            [cell.btnUserProfile touchUpInsideClicked:^{
                
                MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
                profileVC.profileType = OtherUserProfile;
                [self.navigationController pushViewController:profileVC animated:YES];
            }];
            
            
            [cell.btnLike touchUpInsideClicked:^{
                cell.btnLike.selected = !cell.btnLike.selected;
            }];
            
            
            [cell.btnComment touchUpInsideClicked:^{
                
                MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
                [self.navigationController pushViewController:commentVC animated:YES];
                
            }];
            
            
            [cell.btnReport touchUpInsideClicked:^{
                [self showReportPopup];
                // index = indexPath;
            }];
            
            return cell;
        }
    }
    else
    {
        static NSString *identifier = @"MICommentsTableViewCell";
        MICommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.btnReply hideByWidth:YES];
        [cell.viewSeprater hideByHeight:YES];
        
        return cell;
    }
    
    return nil;
}


@end
