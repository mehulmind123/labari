//
//  MISettingsViewController.m
//  DemoStreamingURL
//
//  Created by mac-0003 on 21/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import "MISettingsViewController.h"
#import "MIEditProfileViewController.h"
#import "MIChangePasswordViewController.h"
#import "MILoginViewController.h"
#import "MIAboutUsViewController.h"
#import "MISettingHeaderView.h"

#import "MISettingsTableViewCell.h"
#import "MISettingSearchTableViewCell.h"
#import "MISettingProfilePrivacyTableViewCell.h"


@interface MISettingsViewController ()
{
    NSMutableArray *arrSettingsData;
}
@end

@implementation MISettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

-(void)initialize
{
    self.title = CLocalize(@"Settings");
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    
    [btnLogout.layer setCornerRadius:19.0];
    [btnLogout.layer setMasksToBounds:YES];
    [btnLogout setTitle:CLocalize(@"Logout") forState:UIControlStateNormal];
    
    
    arrSettingsData =
    @[@{CLocalize(@"Profile").uppercaseString:
            @[@{@"title": CLocalize(@"Edit Profile") ,@"image": @"edit_profile"},
              @{@"title": CLocalize(@"Change Password") ,@"image": @"change_password_blue"}]},
      
      @{CLocalize(@"Search Visibility").uppercaseString:
            @[@{CLocalize(@"Friends") : @1,CLocalize(@"Public"): @0,CLocalize(@"Only_me"):@0}]},
      
      @{CLocalize(@"Profile Privacy").uppercaseString:
            @[@{CLocalize(@"Email Privacy") :@{CLocalize(@"Friends"): @1,CLocalize(@"Public"
                  ): @0,CLocalize(@"Only_me"):@0}},
              @{CLocalize(@"Phone Number")  :@{CLocalize(@"Friends"): @1,CLocalize(@"Public"): @0,CLocalize(@"Only_me"):@0}},
              @{CLocalize(@"Date of Birth") :@{CLocalize(@"Friends"): @1,CLocalize(@"Public"): @0,CLocalize(@"Only_me"):@0}}]},
      
      @{CLocalize(@"Notifications").uppercaseString:
            @[@{@"title": CLocalize(@"App Notifications"),@"image": @"notification_blue"}]},
      
      @{CLocalize(@"Select Language").uppercaseString:
            @[@{@"title": @"English",@"image": @"english",@"isSelect":@1},
              @{@"title": @"Français",@"image": @"france",@"isSelect":@0}]},
      
      @{CLocalize(@"About us").uppercaseString:
            @[@{@"title": CLocalize(@"About us"),@"image": @"about_us"},
              @{@"title": CLocalize(@"Contact Us"),@"image": @"contact_us"},
              @{@"title": CLocalize(@"Terms and Conditions"),@"image": @"terms_condition"},
              @{@"title": CLocalize(@"Privacy Policy"),@"image": @"privacy_policy"}]}].mutableCopy;
    
    
    [tblSetings registerNib:[UINib nibWithNibName:@"MISettingsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISettingsTableViewCell"];
    [tblSetings registerNib:[UINib nibWithNibName:@"MISettingSearchTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISettingSearchTableViewCell"];
    [tblSetings registerNib:[UINib nibWithNibName:@"MISettingProfilePrivacyTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISettingProfilePrivacyTableViewCell"];
    [tblSetings registerNib:[UINib nibWithNibName:@"MISettingHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"MISettingHeaderView"];
}




# pragma mark
# pragma mark - UITableView DataSource and Delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrSettingsData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSArray *)[arrSettingsData[section] valueForKey:[[arrSettingsData[section] allKeys] firstObject]]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrSetting = [[arrSettingsData objectAtIndex:indexPath.section] valueForKey:[[[arrSettingsData objectAtIndex:indexPath.section] allKeys] firstObject]];
    
    
    if (indexPath.section == 1)
    {
        static NSString *cellIdetifier = @"MISettingSearchTableViewCell";
        MISettingSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        
        [cell.objSearchVisibility setHidden:NO];
        NSDictionary *dicSearchVisibility = [arrSetting objectAtIndex:indexPath.row];
        
        cell.btnSearchFriend.selected = NO;
        cell.btnSearchPublic.selected = NO;
        cell.btnSearchOnlyMe.selected = NO;
        
        if ([[dicSearchVisibility valueForKey:@"Friends"] boolValue])
            cell.btnSearchFriend.selected = YES;
        
        if ([[dicSearchVisibility valueForKey:@"Public"] boolValue])
            cell.btnSearchPublic.selected = YES;
        
        if ([[dicSearchVisibility valueForKey:@"Only_me"] boolValue])
            cell.btnSearchOnlyMe.selected = YES;
        
        
        [cell.btnSearchFriend addTarget:self action:@selector(btnSearchVisibilityIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSearchPublic addTarget:self action:@selector(btnSearchVisibilityIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSearchOnlyMe addTarget:self action:@selector(btnSearchVisibilityIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 2)
    {
        static NSString *cellIdetifier = @"MISettingProfilePrivacyTableViewCell";
        MISettingProfilePrivacyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        
        [cell.btnFriend addTarget:self action:@selector(btnProfilePrivacyIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPublic addTarget:self action:@selector(btnProfilePrivacyIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnOnlyMe addTarget:self action:@selector(btnProfilePrivacyIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        NSDictionary *dicProfileSetting = [[arrSetting objectAtIndex:indexPath.row] valueForKey:[[[arrSetting objectAtIndex:indexPath.row] allKeys] firstObject]];
        
        
        [cell.lblPrivacyTitle setText:[[[arrSetting objectAtIndex:indexPath.row] allKeys] firstObject]];
        
        
        cell.btnFriend.selected = NO;
        cell.btnPublic.selected = NO;
        cell.btnOnlyMe.selected = NO;
        
        
        if ([[dicProfileSetting valueForKey:@"Friends"] boolValue])
            cell.btnFriend.selected = YES;
        
        if ([[dicProfileSetting valueForKey:@"Public"] boolValue])
            cell.btnPublic.selected = YES;
        
        if ([[dicProfileSetting valueForKey:@"Only_me"] boolValue])
            cell.btnOnlyMe.selected = YES;
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        static NSString *cellIdetifier = @"MISettingsTableViewCell";
        MISettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
        
        [cell.objProfileView setHidden:YES];
        [cell.imgArrow hideByWidth:NO];
        [cell.imgVLang setHidden:YES];
        [cell.objSepraterView hideByHeight:YES];
        
        switch (indexPath.section)
        {
            case 0:     // Profile
            {
                if(indexPath.row == 1)
                    [cell.objSepraterView hideByHeight:NO];
                
                [cell.lblTitle setText:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"title"]];
                [cell.objProfileView setHidden:NO];
                [cell.imgLangSelect hideByWidth:YES];
                [cell.objNotificationSwitch hideByWidth:YES];
                [cell.imgIcon setImage:[UIImage imageNamed:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"image"]]];
            }
                break;
            case 3: //Notifications
            {
                [cell.objProfileView setHidden:NO];
                [cell.lblTitle setText:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"title"]];
                [cell.imgLangSelect hideByWidth:YES];
                [cell.objNotificationSwitch hideByWidth:NO];
                [cell.imgArrow hideByWidth:YES];
                [cell.imgIcon setImage:[UIImage imageNamed:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"image"]]];
                [cell.objNotificationSwitch hideByWidth:NO];
            }
                break;
            case 4: //Select Language
            {
                [cell.objProfileView setHidden:NO];
                [cell.imgVLang setHidden:NO];
                
                if(indexPath.row == 1)
                    [cell.objSepraterView hideByHeight:NO];
                
                [cell.lblTitle setText:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"title"]];
                [cell.objNotificationSwitch hideByWidth:YES];
                [cell.imgArrow hideByWidth:YES];
                [cell.imgLangSelect hideByWidth:NO];
                [cell.imgVLang setImage:[UIImage imageNamed:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"image"]]];
                
                [cell.btnLangSelect setTag:indexPath.row+1];
                
                
                if ([[[arrSetting objectAtIndex:indexPath.row]valueForKey:@"isSelect"]boolValue])
                    [cell.imgLangSelect setImage:[UIImage imageNamed:@"select"]];
                else
                    [cell.imgLangSelect setImage:[UIImage imageNamed:@"unselect"]];
                
            }
                break;
            case 5: //About Us
            {
                
                if(indexPath.row != 0)
                    [cell.objSepraterView hideByHeight:NO];
                
                
                [cell.objProfileView setHidden:NO];
                [cell.lblTitle setText:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"title"]];
                [cell.imgLangSelect hideByWidth:YES];
                [cell.objNotificationSwitch hideByWidth:YES];
                [cell.imgIcon setImage:[UIImage imageNamed:[[arrSetting objectAtIndex:indexPath.row] valueForKey:@"image"]]];
                
            }
                break;
            default:
                break;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 51;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *HeaderIdentifier = @"MISettingHeaderView";
    MISettingHeaderView *header = [tblSetings dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
//    header.tintColor = CRGB(240, 240, 240);
//    header.backgroundView.backgroundColor = CRGB(240, 240, 240);
    [header.lblHeaderTitle setText:[[[arrSettingsData objectAtIndex:section] allKeys] firstObject]];
    return header;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0:
        {
            if(indexPath.row == 0)
            {
                MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
                editProfileVC.editProfileType = OtherScreen;
                [self.navigationController pushViewController:editProfileVC animated:YES];
            }
            else
            {
                MIChangePasswordViewController *changePWVC = [[MIChangePasswordViewController alloc] initWithNibName:@"MIChangePasswordViewController" bundle:nil];
                [self.navigationController pushViewController:changePWVC animated:YES];
            }
            
            break;
        }
        case 4:
        {
            NSMutableArray *arrSetitngLang = [[[arrSettingsData objectAtIndex:indexPath.section] valueForKey:[[[arrSettingsData objectAtIndex:indexPath.section] allKeys] firstObject]] mutableCopy];
            
            NSMutableDictionary *dicLang = [[arrSetitngLang objectAtIndex:indexPath.row] mutableCopy];
            
            if ([[dicLang valueForKey:@"isSelect"]boolValue])
            {
                [dicLang removeObjectForKey:@"isSelect"];
                [dicLang setValue:@1 forKey:@"isSelect"];
            }
            else
            {
                [dicLang removeObjectForKey:@"isSelect"];
                [dicLang setValue:@1 forKey:@"isSelect"];
            }
            
            if ([[dicLang valueForKey:@"title"] isEqualToString:@"English"])
                [arrSetitngLang replaceObjectAtIndex:0 withObject:dicLang];
            else
                [arrSetitngLang replaceObjectAtIndex:1 withObject:dicLang];
            
            
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title != %@",[dicLang valueForKey:@"title"]];
            NSMutableDictionary *dicfilter  = [[[arrSetitngLang filteredArrayUsingPredicate:predicate] objectAtIndex:0] mutableCopy];
            
            [arrSetitngLang removeObject:[[[arrSetitngLang filteredArrayUsingPredicate:predicate] mutableCopy] objectAtIndex:0]];
            
            if ([[dicfilter valueForKey:@"isSelect"]boolValue])
            {
                [dicfilter removeObjectForKey:@"isSelect"];
                [dicfilter setValue:@0 forKey:@"isSelect"];
            }
            else
            {
                [dicfilter removeObjectForKey:@"isSelect"];
                [dicfilter setValue:@0 forKey:@"isSelect"];
            }
            
            if ([[dicfilter valueForKey:@"title"] isEqualToString:@"English"])
                [arrSetitngLang insertObject:dicfilter atIndex:0];
            else
                [arrSetitngLang insertObject:dicfilter atIndex:1];
            
            
            NSMutableDictionary *dicData = [[NSMutableDictionary alloc]init];
            [dicData setObject:arrSetitngLang forKey:[[[arrSettingsData objectAtIndex:indexPath.section] allKeys] firstObject]];
            [arrSettingsData replaceObjectAtIndex:indexPath.section withObject:dicData];
            [tblSetings reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
            
            break;
        }
        case 5:
        {
            NSString *title;
            
            if(indexPath.row == 0)
                title = @"About Us";
            else if(indexPath.row == 1)
                title = @"Contact Us";
            else if(indexPath.row == 2)
                title = @"Terms and Conditions";
            else
                title = @"Privacy Policy";
            
            
            MIAboutUsViewController *aboutusVC = [[MIAboutUsViewController alloc] initWithNibName:@"MIAboutUsViewController" bundle:nil];
            aboutusVC.strTitle = title;
            [self.navigationController pushViewController:aboutusVC animated:YES];
        }
    }
    
    
}




#pragma mark
#pragma mark - Button Action

- (void)btnMenuClicked
{
    [appDelegate resignKeyboard];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

-(IBAction)btnSearchVisibilityIsClicked:(UIButton *)sender
{
    CGPoint localPoint = [tblSetings convertPoint:CGPointZero fromView:sender];
    NSIndexPath* indexPath = [tblSetings indexPathForRowAtPoint:localPoint];
    
    NSMutableArray *arrSearchVisibility = [[[arrSettingsData objectAtIndex:indexPath.section] valueForKey:[[[arrSettingsData objectAtIndex:indexPath.section] allKeys] firstObject]] mutableCopy];
    
    NSMutableDictionary *dicSearchVisibility = [[arrSearchVisibility objectAtIndex:indexPath.row] mutableCopy];
    
    [dicSearchVisibility removeObjectForKey:@"Friends"];
    [dicSearchVisibility removeObjectForKey:@"Public"];
    [dicSearchVisibility removeObjectForKey:@"Only_me"];
    
    switch (sender.tag)
    {
        case 101:
        {
            [dicSearchVisibility setValue:@1 forKey:@"Friends"];
            [dicSearchVisibility setValue:@0 forKey:@"Public"];
            [dicSearchVisibility setValue:@0 forKey:@"Only_me"];
        }
            break;
        case 102:
        {
            [dicSearchVisibility setValue:@0 forKey:@"Friends"];
            [dicSearchVisibility setValue:@1 forKey:@"Public"];
            [dicSearchVisibility setValue:@0 forKey:@"Only_me"];
            
        }
            break;
        case 103:
        {
            [dicSearchVisibility setValue:@0 forKey:@"Friends"];
            [dicSearchVisibility setValue:@0 forKey:@"Public"];
            [dicSearchVisibility setValue:@1 forKey:@"Only_me"];
        }
            break;
        default:
            break;
    }
    
    [arrSearchVisibility removeAllObjects];
    
    [arrSearchVisibility addObject:dicSearchVisibility];
    
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc]init];
    [dicData setObject:arrSearchVisibility forKey:[[[arrSettingsData objectAtIndex:indexPath.section] allKeys] firstObject]];
    [arrSettingsData replaceObjectAtIndex:indexPath.section withObject:dicData];
    
    [tblSetings reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    
    //[tblSetings reloadData];
}

-(IBAction)btnProfilePrivacyIsClicked:(UIButton *)sender
{
    CGPoint localPoint = [tblSetings convertPoint:CGPointZero fromView:sender];
    NSIndexPath* indexPath = [tblSetings indexPathForRowAtPoint:localPoint];
    
    NSMutableArray *arrSearchVisibility = [[[arrSettingsData objectAtIndex:indexPath.section] valueForKey:[[[arrSettingsData objectAtIndex:indexPath.section] allKeys] firstObject]] mutableCopy];
    
    NSMutableDictionary *dicSearchVisibility = [[[arrSearchVisibility objectAtIndex:indexPath.row] valueForKey:[[[arrSearchVisibility objectAtIndex:indexPath.row] allKeys] firstObject]] mutableCopy];
    
    [dicSearchVisibility removeObjectForKey:@"Friends"];
    [dicSearchVisibility removeObjectForKey:@"Public"];
    [dicSearchVisibility removeObjectForKey:@"Only_me"];
    
    switch (sender.tag)
    {
        case 101:
        {
            [dicSearchVisibility setValue:@1 forKey:@"Friends"];
            [dicSearchVisibility setValue:@0 forKey:@"Public"];
            [dicSearchVisibility setValue:@0 forKey:@"Only_me"];
        }
            break;
        case 102:
        {
            [dicSearchVisibility setValue:@0 forKey:@"Friends"];
            [dicSearchVisibility setValue:@1 forKey:@"Public"];
            [dicSearchVisibility setValue:@0 forKey:@"Only_me"];
            
        }
            break;
        case 103:
        {
            [dicSearchVisibility setValue:@0 forKey:@"Friends"];
            [dicSearchVisibility setValue:@0 forKey:@"Public"];
            [dicSearchVisibility setValue:@1 forKey:@"Only_me"];
        }
            break;
        default:
            break;
    }
    
    //    [arrSearchVisibility removeAllObjects];
    
    NSMutableDictionary *dicUpdatePrivcay = [[NSMutableDictionary alloc]init];
    
    [dicUpdatePrivcay setObject:dicSearchVisibility forKey:[[[arrSearchVisibility objectAtIndex:indexPath.row] allKeys] firstObject]];
    
    [arrSearchVisibility removeObjectAtIndex:indexPath.row];
    
    //    if ([[[[arrSearchVisibility objectAtIndex:indexPath.row] allKeys] firstObject] isEqualToString:@"Email Privacy"])
    //        [arrSearchVisibility insertObject:dicUpdatePrivcay atIndex:0];
    //    else if ([[[[arrSearchVisibility objectAtIndex:indexPath.row] allKeys] firstObject] isEqualToString:@"Phone Number"])
    //        [arrSearchVisibility insertObject:dicUpdatePrivcay atIndex:1];
    //    else
    //        [arrSearchVisibility insertObject:dicUpdatePrivcay atIndex:2];
    
    
    [arrSearchVisibility insertObject:dicUpdatePrivcay atIndex:indexPath.row];
    
    //[arrSearchVisibility addObject:dicUpdatePrivcay];
    
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc]init];
    
    
    
    [dicData setObject:arrSearchVisibility forKey:[[[arrSettingsData objectAtIndex:indexPath.section] allKeys] firstObject]];
    [arrSettingsData replaceObjectAtIndex:indexPath.section withObject:dicData];
    
    [tblSetings reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (IBAction)btnLogoutIsClicked:(id)sender
{
    MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
    [self.navigationController pushViewController:loginVC animated:YES];
}

@end
