//
//  MIRequestDetailViewController.m
//  Labari
//
//  Created by mac-0006 on 27/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIRequestDetailViewController.h"
#import "MICommentsViewController.h"

#import "MICommentsTableViewCell.h"


@interface MIRequestDetailViewController ()

@end

@implementation MIRequestDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - General Method
#pragma mark -

- (void)initialize
{
    self.title = @"Apartment";
    
    [self setShadowView:vCreatorInfo];
    [self setShadowView:vDetail];
    [self setShadowView:vAddress];
    [self setShadowView:vComment];
    [self setShadowView:scrollV];
    
    txtComment.placeholder = CLocalize(@"Add Comment"); 
    [txtComment setPlaceholderColor:CRGB(213, 213, 213)];
    txtComment.font = CFontAvenirLTStd55Roman(13);
    txtComment.textColor =  ColorDarkBule_0e293c;
    txtComment.animateHeightChange = YES;
    txtComment.delegate = self;
    [txtComment setContentInset:UIEdgeInsetsMake(0, 5, 0, 5)];
    txtComment.tintColor = ColorDarkBule_0e293c;
    txtComment.backgroundColor = CRGB(242, 242, 242);
    txtComment.layer.cornerRadius  = cnTxtViewHeight.constant/2;
    txtComment.layer.masksToBounds = YES;
    
    
    [lblAddressTitle setText:CLocalize(@"Address")];
    [lblComment setText:CLocalize(@"Comments")];
    [btnViewAll setTitle:CLocalize(@"View All Comments") forState:UIControlStateNormal];
    [btnSend setTitle:CLocalize(@"Send") forState:UIControlStateNormal];
    
    
    lblDetail.attributedText = [appDelegate setAttributedString:@"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.The Point of using lorem ispum is that it has a more -or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English." andAlign:NSTextAlignmentLeft];
    
    
    [tblComment registerNib:[UINib nibWithNibName:@"MICommentsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MICommentsTableViewCell"];
    [tblComment setEstimatedRowHeight:105];
    [tblComment setRowHeight:UITableViewAutomaticDimension];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    cnTblHeight.constant = tblComment.contentSize.height;
}

- (void)setShadowView:(UIView *)view
{
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeZero;
    view.layer.shadowOpacity = 0.2f;
    view.layer.shadowRadius = 2.0f;
    view.layer.masksToBounds = NO;
}


#pragma mark - Growing textview delegate methods
#pragma mark -

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    cnTxtViewHeight.constant = height > 30 ? height : 30;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText = [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    //    btnSend.enabled = (finalText.length > 0);
    //
    //    if (!btnSend.enabled)
    //        cnTxtHeight.constant = 30;
    
    if(finalText.length >= CCommentLength)
        return NO;
    
    return true;
}


#pragma mark - ScrollView Method
#pragma mark -

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [txtComment resignFirstResponder];
}



# pragma mark - UITableView DataSource & Delegate
# pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdetifier = @"MICommentsTableViewCell";
    MICommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row == 0)
        cell.btnReply.hidden = NO;
    else
        cell.btnReply.hidden = YES;
    
    [cell.viewSeprater hideByHeight:YES];
    
    return cell;
}



# pragma mark - Action Events
# pragma mark -

- (IBAction)btnViewAllCommentsClicked:(id)sender
{
    MICommentsViewController *commentVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
    [self.navigationController pushViewController:commentVC animated:YES];
}

- (IBAction)btnCommentSendClicked:(id)sender
{
    [appDelegate resignKeyboard];
    
    if([txtComment.text length] == 0)
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CAddComment withTitle:@""];
}

@end
