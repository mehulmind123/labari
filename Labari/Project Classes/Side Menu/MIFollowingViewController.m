//
//  MIFollowingViewController.m
//  DemoStreamingURL
//
//  Created by mac-0003 on 20/03/17.
//  Copyright © 2017 mac-0003Mi. All rights reserved.
//

#import "MIFollowingViewController.h"
#import "MIProfileViewController.h"

@interface MIFollowingViewController ()
{
    NSMutableArray *arrFollowerList;
}
@end

@implementation MIFollowingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}




#pragma mark
#pragma mark - Set Initialize method

-(void)initialize
{
    
    if (_followType == Followers)
        self.title = CLocalize(@"Followers");
    else
        self.title = CLocalize(@"Followings");
    
    [tblFollowers registerNib:[UINib nibWithNibName:@"MIFolloweringTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIFolloweringTableViewCell"];
    
    arrFollowerList = [[NSMutableArray alloc] initWithObjects:
                       @{@"image":@"temp_user", @"user_name": @"Alexa Garison"},
                       @{@"image":@"temp_user", @"user_name": @"Lily Rechards"},
                       @{@"image":@"temp_user", @"user_name": @"Danish Wagh"},
                       @{@"image":@"temp_user", @"user_name": @"Haily Peterson Wagh"},
                       @{@"image":@"temp_user", @"user_name": @"Alexa Garison"},
                       @{@"image":@"temp_user", @"user_name": @"Amelia Nelson"},
                       @{@"image":@"temp_user", @"user_name": @"Irea Fitzgearald"},
                       @{@"image":@"temp_user", @"user_name": @"Mia Blare"},nil];
}




#pragma mark
#pragma mark - UITableView DataSource and Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrFollowerList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdetifier = @"MIFolloweringTableViewCell";
    MIFolloweringTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dict = [arrFollowerList objectAtIndex:indexPath.row];
    
    cell.imgUser.image = [UIImage imageNamed:[dict valueForKey:@"image"]];
    cell.lblUserName.text = [dict valueForKey:@"user_name"];
    
    
    if (_followType == Followers)
        [cell.btnUnfollow hideByWidth:YES];
    else
        [cell.btnUnfollow hideByWidth:NO];
    
    
    [cell.btnUnfollow touchUpInsideClicked:^{
    }];
    
    [cell.btnProfile touchUpInsideClicked:^{
        
        MIProfileViewController *profileVC = [[MIProfileViewController alloc]initWithNibName:@"MIProfileViewController" bundle:nil];
        profileVC.profileType = OtherUserProfile;
        [self.navigationController pushViewController:profileVC animated:YES];
    }];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


@end
