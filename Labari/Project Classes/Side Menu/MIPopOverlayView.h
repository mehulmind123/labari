//
//  MIPopOverlayView.h
//  Labari
//
//  Created by mac-0006 on 21/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    PresentTypeBottom,
    PresentTypeCenter
    
} PresentType;

@interface MIPopOverlayView : UIView

@property (nonatomic, assign) PresentType presentType;

//@property (assign, nonatomic) BOOL shouldCloseOnClickOutside;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;

+(id)popUpOverlay;

@end
