//
//  PlayerButton.h
//  Mospur
//
//  Created by mac-0009 on 17/03/17.
//  Copyright © 2017 mac-0009. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerButton : UIButton

@property (nonatomic, assign) CGFloat progress;

@end
