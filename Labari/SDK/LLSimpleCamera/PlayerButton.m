//
//  TDVPlayerButton.m
//  Newsplay
//
//  Created by Chun Ye on 12/8/13.
//  Copyright (c) 2013 Tribune Digital Ventures. All rights reserved.
//

#import "PlayerButton.h"

#define DEGREES_TO_RADIANS(x) (x)/180.0*M_PI

@interface PlayerButton ()
{
    CGRect bounds;
}

@property (nonatomic, strong) CAShapeLayer *shapeLayerWhite;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@end

@implementation PlayerButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.progress = 0.0f;
    
    // Get Bounds According to Device
    bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width * CScreenWidth / 375, self.bounds.size.width * CScreenWidth / 375);
    
    // Add White ShapeLayer
    self.shapeLayerWhite = [[CAShapeLayer alloc] init];
    self.shapeLayerWhite.frame = bounds;//self.bounds;
    self.shapeLayerWhite.fillColor = nil;
    self.shapeLayerWhite.strokeColor = CRGBA(255, 255, 255, .2).CGColor;
    
    if (!self.shapeLayerWhite.superlayer) {
        [self.layer addSublayer:self.shapeLayerWhite];
    }
    
    self.shapeLayerWhite.lineWidth = 4;
    self.shapeLayerWhite.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(bounds), (bounds.size.height - 1) / 2)
                                                               radius:bounds.size.width/2 - 4.0f
                                                           startAngle:3*M_PI_2
                                                             endAngle:3*M_PI_2 + 2*M_PI
                                                            clockwise:YES].CGPath;
    
    self.shapeLayerWhite.strokeEnd = 1;
    [self.layer addSublayer:self.shapeLayerWhite];
    
    // Add To be filled
    self.shapeLayer = [[CAShapeLayer alloc] init];
    self.shapeLayer.frame = self.bounds;
    self.shapeLayer.fillColor = nil;
    self.shapeLayer.strokeColor = CRGB(22, 205, 254).CGColor;
    
    if (!self.shapeLayer.superlayer) {
        [self.layer addSublayer:self.shapeLayer];
    }
}

- (void)setProgress:(CGFloat)progress1
{
    _progress = progress1;
    [self updateProgress];
}

- (void)updateProgress
{
    if (self.progress > 0) {
        if (!self.shapeLayer.superlayer) {
            [self.layer addSublayer:self.shapeLayer];
        }
        BOOL startingFromIndeterminateState = [self.shapeLayer animationForKey:@"indeterminateAnimation"] != nil;
        [self stopIndeterminateAnimation];
        
        self.shapeLayer.lineWidth = 4;
        self.shapeLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.bounds), (self.bounds.size.height - 1) / 2)
                                                              radius:self.bounds.size.width/2 - 4.0f
                                                          startAngle:3*M_PI_2
                                                            endAngle:3*M_PI_2 + 2*M_PI
                                                           clockwise:YES].CGPath;
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        animation.fromValue = (startingFromIndeterminateState) ? @0 : nil;
        animation.toValue = [NSNumber numberWithFloat:self.progress/60];
        animation.duration = 0.2;
        self.shapeLayer.strokeEnd = (self.progress / 60);
        [self.shapeLayer addAnimation:animation forKey:@"animation"];
    } else {
        [self.shapeLayer removeAnimationForKey:@"animation"];
        if (self.shapeLayer.superlayer) {
            [self.shapeLayer removeFromSuperlayer];
        }
    }
}

- (void)startIndeterminateAnimation
{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    self.shapeLayer.lineWidth = 4;
    self.shapeLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.bounds), (self.bounds.size.height - 1) / 2)
                                                          radius:self.bounds.size.width/2 - 4.0f
                                                      startAngle:DEGREES_TO_RADIANS(348)
                                                        endAngle:DEGREES_TO_RADIANS(12)
                                                       clockwise:NO].CGPath;
    self.shapeLayer.strokeEnd = 1;
    
    [CATransaction commit];
    
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    rotationAnimation.toValue = [NSNumber numberWithFloat:2*M_PI];
    rotationAnimation.duration = 2.0;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [self.shapeLayer addAnimation:rotationAnimation forKey:@"indeterminateAnimation"];
}

- (void)stopIndeterminateAnimation
{
    [self.shapeLayer removeAnimationForKey:@"indeterminateAnimation"];
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    [CATransaction commit];
}

@end
