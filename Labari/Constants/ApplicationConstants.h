//
//  ApplicationConstants.h
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#ifndef ApplicationConstants_h
#define ApplicationConstants_h

//#define NSLog(...)

/*======== WHITE SPACE CHAR SET =========*/
#define CWhitespaceCharSet [NSCharacterSet whitespaceAndNewlineCharacterSet]


/*======== FONT =========*/
#define CFontHelveticaRegular(fontSize) [UIFont fontWithName:@"HelveticaNeue" size:fontSize]
#define CFontHelveticaBold(fontSize)    [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize]
#define CFontHelveticaMedium(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize]
#define CFontHelveticaLight(fontSize)   [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize]
#define CFontHelveticaItalic(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Italic" size:fontSize]

#define CFontAvenirLTStd55Roman(fontSize)    [UIFont fontWithName:@"AvenirLTStd-Roman" size:fontSize]
#define CFontAvenirLTStd65Meduim(fontSize)     [UIFont fontWithName:@"AvenirLTStd-Medium" size:fontSize]
#define CFontAvenirLTStd85Heavy(fontSize)       [UIFont fontWithName:@"AvenirLTStd-Heavy" size:fontSize]
#define CFontAvenirLTStd95Balck(fontSize)   [UIFont fontWithName:@"AvenirLTStd-Black" size:fontSize]


/*======== COLOR =========*/

#define ColorLightGray                  CRGB(225, 225, 225)
#define ColorLightWhite                 CRGB(106, 106, 106)
#define ColorYellow_fdb200              CRGB(253, 178, 0)

#define ColorGreen_99cb48               CRGB(153, 203, 72)
#define ColorRed_9FF454D                CRGB(255, 69, 77)
#define ColorSkyEnable_00B5E7           CRGB(0, 181, 231)
#define ColorSkyDisable_00B5E7          CRGB(173, 228, 242)
#define ColorSkyCommon                  CRGB(25, 178, 235)

#define ColorBlack_383838               CRGB(38, 38, 38)
#define ColorBlack_222222               CRGB(34, 34, 34)
#define ColorBlack_7b7b7b               CRGB(149, 149, 149)

#define ColorOrange                     CRGB(244, 164, 9)
#define ColorWhite_EEEEEE               CRGB(238, 238, 238)



#define ColorWhite_FFFFFF               CRGB(255, 255, 255)

#define ColorAquamarina_00e1c6          CRGB(87, 195, 182)
#define ColorTurquoise_19bbd5           CRGB(27, 187, 213)
#define ColorSkyBule_2c9dde             CRGB(52, 156, 215)
#define ColorDarkBule_0e293c            CRGB(14, 41, 16)
#define ColorGray_C6DAEC                CRGB(198, 218, 235)





/*======== USER DEFAULT =========*/

#define UserDefaultLoginToken                   @"LoginToken"
#define UserDefaultDeviceToken                  @"DeviceToken"
#define UserDefaultiFiltered                    @"iFiltered"

#define TimestampGoLivePostNew                  @"TimestampGoLivePostNew"
#define TimestampGoLivePostOld                  @"TimestampGoLivePostOld"

#define TimestampSmartPostNew                   @"TimestampSmartPostNew"
#define TimestampSmartPostOld                   @"TimestampSmartPostOld"
#define TimestampCommentOld                     @"TimestampCommentOld"
#define TimestampNotificationList               @"TimestampNotificationList"
#define TimestampExamList                       @"TimestampExamList"





/*======== NOTIFICATION CONSTANTS =========*/

#define NotificationImageUploaded               @"NotificationImageUploaded"
#define NotificationArrive                      @"NotificationArrive"





/*======== OTHER =========*/
#define CCommentLength  160



/*======== LANGUAGE =========*/

#define CUserDefaultLanguage    @"UserSelectedLanguage"
#define CUserDefaultRegion      @"UserSelectedRegion"
#define CLanguageEnglish        @"en"
#define CLanguageFrench         @"fr"


#define CLocalize(text) [[LocalizationSystem sharedLocalSystem] localizedStringForKey:text value:text]

#define CSetUserLanguage(language) [[LocalizationSystem sharedLocalSystem] setLanguage:language]
//#define CGetUserLanguage [[LocalizationSystem sharedLocalSystem] getLanguage]
#define CGetUserLanguage [CUserDefaults valueForKey:CUserDefaultLanguage]



#endif /* ApplicationConstants_h */
