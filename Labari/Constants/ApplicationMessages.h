//
//  ApplicationMessages.h
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#ifndef ApplicationMessages_h
#define ApplicationMessages_h


/*======== Loader Messages =========*/
#define CMessageCongratulation              @"Congratulations!"
#define CMessageSorry                       @"Sorry!"
#define CMessageLoading                     @"Loading..."
#define CMessageSearching                   @"Searching..."
#define CMessageVerifying                   @"Verifying..."
#define CMessageWait                        @"Please Wait..."
#define CMessageUpdating                    @"Updating..."
#define CMessageAuthenticating              @"Authenticating..."
#define CMessageErrorInternetNotAvailable   @"Intenet Connection Not Available!\n Please Try Again Later."



/*======== Validation Messages =========*/

#define CPasswordLength     6


//...Login Or Change Password
#define CMessageEmailOrMobile           @"Email or Mobile number field can not be blank."
#define CMessagePassword                @"Password field can not be blank."
#define CMessageNewPassword             @"New password field can not be blank."
#define CMessageOldPassword             @"Old password field can not be blank."
#define CMessageConfirmPassword         @"Confirm password field can not be blank."
#define CMessageValidPassword           @"Password must be minimum 6 characters alphanumeric."
#define CMessageDoesNotMatchPassword    @"Password and Confirm password combination does not match."

//..Register
#define CMessageFirstName           @"First name can not be blank."
#define CMessageLastName            @"Last name can not be blank."
#define CMessageEmail               @"Email can not be blank."
#define CMessageValidEmail          @"Please enter valid email address."
#define CMessageMobileNumber        @"Mobile number field can not be blank."
#define CMessageValidMobileNumber   @"Please enter valid mobile number."
#define CMessageInterest            @"Please select atleast one Interest."
#define CMessageAcceptTerms         @"Please accept terms and conditions."


//....Verification
#define CMessageVerificationCode    @"Verification code can not be blank."


//Select Interest
#define CSelectMinInterest          @"Please select atleast 1 interest."
#define CSelectMaxInterest          @"You can select maximum 3 interest."



#define CAddComment                 @"Please add comment."
#define CAddMessage                 @"Please type any message."
#define CAddFriendList              @"Person is not in your friends list, You want to add person as a friend?"
#define CRemoveFromFrdList          @"Are you sure you want to remove this friend?"
#define CNotPostedRequest           @"You have not posted any request yet. Do you want to post a request?"


#endif /* ApplicationMessages_h */
