//
//  APIRequest.h
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//





/*===========================================================
 ===========================================================*/

//********** Server

#define BASEURL     @""





//********** Request, Response Constant

#define CVersion    @"1"
#define COffset     @0
#define CLimit      @20

#define CJsonStatus             @"status"
#define CJsonMessage            @"message"
#define CJsonTitle              @"title"
#define CJsonData               @"data"
#define CJsonResponse           @"response"
#define CJsonNewTimestamp       @"new-timestamp"

#define CStatusZero             @0
#define CStatusOne              @1
#define CStatusFive             @5
#define CStatusNine             @9
#define CStatusTen              @10




//********** API Tag

#define CAPITagLogin                        @"login"
#define CAPITagForgotPassword               @"forgot_password"
#define CAPITagResendOTP                    @"resend_otp"
#define CAPITagResetPassword                @"reset_password"
#define CAPITagForgotSmartCode              @"forgot_smartcode"
#define CAPITagUserProfile                  @"user_profile"
#define CAPITagUpdateProfile                @"update_user_profile"
#define CAPITagChangePassword               @"change_password"

#define CAPITagGoLivePost                   @"go_live"
#define CAPITagSmartPage                    @"smart_page"
#define CAPITagSmartPagePost                @"post_list"
#define CAPITagPostDetail                   @"post_detail"
#define CAPITagLikePost                     @"like_post"
#define CAPITagFollowingInstitute           @"following_institutes"
#define CAPITagFollowInstitute              @"follow_institute"
#define CAPITagComments                     @"comments"
#define CAPITagPostComment                  @"post_comment"
#define CAPITagUnfollowInstitute            @"follow_institute"

#define CAPITagInstituteList                @"institute_list"
#define CAPITagFilterByInstitute            @"filterby_institute"
#define CAPITagFilterByCourse               @"filterby_course"
#define CAPITagFilterByCountry              @"filterby_country"
#define CAPITagFilterByState                @"filterby_state"
#define CAPITagFilterByCity                 @"filterby_city"
#define CAPITagFilterByAll                  @"filterby_allselected"
#define CAPITagSearchInstitute              @"search_institute"

#define CAPITagHolidayList                  @"holiday_list"
#define CAPITagHolidayDetail                @"holiday_detail"

#define CAPITagExamList                     @"exam_list"
#define CAPITagExamDetail                   @"exam_detail"
#define CAPITagClassPerformance             @"class_performance"
#define CAPITagMyPerformance                @"my_performance"

#define CAPITagHostelDetail                 @"hostel_detail"

#define CAPITagNotificationList             @"notification_list"
#define CAPITagDeleteNotification           @"delete_notification"
#define CAPITagNotificationUnreadCount      @"notification_unread_count"

#define CAPITagCalendar                     @"calendar"
#define CAPITagAttendance                   @"attendance"
#define CAPITagTimetable                    @"time_table"
#define CAPITagFoodmenu                     @"food_menu"


#define CAPITagDeviceToken                  @"device_token"


/*===========================================================
 ===========================================================*/



#import <Foundation/Foundation.h>

@interface APIRequest : NSObject
+ (id)request;



#pragma mark
#pragma mark - LRF


@end
