//
//  NSDate+MIDate.h
//  EdSmart
//
//  Created by mac-0005 on 7/16/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (MIDate)

+ (NSInteger)getCurrentHour;

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format;
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format;
+ (NSString *)convertStringFromDate:(NSString *)string withFormat:(NSString *)format;

- (NSString *)timeAgo;
- (NSString *)displayTimeofPost:(NSString *)fromDate;

+ (NSString *)setTime:(NSString *)createdDate;
+ (NSString *)stringFromDateForGMT:(NSDate *)date withFormat:(NSString *)format;
+ (NSDate *)dateFromStringGMT:(NSString *)datestring withFormat:(NSString *)format;

+ (NSDate *)firstDayOfMonth:(NSDate *)date;
+ (NSDate *)lastDayOfMonth:(NSDate *)date;
@end
