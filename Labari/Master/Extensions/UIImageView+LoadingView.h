//
//  UIImageView+LoadingView.h
//  Patch
//
//  Created by mac-00015 on 12/4/15.
//  Copyright © 2015 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (LoadingView)

+(id)loadingView;

@end
