//
//  UIViewController+Helper.m
//  MI API Example
//
//  Created by mac-0001 on 13/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UIViewController+Helper.h"
#import "NSObject+NewProperty.h"

static NSString *const BACKACTIIONHANDLER = @"backActionHandler";
static NSString *const MENUACTIONHANDLER = @"menuActionHandler";

@implementation UIViewController (Helper)

+(UIViewController *)viewController
{
    return [[self alloc] init];
}


- (BOOL)isVisible
{
    return (self.isViewLoaded && self.view.window);
}

- (BOOL)isDismissed
{
    return ([self isBeingDismissed] || [self isMovingFromParentViewController]);
}

- (BOOL)isPresented
{
    return ([self isBeingPresented] || [self isMovingToParentViewController]);
}



-(void)presentOnTop
{
    [[UIApplication topMostController] presentViewController:self animated:YES completion:nil];
}



-(void)setBackButton
{
    __typeof(self)blockSelf = self;
    [self setBackButton:^{
        [blockSelf.navigationController popViewControllerAnimated:YES];
    }];
}

//-(void)setLeftMenuButton
//{
//    __typeof(self)blockSelf = self;
//    [self setLeftMenuButton:^{
//        [blockSelf.sidePanelController showLeftPanelAnimated:YES];
//    }];
//}
//
//-(void)setRightMenuButton
//{
//    __typeof(self)blockSelf = self;
//    [self setRightMenuButton:^{
//        [blockSelf.sidePanelController showRightPanelAnimated:YES];
//    }];
//}



-(void)setBackButton:(Block)block
{
    [self setObject:block forKey:BACKACTIIONHANDLER];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage backImage] imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 8, 0, 0)] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    
}

-(void)backButtonClicked:(id)sender
{
    Block block = [self objectForKey:BACKACTIIONHANDLER];
    
    if (block)
        block();
}



//-(void)setLeftMenuButton:(Block)block
//{
//    [self setObject:block forKey:MENUACTIONHANDLER];
//
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:self.sidePanelController.menuImage?self.sidePanelController.menuImage:[JASidePanelController defaultImage] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked:)];
//}
//
//-(void)setRightMenuButton:(Block)block
//{
//    [self setObject:block forKey:MENUACTIONHANDLER];
//    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:self.sidePanelController.menuImage?self.sidePanelController.menuImage:[JASidePanelController defaultImage] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked:)];
//}

-(void)menuButtonClicked:(id)sender
{
    Block block = [self objectForKey:MENUACTIONHANDLER];
    
    if (block)
        block();
}

- (void)presentPopUp:(UIView *)view from:(PresentType)presentType
{
    if (view)
    {
        MIPopOverlayView *popUpOverlay = [MIPopOverlayView popUpOverlay];
      //  popUpOverlay.shouldCloseOnClickOutside = closable;
        popUpOverlay.presentType = presentType;
        [appDelegate.window addSubview:popUpOverlay];
        
        if (presentType == PresentTypeCenter)
        {
            view.center = popUpOverlay.center;
            view.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
            [UIView animateWithDuration:0.2 animations:^{
                view.transform = CGAffineTransformIdentity;
            } completion:nil];
            [popUpOverlay addSubview:view];
        }
        else if (presentType == PresentTypeBottom)
        {
            CViewSetY(view, CScreenHeight);
            
            [UIView animateWithDuration:0.2 animations:^{
                CViewSetY(view, CScreenHeight - CViewHeight(view));
            } completion:nil];
            [popUpOverlay addSubview:view];
        }
    }
}

- (void)dismissPopUp:(UIView *)view
{
    if (view && [view.superview isKindOfClass:[MIPopOverlayView class]])
    {
        __block MIPopOverlayView *popUpOverlay = (MIPopOverlayView *)view.superview;
        
        if (popUpOverlay.presentType == PresentTypeCenter)
        {
            [UIView animateWithDuration:0.1 animations:^{
                view.transform = CGAffineTransformMakeScale(0.01, 0.01);
            } completion:^(BOOL finished)
             {
                 [popUpOverlay removeFromSuperview];
                 popUpOverlay = nil;
             }];
        }
        else
        {
            [UIView animateWithDuration:0.1 animations:^{
                CViewSetY(view, CScreenHeight);
            } completion:^(BOOL finished)
             {
                 [popUpOverlay removeFromSuperview];
                 popUpOverlay = nil;
             }];
        }
    }
}


@end



// To-Do check "presentOnTop" method - Its not working.
