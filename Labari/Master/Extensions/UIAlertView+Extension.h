//
//  UIAlertView+Extension.h
//  MyPatients
//
//  Created by mac-0010 on 4/2/15.
//  Copyright (c) 2015 Robots and Pencils Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//define this handler outside the declaration as in category we cannot add instance variables, in .m file we will associate this with self
typedef void(^UIActionAlertViewCallBackHandler)(UIAlertView *alertView, NSInteger buttonIndex);

@interface UIAlertView (Extension)<UIAlertViewDelegate>

- (void)showAlerViewWithHandlerBlock:(UIActionAlertViewCallBackHandler)handler;

@end
