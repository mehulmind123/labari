//
//  UITextField+MITextField.h
//  EdSmart
//
//  Created by mac-0006 on 14/07/2016.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (MITextField)

- (void)addLeftPaddingWithWidth:(CGFloat)width;
- (void)addRightPaddingWithWidth:(CGFloat)width;

-(void)setLeftImage:(UIImage *)img withSize:(CGSize)sizeImg;
-(void)setRightImage:(UIImage *)img withSize:(CGSize)sizeImg;

@end
