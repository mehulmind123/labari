//
//  UIViewController+Helper.h
//  MI API Example
//
//  Created by mac-0001 on 13/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Master.h"
#import "MIPopOverlayView.h"
@interface UIViewController (Helper)

+(UIViewController *)viewController;

- (BOOL)isVisible;
- (BOOL)isDismissed;
- (BOOL)isPresented;


-(void)presentOnTop;


-(void)setBackButton;


-(void)setBackButton:(Block)block;

- (void)presentPopUp:(UIView *)view from:(PresentType)presentType;
- (void)dismissPopUp:(UIView *)view;
@end
