//
//  UITableView+Extension.h
//  Master
//
//  Created by mac-0001 on 21/02/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Extension)

-(NSIndexPath *)lastIndexPath;

@end
