//
//  UIWebView+LoadingView.h
//  Patch
//
//  Created by mac-00015 on 1/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (LoadingView)
+(id)loadingWebView;
@end
