//
//  UIWebView+LoadingView.m
//  Patch
//
//  Created by mac-00015 on 1/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//


#import "UIWebView+LoadingView.h"
#import "Constants.h"

@implementation UIWebView (LoadingView)

+(id)loadingView
{
    id view = [self loadingWebView];
    
    if(!view)
        view = [[[self class] alloc] init];
    
    return view;
}


+(id)loadingWebView
{
    NSString *strImage = @"";
    
    if (Is_iPhone_6)
    {
        strImage = @"contentLoading_6";
    }
    else if (Is_iPhone_6_PLUS || IS_IPAD)
    {
        strImage = @"contentLoading_6P";
    }
    else
    {
        if (Is_iPhone_4)
            strImage = @"contentLoading_4";
        else
            strImage = @"contentLoading_5";
    }
    
    UIWebView *loadingWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, CScreenHeight)];
    loadingWebView.backgroundColor = [UIColor clearColor];
    NSURL *url = [[NSBundle mainBundle] URLForResource:strImage withExtension:@"gif"];
    NSURLRequest *imgreq = [NSURLRequest requestWithURL:url];
    loadingWebView.scalesPageToFit = YES;
    [loadingWebView loadRequest:imgreq];
    return loadingWebView;
}

@end
