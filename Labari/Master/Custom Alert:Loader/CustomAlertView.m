//
//  CustomAlertView.m
//  EdSmart
//
//  Created by mac-0006 on 15/03/2016.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import "CustomAlertView.h"
#define DisplayDuration   4
#define AnimateDuration   0.2

static CustomAlertView *selfAlert = nil;

@implementation CustomAlertView

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        selfAlert = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:nil options:nil]lastObject];
        CViewSetWidth(selfAlert, CScreenWidth);
    });
    
    return selfAlert;
}

+ (void)iOSAlert:(NSString *)title withMessage:(NSString *)message withDelegate:(id)delegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

+ (void)showStatusBarAlert:(NSString *)message
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [CustomAlertView sharedInstance];

    [selfAlert.timerAlert invalidate];
    selfAlert.timerAlert = nil;
    
    selfAlert.lblAlert.text = message;
    [selfAlert.lblAlert layoutIfNeeded];
    [selfAlert.lblAlert updateConstraintsIfNeeded];
    [selfAlert.lblAlert setPreferredMaxLayoutWidth:(CScreenWidth - 20)];
    
    CGFloat height = [selfAlert systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 20;
    CViewSetHeight(selfAlert, height);

    if (![selfAlert.superview  isKindOfClass:[UIWindow class]])
    {
        selfAlert.alpha = 0;
        
        [UIView animateWithDuration:AnimateDuration animations:^
         {
             selfAlert.alpha = 1;
             [selfAlert layoutIfNeeded];
             [appDelegate.window addSubview:selfAlert];
         } completion:nil];
    }
    
    selfAlert.timerAlert = [NSTimer scheduledTimerWithTimeInterval:DisplayDuration target:selfAlert selector:@selector(hideWithAnimation) userInfo:nil repeats:NO];
}

- (void)hideWithAnimation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    [UIView animateWithDuration:AnimateDuration animations:^
    {
        selfAlert.lblAlert.text = @"";
        CViewSetHeight(selfAlert, 0);
        [selfAlert layoutIfNeeded];
        
    } completion:^(BOOL finished)
    {
        [selfAlert removeFromSuperview];
    }];
}

+ (void)dismissStatusbarAlert
{
    if (selfAlert)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [selfAlert removeFromSuperview];
    }
}

@end
