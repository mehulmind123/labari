//
//  CustomAlertView.h
//  EdSmart
//
//  Created by mac-0006 on 15/03/2016.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView

@property (strong, nonatomic) IBOutlet UILabel *lblAlert;
@property (strong, nonatomic)  NSTimer *timerAlert;

+ (instancetype)sharedInstance;

+ (void)iOSAlert:(NSString *)title withMessage:(NSString *)message withDelegate:(id)delegate;
+ (void)showStatusBarAlert:(NSString *)message;
+ (void)dismissStatusbarAlert;
@end
