//
//  UIImage+fixOrientation.h
//  Finance
//
//  Created by mac-0001 on 7/15/13.
//  Copyright (c) 2013 MInd-MAC Mini 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)
- (UIImage *)fixOrientation;

-(UIImage *)scaleAndRotateImage:(UIImage *)image;
@end
