//
//  Master.h
//  Master
//
//  Created by mac-0001 on 28/11/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//


#ifndef _Master_H
    #define _Master_H
#endif



#define MASTER_VERSION @"4.0"


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "Constants.h"
#import "BasicAppDelegate.h"
#import "UIApplication+Extension.h"

#import "NSString+MIExtension.h"
#import "UIDevice-Hardware.h"
#import "UIView+MIExtension.h"
#import "UITextField+MIExtension.h"
#import "UITextView+MIExtension.h"
#import "NSDictionary+MIExtension.h"
#import "UIImage+MIExtension.h"
#import "UIViewController+Helper.h"
#import "UIViewController+InAppPurchase.h"
#import "UIViewController+UIImagePickerController.h"
#import "UIButton+EventHandler.h"
#import "UIView+ScreenShot.h"
#import "UIViewController+ScreenShot.h"
#import "UIViewController+BlockHandler.h"
#import "UIViewController+LoaderAndAlerts.h"
#import "UINavigationController+Extension.h"
#import "UIActionSheet+EventHandler.h"
#import "UIView+Extension.h"
#import "NSDate+MIExtension.h"
#import "UITableView+Extension.h"
#import "UIImage+Extensiom.h"
#import "UIBarButtonItem+Extension.h"
#import "UILabel+Extension.h"
#import "NSFetchedResultsController+Extension.h"
#import "UIGestureRecognizer+Blocks.h"
#import "UIImageView+FullScreenImage.h"

#import "NSObject+NewProperty.h"
#import "DelegateObserver.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
#import "UIAlertController+Extension.h"
#endif

#import "NSString+Extension.h"

#import "UIView+AutoLayoutConstraints.h"
#import "CustomAlertView.h"


#import "LocationManager.h"
#import "UIViewController+LocationManager.h"

#import "Store.h"
#import "NSManagedObject+Helper.h"
#import "NSManagedObjectContext+Helper.h"
#import "FetchedResultsTableDataSource.h"
#import "UIViewController+NSFetchedResultsController.h"


#import "KeychainWrapper.h"
#import "LocalizationSystem.h"


#import "SHCMulticastDelegate.h"
#import "NSObject+Extension.h"

#import "UIControl+JTTargetActionBlock.h"



#import "MIQuery.h"
#import "NSManagedObject+Mapping.h"


static NSString *const NSLocationAlwaysUsageDescription = @"NSLocationAlwaysUsageDescription";
static NSString *const NSLocationWhenInUseUsageDescription = @"NSLocationWhenInUseUsageDescription";

