//
//  ParentViewController.m
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "ParentViewController.h"
#import "MIGradientLayer.h"

@interface ParentViewController ()

@end

@implementation ParentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configure];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self resignKeyboard];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark - General Method

- (void)configure
{
    //...Generic config of UINavigationBar.....
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
   
   //...Set gradientLayer in Navigation bar
    MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrame:self.navigationController.navigationBar.layer.bounds];
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *gradientImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
   
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithPatternImage:gradientImage]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:CFontAvenirLTStd65Meduim(15), NSForegroundColorAttributeName:ColorWhite_FFFFFF}];
    
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    UIImage *image = [[UIImage imageNamed:@"back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.navigationController.navigationBar setBackIndicatorTransitionMaskImage:image];
    [self.navigationController.navigationBar setBackIndicatorImage:image];
    
}

- (void)resignKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}



#pragma mark -
#pragma mark - UIStatusBar

- (void)setStatusBarHidden:(BOOL)hidden
{
    [self setNeedsStatusBarAppearanceUpdate];
}


@end
