//
//  MIProfileHeaderView.h
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIProfileHeaderView : UIView
{
    IBOutlet UIButton *btnFollowings;
    IBOutlet UIButton *btnFollowers;
}


@property (strong, nonatomic) IBOutlet UIImageView *imgVUser;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblAboutUser;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalFolloewers;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalFollowings;
@property (strong, nonatomic) IBOutlet UILabel *lblFollowings;
@property (strong, nonatomic) IBOutlet UILabel *lblFollowers;

@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnChat;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnCamera;
@property (strong, nonatomic) IBOutlet UIButton *btnPost;
@property (strong, nonatomic) IBOutlet UIButton *btnAlbums;
@property (strong, nonatomic) IBOutlet UIButton *btnFriends;
@property (strong, nonatomic) IBOutlet UIButton *btnFFPost;
@property (strong, nonatomic) IBOutlet UIButton *btnFollow;
@property (strong, nonatomic) IBOutlet UIButton *btnAddFriend;

@property (strong, nonatomic) IBOutlet UIView *viewSelectedTab;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnBtnAddFriendCenter;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnBtnFollowCenter;


+(id)customProfileHeader;


- (IBAction)btnFollowersClicked:(id)sender;
- (IBAction)btnFollowingsClicked:(id)sender;

@end
