//
//  MIProfileHeaderView.m
//  Labari
//
//  Created by mac-0005 on 3/23/17.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIProfileHeaderView.h"


#import "MIFollowingViewController.h"

@implementation MIProfileHeaderView


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrame:_viewSelectedTab.layer.bounds];
    [_viewSelectedTab.layer insertSublayer:gradient atIndex:0];
    
    _viewSelectedTab.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewSelectedTab.layer.shadowOffset = CGSizeZero;
    _viewSelectedTab.layer.shadowOpacity = 0.5f;
    _viewSelectedTab.layer.shadowRadius = 4.0f;
    _viewSelectedTab.layer.masksToBounds = NO;
    
}

+(id)customProfileHeader
{
    MIProfileHeaderView  *customHeader = [[[NSBundle mainBundle] loadNibNamed:@"MIProfileHeaderView" owner:nil options:nil] lastObject];
    
    if(Is_iPhone_5)
       customHeader.frame = CGRectMake(0, 0, CScreenWidth, 485);
    else
       customHeader.frame = CGRectMake(0, 0, CScreenWidth, (485 * CScreenWidth) / 375);
    
    
    [customHeader.btnAddFriend setTitle:CLocalize(@"Add Friend").uppercaseString forState:UIControlStateNormal];
    [customHeader.btnPost setTitle:CLocalize(@"Posts").uppercaseString forState:UIControlStateNormal];
    [customHeader.btnAlbums setTitle:CLocalize(@"Albums").uppercaseString forState:UIControlStateNormal];
    [customHeader.btnFriends setTitle:CLocalize(@"Friends").uppercaseString forState:UIControlStateNormal];
    [customHeader.btnFFPost setTitle:CLocalize(@"FF Post").uppercaseString forState:UIControlStateNormal];
    
    
    [customHeader.lblFollowers setText:CLocalize(@"Followers").uppercaseString];
    [customHeader.lblFollowings setText:CLocalize(@"Followings").uppercaseString];
    
    
    
    return customHeader;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _imgVUser.layer.cornerRadius = CViewWidth(_imgVUser) / 2;
    _imgVUser.layer.borderColor = [UIColor whiteColor].CGColor;
    _imgVUser.layer.borderWidth = 3;
    _imgVUser.layer.masksToBounds = YES;
    
    [self.btnFollow layoutIfNeeded];
    MIGradientLayer *gradientFollow = [MIGradientLayer gradientLayerWithFrameForBorder:self.btnFollow.layer.bounds];
    [self.btnFollow.layer insertSublayer:gradientFollow atIndex:0];
    
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnFollowersClicked:(id)sender
{
    MIFollowingViewController *followingVC = [[MIFollowingViewController alloc] initWithNibName:@"MIFollowingViewController" bundle:nil];
    followingVC.followType = Followers;
    [self.viewController.navigationController pushViewController:followingVC animated:YES];
}

- (IBAction)btnFollowingsClicked:(id)sender
{
    MIFollowingViewController *followingVC = [[MIFollowingViewController alloc] initWithNibName:@"MIFollowingViewController" bundle:nil];
    followingVC.followType = Followings;
    [self.viewController.navigationController pushViewController:followingVC animated:YES];
}

@end
