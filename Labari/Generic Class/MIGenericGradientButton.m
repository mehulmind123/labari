//
//  MIGenericGradientButton.m
//  Labari
//
//  Created by mac-0006 on 31/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIGenericGradientButton.h"
#import "MIGradientLayer.h"

@implementation MIGenericGradientButton


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initialize];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setNeedsUpdateConstraints];
    [self layoutIfNeeded];
    
    self.layer.cornerRadius = CViewHeight(self.layer) / 2;
    
    if(self.tag != 100)
    {
        MIGradientLayer *gradient = [MIGradientLayer gradientLayerWithFrame:self.layer.bounds];
        [self.layer insertSublayer:gradient atIndex:0];
    }
    
}

- (void)initialize
{
    NSString *fontName = self.titleLabel.font.fontName;
    CGFloat fontSize = self.titleLabel.font.pointSize;
    
    
    if(Is_iPhone_5)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 1)];
    else if (Is_iPhone_6_PLUS)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 1)];
    
    self.layer.masksToBounds = YES;
    
}

@end
