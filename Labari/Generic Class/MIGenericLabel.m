//
//  MIGenericLabel.m
//  Labari
//
//  Created by mac-0006 on 21/03/2017.
//  Copyright © 2017 Labari. All rights reserved.
//

#import "MIGenericLabel.h"

@implementation MIGenericLabel

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if(Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize-1];
    else if(Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize+1];
    
}


@end
