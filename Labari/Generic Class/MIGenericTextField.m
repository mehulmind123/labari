//
//  MIGenericTextField.m
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIGenericTextField.h"

@implementation MIGenericTextField

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize - 1];
    else if(Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize + 1];
    
    [self addLeftPaddingWithWidth:25];
    [self setPlaceHolderColor:ColorGray_C6DAEC];
    
    if (self.tag == 200)
    {
        self.layer.cornerRadius = 5;
    }
}

@end
