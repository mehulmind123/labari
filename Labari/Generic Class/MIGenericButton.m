//
//  MIGenericButton.m
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIGenericButton.h"
#import "MIGradientLayer.h"

@implementation MIGenericButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSString *fontName = self.titleLabel.font.fontName;
    CGFloat fontSize = self.titleLabel.font.pointSize;
    
    [self layoutIfNeeded];
   // self.layer.cornerRadius = self.bounds.size.height/2; //CViewHeight(self.layer.bounds) / 2;
    
    if(Is_iPhone_5)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 1)];
    else if (Is_iPhone_6_PLUS)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 1)];
    
    self.layer.masksToBounds = YES;
}


@end
