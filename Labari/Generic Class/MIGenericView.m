//
//  MIGenericView.m
//  Labari
//
//  Created by mac-0006 on 18/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIGenericView.h"

@implementation MIGenericView

- (void)awakeFromNib
{
    [super awakeFromNib];
  
    self.layer.cornerRadius = 5;
    
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowRadius = 3.0f;
    self.layer.shadowOpacity = 0.1f;
    self.layer.masksToBounds = NO;
}

@end
