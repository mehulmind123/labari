//
//  MIGradientLayer.m
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIGradientLayer.h"

@implementation MIGradientLayer

+(id)gradientLayerWithFrame:(CGRect)frame
{
    MIGradientLayer *gradient = [MIGradientLayer layer];
    gradient.frame = frame;
    
    
    gradient.colors = [NSArray arrayWithObjects:
                       (id)([UIColor colorWithRed:(87/255.0) green:(195/255.0) blue:(182/255.0) alpha:1.0].CGColor),
                       (id)([UIColor colorWithRed:(27/255.0) green:(187/255.0) blue:(213/255.0) alpha:1.0].CGColor),
                       (id)([UIColor colorWithRed:(52/255.0) green:(156/255.0) blue:(215/255.0) alpha:1.0].CGColor), nil];
    
    gradient.startPoint = CGPointMake(0.2,0.0);
    gradient.endPoint = CGPointMake(0.8, 0.0);
    
    return gradient;
}

+(id)gradientLayerWithFrameSeperator:(CGRect)frame
{
    MIGradientLayer *gradient = [MIGradientLayer layer];;
    gradient.frame = frame;
    
    gradient.colors = [NSArray arrayWithObjects:
                       (id)([UIColor colorWithRed:(255/255.0) green:(240/255.0) blue:(5/255.0) alpha:1.000].CGColor),
                       (id)([UIColor colorWithRed:(255/255.0) green:(186/255.0) blue:(0/255.0) alpha:1.000].CGColor), nil];
    
    gradient.startPoint = CGPointMake(0.2,0.0);
    gradient.endPoint = CGPointMake(0.8, 0.0);
    
    return gradient;
}


+(id)gradientLayerWithFrameForBorder:(CGRect)frame
{
    MIGradientLayer *gradient = [self gradientLayerWithFrame:frame];
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.lineWidth = 1.5;
    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:50].CGPath;
    shapeLayer.fillColor = nil;
    shapeLayer.strokeColor = [UIColor blackColor].CGColor;
    gradient.mask = shapeLayer;
    gradient.cornerRadius = frame.size.height/2;
    return gradient;
    
}

@end
